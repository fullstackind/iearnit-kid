/**
 * @format
 */

import {AppRegistry} from 'react-native';
import React from 'react';
import App from './App';
import {name as appName} from './app.json';
import {Provider} from "react-redux";
import {store} from "./store";
import FlashMessage from "react-native-flash-message";
import OfflineNotice from "./src/components/OfflineNotice";
const RNRedux = () => (
  
  
    <Provider store = { store }>
    <OfflineNotice></OfflineNotice>
      <App />
      <FlashMessage position="top" />
    </Provider>
  )
AppRegistry.registerComponent(appName, () => RNRedux);
