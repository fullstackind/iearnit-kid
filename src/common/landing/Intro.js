import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import { SvgUri } from 'react-native-svg';
import {styles, colors, fsize} from '../../styles/style'

const slides = [
    {
      key: 1,
      title: 'Q-Bank',
      text: 'Description.\nSay something cool',
      // image: require('../assets/images/naveen-kingsly-04-PjpsHya0-unsplash.jpg'),
      backgroundColor: colors.primary,
    },
    {
      key: 2,
      title: 'Q-Bank',
      text: 'Other cool stuff',
      // image: require('../assets/images/naveen-kingsly-04-PjpsHya0-unsplash.jpg'),
      backgroundColor: colors.primary,
    },
    {
      key: 3,
      title: 'Q-Bank',
      text: 'I\'m already out of descriptions\n\nLorem ipsum bla bla bla',
      // image: require('../assets/images/naveen-kingsly-04-PjpsHya0-unsplash.jpg'),
      backgroundColor: colors.primary,
    }
  ];
export default class Intro extends Component {
  constructor(props) {
    super(props);
    this.state = {
        showRealApp: false
    };
  }
  _renderItem = ({ item }) => {
    return (

    
      <View style={style.slide}>
      <View style={{  position:"absolute",
        top:20}}>
    <Text style={{marginBottom:0}}><Text style={{...styles.logo_text_B,fontSize:fsize.h2,}}>Q-</Text><Text style={{...styles.logo_text,fontSize:fsize.h2}}>Bank</Text></Text>
    </View>    
        <SvgUri
    width='320'
    height='320'
    uri="http://intelivibe.in/images/illustration1.svg"
  />
    {/* <Text style={style.text}>{item.text}</Text> */}
    <View style={{ marginTop:30 }}>
    <Text style={{marginBottom:0,...styles.logo_text,fontSize:fsize.lg,textAlign: 'center'}}>The <Text style={{...styles.logo_text_B,fontSize:fsize.lg}}>ALL NEW</Text></Text>
    <Text style={{marginBottom:10,...styles.logo_text,fontSize:fsize.lg,textAlign: 'center'}}>QBank is here</Text>
    
    </View> 
    <TouchableOpacity style={{position:"absolute",
        bottom:70}} onPress={() => this.props.navigation.navigate('AuthLanding')} >
    <Text style={{...style.text,...styles.logo_text,fontSize:fsize.md-2,textAlign: 'center',color:"#f2f2f2"}}>SKIP INTRO</Text> 
    </TouchableOpacity>
    </View>
 
    );
  }
  _onDone = () => {
    // User finished the introduction. Show real app through
    // navigation or simply by controlling state
    // this.setState({ showRealApp: true });
    this.props.navigation.navigate('AuthLanding');
  }
  render() {
    if (this.state.showRealApp) {
        return <View><Text>text</Text></View>;
      } else {
        return <AppIntroSlider showNextButton={false}  renderItem={this._renderItem} data={slides} onDone={this._onDone}/>;
      }
  }
}


const style = StyleSheet.create({
    slide: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.primary,
        position:"relative"
      },
      image: {
        width: 320,
        height: 320,
        marginVertical: 32,
      },
      text: {
        color: 'rgba(255, 255, 255, 0.8)',
        textAlign: 'center',
        
      },
      title: {
        fontSize: 22,
        color: 'white',
        textAlign: 'center',
        paddingVertical:10,
      
      },
});
