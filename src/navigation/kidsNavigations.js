import React from 'react';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    TouchableOpacity,
    Image,
    Dimensions
  } from 'react-native';

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import {
	createDrawerNavigator,
	DrawerContentScrollView,
	DrawerItemList,
} from "@react-navigation/drawer";
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Feather from 'react-native-vector-icons/Feather';
import { styles } from '../styles/style';
import { colors,fonts,fsize,images } from '../styles/base';
import {useSelector} from "react-redux";
import { BASE_URL,IMG_URL } from '../config';

import Dash from '../components/kids/Dash';
import Logout from '../components/auth/Logout';
import JobsActive from '../components/kids/JobsActive';
import JobsCompleted from '../components/kids/JobsCompleted';
import JobDetails from '../components/kids/JobDetails';
import ConnectionsList from '../components/kids/ConnectionsList';
import UserDetail from '../components/kids/UserDetail';
import RequestedUsers from '../components/kids/RequestedUsers';
import FindUsers from '../components/kids/FindUsers';
import RedeemCoupon from '../components/kids/RedeemCoupon';
import PurchasedCoupons from '../components/kids/PurchasedCoupons';
import Notifications from '../components/kids/Notifications';
import Profile from '../components/kids/Profile';
import UpdateProfile from '../components/kids/UpdateProfile';
import OfferPage from '../components/kids/OfferPage';
import ListCoupons from '../components/kids/ListCoupons';


const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();
const ListingTabs = createMaterialTopTabNavigator();


const HomeStack = createNativeStackNavigator();
const HomeScreen = () => (
   <HomeStack.Navigator
    screenOptions={({navigation, route}) => ({
      
      headerStyle: {
        backgroundColor: colors.primary,
      },
      headerTintColor: '#fff',
      headerShadowVisible:false,
      headerTitleStyle: {
        
        fontFamily:fonts.primaryLight
      },
      headerLeft: (props) => (
        <View style={{paddingLeft: 0}}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack()
            }}>
            <Ionicons name="chevron-back" size={40} color="#fff" />
          </TouchableOpacity>
        </View>
      ),
      headerRight: (props) => (
       <></>
         ),
    })
  
  
  }
    
    >

      <HomeStack.Screen
        name="Dash"
        component={Dash}
        options={{headerShown:false, }}
      />
          <HomeStack.Screen
        name="Jobs"
        component={MyTabs}
        options={{headerShown:true, }}
      />
      <HomeStack.Screen
        name="JobDetails"
        component={JobDetails}
        options={{headerShown:true, }}
      />
      <HomeStack.Screen
        name="UserConnections"
        component={ConnectionsList}
        options={{headerShown:true,title:"User Connections" }}
      />
       <HomeStack.Screen
        name="UserDetail"
        component={UserDetail}
        options={{headerShown:true,title:"View Profile" }}
      />
       <HomeStack.Screen
        name="Users"
        component={UsersTabs}
        options={{headerShown:true, }}
      />
      <HomeStack.Screen
        name="RedeemCoupon"
        component={RedeemCoupon}
        options={{headerShown:true,title:"Purchase Coupon"  }}
      />
       <HomeStack.Screen
        name="OfferPage"
        component={OfferPage}
        options={{headerShown:true,title:"Offers"  }}
      />
       <HomeStack.Screen
        name="ListCoupons"
        component={ListCoupons}
        options={{headerShown:true,title:"Available Redeems"  }}
      />
      <HomeStack.Screen
        name="Profile"
        component={ProfileStackScreen}
        options={{headerShown:false, }}
      />
   </HomeStack.Navigator>
);

const getTabBarVisibility = (route) => {
	const routeName = route.state
	  ? route.state.routes[route.state.index].name
	  : '';
  
	if (routeName === 'Teachers') {
	  return false;
	}else if(routeName === 'Requests') {
		return false;
	}else if(routeName === 'Learning') {
		return false;
	}else if(routeName === 'Videos') {
		return false;
	}else if(routeName === 'Chats') {
		return false;
	}else{

		return true;
	}
  
	
  }
  function MyTabs() {
    return (
      <ListingTabs.Navigator  screenOptions={{
        tabBarLabelStyle: { fontSize: 12,...styles.labelText },
        // tabBarItemStyle: { width: 100 },
        tabBarStyle: { backgroundColor: '#fff',elevation:0 },
        tabBarIndicatorStyle: { backgroundColor:colors.thirdary,height:2 },
        
      }}
      swipeEnabled={false}
      >
        <ListingTabs.Screen name="Active" component={JobsActive} />
        <ListingTabs.Screen name="Completed" component={JobsCompleted} />
      </ListingTabs.Navigator>
    );
  }
  
  const UsersListingTabs = createMaterialTopTabNavigator();
  
  function UsersTabs() {
    return (
      <UsersListingTabs.Navigator  screenOptions={{
        tabBarLabelStyle: { fontSize: 12,...styles.labelText },
        // tabBarItemStyle: { width: 100 },
        tabBarStyle: { backgroundColor: '#fff',elevation:0 },
        tabBarIndicatorStyle: { backgroundColor:colors.thirdary,height:2 },
       
      }}
      swipeEnabled={false}
      >
        <UsersListingTabs.Screen name="Requests" component={RequestedUsers} />
        <UsersListingTabs.Screen name="Find Users" component={FindUsers} />
      </UsersListingTabs.Navigator>
    );
  }
  function DrawerContent(props) {
    const activeState = useSelector((state) => state);
  
      // console.log("reduxstore",activeState);
      return (
      <View style={{backgroundColor:colors.secondary,height:Dimensions.get('window').height}}>
          
          <DrawerContentScrollView {...props}>
                  
              <SafeAreaView>
      
                      <View
                          style={{
                              height: 120,
                              alignItems: "center",
                              justifyContent: "center",
                          }}>
   { activeState.authUser.userData.avatar == null &&  activeState.authUser.userData.profile_pic != null && (
              <>
                  <Image
                              source={{uri:IMG_URL+'uploads/profile/'+activeState.authUser.userData.profile_pic}}
                              // resizeMode={"contain"}
                              style={{ width: 80,
                                  height: 80,
                                  borderRadius: 60,
                                  borderWidth: 2,
                                  borderColor: "white",
                                  marginBottom:8,}}
                          />
  
              </>
   )}
   {activeState.authUser.userData.profile_pic == null && activeState.authUser.userData.avatar != null && (
              <>
                  <Image
                              source={{uri:IMG_URL+'uploads/profile/avatar/'+activeState.authUser.userData.avatar}}
                              // resizeMode={"contain"}
                              style={{ width: 80,
                                  height: 80,
                                  borderRadius: 60,
                                  borderWidth: 2,
                                  borderColor: "white",
                                  marginBottom:8,}}
                          />
  
              </>
   )}
                      
  
                          <Text style={{fontSize:20,...styles.headTextSmall}}>{activeState.authUser.userData.name}</Text>
                      </View>
                      
                      <View style={{flex: 1}}>
                          <DrawerItemList {...props} />
                      </View>
          
              </SafeAreaView>
              
          </DrawerContentScrollView>
      
      
      </View>
      );
  }
  const tabNavigations = () => {
	return (
		<Tab.Navigator
			initialRouteName="Home"
			screenOptions={{
			headerShown:false,
        activeTintColor: "#F9F6F6",
        inactiveTintColor: "grey",
        tabBarActiveTintColor: colors.primary,
        tabBarInactiveTintColor: 'gray',
        style: {backgroundColor: "white"},
				// labelStyle: {fontFamily: font.primary},
			}}
			// initialRouteName="StutorsHome"
			activeColor="#fff"
			shifting="false"
		>
			<Tab.Screen
				name="Home"

				options={({ route }) => ({
					tabBarVisible: getTabBarVisibility(route),
					tabBarLabel: "Home",
					tabBarIcon: ({color}) => (
						<MaterialIcons name="home" color={color} size={26} />
					),
				  })}
				
				component={HomeScreen}
			/>

			<Tab.Screen
				name="Connections"
				options={({ route }) => ({
					tabBarVisible: getTabBarVisibility(route),
					tabBarLabel: "Connections",
					tabBarIcon: ({color}) => (
						<Feather name="users" color={color} size={26} />
					),
				})}
				component={ConnectionStackScreen}
			/>

			<Tab.Screen
				name="MyJobs"
				options={{
					tabBarLabel: "My Jobs",
					tabBarIcon: ({color}) => (
						<MaterialCommunityIcons name="bag-personal-outline" color={color} size={28} />
					),
				}}
				component={JobStackScreen}
			/>

			<Tab.Screen
				name="Notifications"
				options={{
					tabBarLabel: "Notifications",
					tabBarIcon: ({color}) => (
						<Feather name="bell" color={color} size={26} />
					),
					
				}}
				
				component={NotificationsStackScreen}
			/>
      <Tab.Screen
				name="My Profile"
				options={{
					tabBarLabel: "Profile",
					tabBarIcon: ({color}) => (
						<Feather name="user" color={color} size={26} />
					),
					
				}}
				
				component={ProfileStackScreen}
			/>
		</Tab.Navigator>
	);
};



const KidsDashbaordStack = createNativeStackNavigator();
const KidsDashboardScreen = () => (
    <KidsDashbaordStack.Navigator headerMode="none">
    <KidsDashbaordStack.Screen
        name="Dashboard"
        component={DrawerScreen}
        options={{
            animationEnabled: false,
            headerShown:false,
        }}
    />
</KidsDashbaordStack.Navigator>
);

const DrawerScreen = () => {
	return (
		<Drawer.Navigator
			initialRouteName="Home"
			drawerStyle={{
				backgroundColor: "red",
				width: 280,
			}}
			drawerContent={(props) => <DrawerContent {...props} />}>
			<Drawer.Screen
				name="Home"
				component={tabNavigations}
				options={{
					title: "Home", //Set Header Title
          headerShown:false,
					drawerIcon: () => (
						<View
							style={
								(styles.DrawerIcons,
								{
									alignItems: "center",
									justifyContent: "center",
									backgroundColor: "#fff",
									height: 40,
									width: 40,
									borderRadius: 20,
								})
							}>
							{/* <Image source={require('./assets/images/tickets.png')}/> */}
							<Ionicons name="home-outline" size={30} color={colors.primary} />
						</View>
					),
				}}
			/>
      <Drawer.Screen
				name="Coupons"
				component={PurchasedCoupons}
				options={{
					title: "Coupons", //Set Header Title
          headerShown:true,
					drawerIcon: () => (
						<View
							style={
								(styles.DrawerIcons,
								{
									alignItems: "center",
									justifyContent: "center",
									backgroundColor: "#fff",
									height: 40,
									width: 40,
									borderRadius: 20,
								})
							}>
							{/* <Image source={require('./assets/images/tickets.png')}/> */}
							<Ionicons name="pricetags" size={30} color={colors.primary} />
						</View>
					),
				}}
			/>
	<Drawer.Screen
				name="Logout"
				component={Logout}
				options={{
					title: "Logout", //Set Header Title
          headerShown:false,
					drawerIcon: () => (
						<View
							style={
								(styles.DrawerIcons,
								{
									alignItems: "center",
									justifyContent: "center",
									backgroundColor: "#fff",
									height: 40,
									width: 40,
									borderRadius: 20,
								})
							}>
							{/* <Image source={require('./assets/images/tickets.png')}/> */}
							<Ionicons name="log-out-outline" size={30} color={colors.primary} />
						</View>
					),
				}}
			/>
		</Drawer.Navigator>
	);
};
const ConnectionStack = createNativeStackNavigator();
const ConnectionStackScreen = () => {

	
	return (
		<ConnectionStack.Navigator 
		screenOptions={({navigation, route}) => ({
      
      headerStyle: {
        backgroundColor: colors.primary,
      },
      headerTintColor: '#fff',
      headerShadowVisible:false,
      headerTitleStyle: {
        
        fontFamily:fonts.primaryLight
      },
      headerLeft: (props) => (
        <View style={{paddingLeft: 0}}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack()
            }}>
            <Ionicons name="chevron-back" size={40} color="#fff" />
          </TouchableOpacity>
        </View>
      ),
      headerRight: (props) => (
       <></>
         ),
    })
  
  
  }>
		
      	<ConnectionStack.Screen
				name="Users"
				component={UsersTabs}
				options={{headerShown: true}}
			/>
		
	
		</ConnectionStack.Navigator>
	);
};
const JobStack = createNativeStackNavigator();
const JobStackScreen = () => {

	
	return (
		<JobStack.Navigator 
		screenOptions={({navigation, route}) => ({
      
      headerStyle: {
        backgroundColor: colors.primary,
      },
      headerTintColor: '#fff',
      headerShadowVisible:false,
      headerTitleStyle: {
        
        fontFamily:fonts.primaryLight
      },
      headerLeft: (props) => (
        <View style={{paddingLeft: 0}}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack()
            }}>
            <Ionicons name="chevron-back" size={40} color="#fff" />
          </TouchableOpacity>
        </View>
      ),
      headerRight: (props) => (
       <></>
         ),
    })
  
  
  }>
		
      	<JobStack.Screen
				name="My Jobs"
				component={MyTabs}
				options={{headerShown: true}}
			/>
		
	
		</JobStack.Navigator>
	);
};
const NotificationsStack = createNativeStackNavigator();
const NotificationsStackScreen = () => {

	
	return (
		<NotificationsStack.Navigator 
		screenOptions={({navigation, route}) => ({
      
      headerStyle: {
        backgroundColor: colors.primary,
      },
      headerTintColor: '#fff',
      headerShadowVisible:false,
      headerTitleStyle: {
        
        fontFamily:fonts.primaryLight
      },
      headerLeft: (props) => (
        <View style={{paddingLeft: 0}}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack()
            }}>
            <Ionicons name="chevron-back" size={40} color="#fff" />
          </TouchableOpacity>
        </View>
      ),
      headerRight: (props) => (
       <></>
         ),
    })
  
  
  }>
		
      	<NotificationsStack.Screen
				name="Notifications"
				component={Notifications}
				options={{headerShown: true}}
			/>
		
	
		</NotificationsStack.Navigator>
	);
};
const ProfileStack = createNativeStackNavigator();
const ProfileStackScreen = () => {

	
	return (
		<ProfileStack.Navigator 
    screenOptions={({navigation, route}) => ({
      
      headerStyle: {
        backgroundColor: colors.primary,
      },
      headerTintColor: '#fff',
      headerShadowVisible:false,
      headerTitleStyle: {
        
        fontFamily:fonts.primaryLight
      },
      headerLeft: (props) => (
        <View style={{paddingLeft: 0}}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack()
            }}>
            <Ionicons name="chevron-back" size={40} color="#fff" />
          </TouchableOpacity>
        </View>
      ),
      headerRight: (props) => (
       <></>
         ),
    })
  
  
  }>
		
      	<ProfileStack.Screen
				name="Profile"
				component={Profile}
				options={{headerShown: true}}
			/>
			<ProfileStack.Screen
				name="EditProfile"
				component={UpdateProfile}
				options={{headerShown: true,title:"Edit Profile"}}
			/>
	
		</ProfileStack.Navigator>
	);
};



export default KidsDashboardScreen;