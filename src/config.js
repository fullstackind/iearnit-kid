export const  BASE_URL="http://139.59.27.198/projects/task-app/index.php/";
export const IMG_URL="http://139.59.27.198/projects/task-app/";



// export const  BASE_URL="http://192.168.18.3/works/inventix/task-app/index.php/";
// export const IMG_URL="http://192.168.18.3/works/inventix/task-app/";

export const trimString = function (string, length) {
    return string.length > length ? 
           string.substring(0, length) + '...' :
           string;
  };