/* eslint-disable */

import {StyleSheet, Dimensions, Platform} from 'react-native'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export const dimensions = {
  fullHeight: Dimensions.get('window').height,
  fullWidth: Dimensions.get('window').width
}
export const deviceWidth = Dimensions.get("window").width;
export const deviceHeight = Platform.OS === "ios"
    ? Dimensions.get("window").height
    : Dimensions.get("window").height


export const colors  = {
  primary:'#006192',
  secondary:"#fff",
  thirdary:'#FFC107',
  buttonPrimary: '#FFC107',
  buttonSecondary: '#0D3667',
  inputBorder:'#BAC0D0',
  inputPlaceholder:'#BAC0D0',
  buttonBlue: '#71dbf5',

  // InputColor:'#b3b3b3',
  buttonTextColor:'#FFF',
  buttonTextGrey:'#BAC0D0',


  fontLight:"#A0A0A0",
  fontBlue:"#4EB6CF",
  fontDark:'#000'
}
export const statusBarConfig  = {
  color:colors.primary,
  barStyle:"light-content",
  translucent:false
}
export const fonts  = {
  primaryLight: 'Roboto-Regular',
  primaryMedium: 'Roboto-Medium',
  primaryBold:'Roboto-Bold'

 
}
export const spacing = {
  sm: 10,
  md: 20,
  lg: 30,
  xl: 40,
  
}
export const fsize = {
  h1: RFPercentage(6),
  h2: RFPercentage(4),
  h3: RFPercentage(3),
  md: RFPercentage(2),
  lg: RFPercentage(2.3),
  xs: RFPercentage(1.4),
  sm: RFPercentage(1.8),
  
  
  xl: RFPercentage(3),
  xxl:RFPercentage(3.5)

}


export const images= {
// dashMainImage:require('../common/assets/images/concept-online-exam-internet-woman-sitting-near-online-form-survey-laptop_186332-587.jpeg'),
// placeholder:require('../common/assets/images/image-placeholder.jpeg'),
// medal:require('../common/assets/images/medal.png'),
// splashImage:require('../common/assets/images/goldprep.gif'),
logo:require('../assets/images/easecrm@1X.png'),
edit:require('../assets/images/editing.png'),
home:require('../assets/images/home.png'),
enquiries:require('../assets/images/enquiries.png'),
availability:require('../assets/images/availability.png'),
plus:require('../assets/images/plus.png'),
settings:require('../assets/images/settings.png'),
// checkBox:require('../common/assets/images/checked.png'),
// check:require('../common/assets/images/checked-simple.png'),
// home:require('../common/assets/images/home.png'),
// profile:require('../common/assets/images/user.png'),
ProfilePic:require('../assets/images/profile.jpeg'),
// cart:require('../common/assets/images/cart.png'),
// test:require('../common/assets/images/news.png'),
// logout:require('../common/assets/images/logout.png'),
// settings:require('../common/assets/images/settings.png'),
// users:require('../common/assets/images/users.png')
}
