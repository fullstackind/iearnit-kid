import {StyleSheet, Dimensions, Platform} from 'react-native'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { colors, fonts, spacing, fsize } from './base'





const rounded = {
    borderRadius: 8
};

const button = { 
   
    ...rounded
}
const normalText={
    fontFamily:fonts.primaryLight,
    fontSize:fsize.md,
    color:'#000'
}
const styles = StyleSheet.create({
    container:{
        flex:1,
      
      
    },
    subContainer:{
        flex:1,
        alignItems:"center",
        justifyContent:"center",
        paddingHorizontal:50,
        
    },
    curvedContainer:{
      
      flex:1,
    
      borderRadius:10,
      marginHorizontal:8,
      marginBottom:10,
      backgroundColor:colors.secondary,
     
    },
    col:{
      flex:1, 
    },
    center:{
        
        justifyContent:'space-between',
        alignItems:'center'
    },
    row_center:{
        flexDirection:"row",
        justifyContent:'space-between',
        alignItems:'center'
    },
    buttonTextStyle:{
        textAlign:'center',
       
        fontFamily:fonts.primaryMedium,textTransform:'uppercase'

    },
    button:{
        ...button,
        marginVertical:5
    },
    heading_text:{
        fontFamily:fonts.primaryLight,
        color:colors.fontBlue
    },
    normalText,
    instructionText:{
     ...normalText,

     lineHeight:22,
     fontFamily:fonts.primaryMedium
    },
    labelText:{
        ...normalText,
        fontSize:fsize.sm,
        width:'100%',
        fontFamily:fonts.primaryMedium
    },
    inputField: {
        borderWidth: 0,
        paddingLeft:10,
        borderRadius:8,
       
        fontFamily:fonts.primaryLight
      },
      formErrorLabel:{
        color: 'orange',
        fontSize:fsize.md-1,
        paddingVertical:4,
        fontFamily:fonts.primaryLight
      },
      spacer:{
         
          backgroundColor:"transparent"
      },
      dashTiles:{
          borderColor:'#f2f2f2',
          borderWidth:1,
          padding:15,
          flexDirection:"row",
          alignItems:"center",
          justifyContent:"space-between"

      },
      headText:{
        color:colors.secondary,
        fontFamily:fonts.primaryMedium,
        fontSize:fsize.h3
      },
      headTextLarge:{

        fontFamily:fonts.primaryMedium,
        fontSize:fsize.h2-8
      },
      headTextSmall:{
        color:"#183E6E",
        fontFamily:fonts.primaryMedium,
        fontSize:fsize.lg-2
      },
      highlightText:{
        color:colors.fontBlue,
        fontFamily:fonts.primaryMedium,
        fontSize:fsize.md
      },
      planTiles:{
        backgroundColor:'#F6F9FA', alignItems:"center",
        justifyContent:"center",paddingVertical:20,borderRadius:18
      },
      lightText:{
        color:colors.fontLight,
        fontSize:fsize.md,
        fontFamily:fonts.primaryLight,
        
    },
    imageIcon:{
        width:35,
        height:35
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22,
        backgroundColor:'rgba(0,0,0,.4)'
      },
      modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 0,
        width: 300,
        paddingLeft:20,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        position:'relative'
      },
      modalText: {
        fontSize:fsize.lg,
        fontFamily:fonts.primaryMedium,
        letterSpacing:1, 
        textAlign: "left",
        color:'#565757'
      },
      row: {
        flex: 1,
        marginLeft:5
        // justifyContent: "space-around"
    },
    item:{
      height: Dimensions.get('window').width / 6,// approximate a square
      alignItems:'center',
      justifyContent:"center",
      marginBottom:10,
      elevation:2,
      marginRight:5,
      flex:1
    },
    itemInvisible: {
      backgroundColor: 'transparent',
    },
    backgroundVideo: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
    }, 
    profileImage:{
      width:70,
      height:70,
      borderRadius:35,
      borderColor:"#fff",
      borderWidth:2
    }, 
    feedImage:{
      width:"100%",
      height:200,
      borderRadius:4,
      borderColor:"#fff",
      borderWidth:2,
  
    },
    couponImage:{
      width:"100%",
      height:80,
      borderRadius:8,
      borderColor:"#fff",
      borderWidth:2,
     
    } 

});







export {styles,colors,fonts,fsize,spacing};