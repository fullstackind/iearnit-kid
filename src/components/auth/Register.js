import React, { Component } from 'react';
import { View, Text, Image, StatusBar, ScrollView, TouchableOpacity,Picker } from 'react-native';
import { Formik } from 'formik';
import * as yup from 'yup';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import {images } from '../../styles/base';
import {styles, colors, fsize} from '../../styles/style'
import Button from '../../widgets/button'
import {TextField,Label} from '../../widgets/TextField'
import {Spacer} from '../../widgets/Spacer'
import ModalSelector from 'react-native-modal-selector'
import {Loader} from '../../widgets/Loader'
import { showMessage, hideMessage } from "react-native-flash-message";
const validationSchema = yup.object().shape({
  name: yup.string().required().matches(/^[aA-zZ\s]+$/, "Only alphabets are allowed for this field ").label(' Name'),
  email: yup.string().email().required().label(' Email'),
  contact_number: yup.string()
  .required()
  .matches(/^[0-9]+$/, "Must be only digits")
  .min(10, 'Must be exactly 10 digits')
  .max(10, 'Must be exactly 10 digits').label('Contact Number'),
  gender: yup.string().required().label('Gender'),
  password: yup.string().required().label('Password').matches(
    /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
    "Must Contain 8 Characters, One Uppercase,(A-Z) One Lowercase(a-z), One Number(0-9) and one special case Character"
  ),
  country_code: yup.string().required().label('Country Code'),
});
import axios from 'axios';
import { BASE_URL } from '../../config';
import { min } from 'react-native-reanimated';
export default class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error:[],
      user_type: this.props.route.params
      ? this.props.route.params.user_type
      : "kids",
    }; 
  }
  SigninApiCall=(values)=>{
    let url=BASE_URL+'api/children/signup';
    if(this.state.user_type =='parent'){
      url=BASE_URL+'api/parent/signup';
    }
    axios({
        method: 'post',
        url: url,
        headers: { 'Content-Type': 'application/json' },
        data:{ 
            "name":values.name, 
            "contact_number":values.contact_number, 
            "country_code":values.country_code,
          "email":values.email, 
          "gender":values.gender, 
          "password":values.password, 
        }
      })
      .then( (response)=> {

        // alert(JSON.stringify(response));
        
        if(response.data.success){
          this.setState({loading:false},()=>{
            showMessage({
              message: "Registration completed Successfully",
              type: "success",
              position:"top",
              duration:2000,
              // hideStatusBar:true,
              icon:"success",
            });
            this.formik.resetForm();
            this.props.navigation.navigate("Login",{user_type:this.state.user_type});
          });
  
  
          // this.props.saveLoggedUser(response.data.data).then((res)=>{
          //   this.setState({loading:false});
          // }).catch((err)=>{
          //   this.setState({loading:false});
          // });
          
        }else{
          this.setState({loading:false,error:response.data.error});
        }
      //   this.setState({loading:false});
      //   // alert(JSON.stringify(response));
      // console.log("dat",response.data)
      //   // this.setState({bookmarks:response.data.bookmark_items})
      //   // this.setState({completed:true});
    
        
      })
      .catch((error)=> {
        this.setState({loading:false,error:['something went wrong']});
        console.log('er',error);
       
        // this.setState({error:true});
    
      });

  }
  render() {
    let genderindex = 0;
    const genderdata = [
        { key: genderindex++, label: 'Male',value:'male' },
        { key: genderindex++, label: 'Female',value:'female' },
        // { key: genderindex++, label: 'others',value:'others' },
      ];
      let countryCodeindex = 0;
    const countryCodedata = [
        { key: countryCodeindex++, label: '+91',value:'+91' },
        { key: countryCodeindex++, label: '+971',value:'+971' },
        { key: countryCodeindex++, label: '+1',value:'+1' },
        { key: countryCodeindex++, label: '+44',value:'+44' },
        { key: countryCodeindex++, label: '+966',value:'+966' },
        { key: countryCodeindex++, label: '+65',value:'+65' },
      ];
    return (
      <View style={{...styles.container,backgroundColor:colors.primary}}>
        <ScrollView style={{...styles.container,backgroundColor:colors.primary}}>
  	<StatusBar
						backgroundColor={colors.primary}
						barStyle="light-content"
						translucent={false}
					/>
        <View style={{...styles.center,backgroundColor:colors.primary,paddingVertical:50}}>
        <Text style={{color:"#fff",fontSize:fsize.h2+2}}>{this.state.user_type =="kids"?"KIDS":"PARENT"}</Text>
        {/* <Image source={images.logo} style={{width:'60%',height:100}} resizeMode="contain"/> */}
        </View>
        <View style={{...styles.center,paddingVertical:30,paddingHorizontal:30,backgroundColor:"#fff",borderRadius:25}}>
        {/* <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}} style={{marginBottom:20}}><Text style={{...styles.heading_text}}>{'<< Back'}</Text></TouchableOpacity> */}
        <Text style={{marginBottom:20}}><Text style={{...styles.heading_text,fontSize:fsize.h3}}>Register</Text></Text>
        {this.state.error.length > 0 && this.state.error.map((item)=>(
           <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{item}</Text></View>
        ))}
        <Formik
         innerRef={(p) => (this.formik = p)}
      initialValues={{ name: '',email:'',contact_number:'',gender:'male', password:'',country_code:'+1' }}
      onSubmit={(values, {resetForm,actions}) => {
    //    console.log(values);
       this.setState({loader:true});
        // setTimeout(() => {
        //   actions.setSubmitting(false);
        // }, 1000);
        this.SigninApiCall(values);
        // resetForm();
      }}
      validationSchema={validationSchema}
    >
      {(formikProps) => (
        <>
       
        <Spacer/>
        <Label labelText="Name" color="#183E6E" textTransform="capitalize"/>
        <TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Name"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="name" onInputBlur={formikProps.handleBlur('name')} onInputChange={formikProps.handleChange('name')} value={formikProps.values.name}/>
        {formikProps.errors.name && formikProps.touched.name &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.name}</Text></View>}
         {formikProps.errors.name == null && <Spacer/> }

         <Label labelText="Email" color="#183E6E" textTransform="capitalize"/>
         <Spacer/>
        <TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Email"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="email" onInputBlur={formikProps.handleBlur('email')} onInputChange={formikProps.handleChange('email')} value={formikProps.values.email} />
        {formikProps.errors.email && formikProps.touched.email &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.email}</Text></View>}
         {formikProps.errors.email == null && <Spacer/> }
         <Spacer/>
         <View style={{...styles.row_center}}>
           
           <View style={{flex:3,marginRight:5}}>
              <View style={{width:"100%",borderColor:colors.inputBorder,borderWidth:1,backgroundColor:colors.secondary, borderRadius:8,alignSelf:'center'}}>
             
              {/* <Picker
               // ref={this.pickerRef}
               style={{color:"#000",height:50,}}
               selectedValue={formikProps.values.country_code}
               onValueChange={itemValue => formikProps.setFieldValue('country_code', itemValue)}
               >
               <Picker.Item label="+91" value="+91" />
               <Picker.Item label="+971" value="+971" />
               <Picker.Item label="+1" value="+1" />
               <Picker.Item label="+44" value="+44" />
               <Picker.Item label="+966" value="+966" />
               <Picker.Item label="+65" value="+65" />

             </Picker> */}
              <ModalSelector
                    data={countryCodedata}
                    initValue={formikProps.values.country_code}
                    onChange={(option)=>{ formikProps.setFieldValue('country_code', option.value)}} >
<TextField placeholderTextColor={colors.inputPlaceholder} marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2}
                        
                        editable={false}
                        placeholder="+91"
                        value={formikProps.values.country_code} />
                        </ModalSelector>
         </View>
       </View>

       <View style={{...styles.row_center,flex:4,backgroundColor:colors.secondary,paddingLeft:0, borderRadius:8,alignSelf:'center',marginLeft:5}}>
       <TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Contact Number"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="contact_number" onInputBlur={formikProps.handleBlur('contact_number')} onInputChange={formikProps.handleChange('contact_number')} value={formikProps.values.contact_number} keyboardType="phone-pad"/>
       </View>
 
       </View>



        
        {formikProps.errors.contact_number && formikProps.touched.contact_number &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.contact_number}</Text></View>}
         {formikProps.errors.contact_number == null && <Spacer/> }

         <Label labelText="Gender" color="#183E6E" textTransform="capitalize"/>
         <View style={{width:"100%"}}>
              
         {/* <Picker style={{width: "100%", height: 80}} itemStyle={{height: 80}}
               
              
               selectedValue={formikProps.values.gender}
               onValueChange={itemValue => formikProps.setFieldValue('gender', itemValue)}
               >
                 <Picker.Item label="Select" value="" />
               <Picker.Item label="Male" value="male" />
               <Picker.Item label="Female" value="female" />
               <Picker.Item label="Others" value="others" />
              
             </Picker> */}
                 <ModalSelector
                    data={genderdata}
                    initValue={formikProps.values.gender}
                    onChange={(option)=>{ formikProps.setFieldValue('gender', option.value)}} >
<TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2}
                        
                        editable={false}
                        placeholder="Gender"
                        value={formikProps.values.gender} />
                        </ModalSelector>
         </View>
         {formikProps.errors.gender && formikProps.touched.gender &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.gender}</Text></View>}
        {formikProps.errors.gender == null && <Spacer/> }

        <Label labelText="Password" color="#183E6E" textTransform="capitalize"/>
        <Spacer/>
        <TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Password"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="password" onInputBlur={formikProps.handleBlur('password')} onInputChange={formikProps.handleChange('password')} secureTextEntry showHideButton secureTextEntry value={formikProps.values.password}/>
        {formikProps.errors.password && formikProps.touched.password &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.password}</Text></View>}
         {formikProps.errors.password == null && <Spacer/> }




      
      
  



        <Spacer/>
        <TouchableOpacity style={{width:"100%"}} onPress={() =>{this.props.navigation.navigate("Login",{user_type:this.state.user_type})}}><Text style={{...styles.instructionText,color:colors.fontBlue,textAlign:'right'}}>Sign in</Text></TouchableOpacity>
        <Spacer/>
        
        <Spacer/>
        <Spacer/>
        <Button borderRadius={30}  text="Sign up" onPress={()=>formikProps.handleSubmit()} backgroundColor={colors.buttonBlue} textColor={colors.buttonTextColor}/>
       
        </>
        )}
       </Formik>
    
        </View>
        </ScrollView>
      </View>
    );
  }
}