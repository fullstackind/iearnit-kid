import React, { Component } from 'react';
import { View, Text, Image, StatusBar, ScrollView, TouchableOpacity } from 'react-native';
import { Formik } from 'formik';
import * as yup from 'yup';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import {images } from '../../styles/base';
import {styles, colors, fsize} from '../../styles/style'
import Button from '../../widgets/button'
import {TextField,Label} from '../../widgets/TextField'
import {Spacer} from '../../widgets/Spacer'
import {Loader} from '../../widgets/Loader'
import axios from 'axios';
import { BASE_URL } from '../../config';
import {saveLoggedUser} from '../../../action';
import {connect} from "react-redux";
const validationSchema = yup.object().shape({
  email: yup.string().email().required().label('Email'),
 
  password: yup.string().required().label('Password')
});
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error:'',
      loading:false,
      user_type: this.props.route.params
      ? this.props.route.params.user_type
      : "kids",
    }; 
  }
  SigninApiCall=(values)=>{



    let url=BASE_URL+'api/children/login';
    if(this.state.user_type =='parent'){
      url=BASE_URL+'api/parent/login';
    }
    axios({
      method: 'post',
      url: url,
      headers: { 'Content-Type': 'application/json' },
      data:{ 
        "email":values.email, 
        "password":values.password, 
      }
    })
    .then( (response)=> {
      // alert(JSON.stringify(response));
      if(response.data.success){
    


        this.props.saveLoggedUser(response.data.data).then((res)=>{
          this.setState({loading:false});
        }).catch((err)=>{
           this.setState({loading:false});
         });
        
      }else{
        this.setState({loading:false,error:response.data.error.error_message});
      }
      this.setState({loading:false});
      
  
      
    })
    .catch((error)=> {
      this.setState({loading:false,error:'somthing went wrong.server not responding please try again later'});
     
  
    });


  }
  render() {
    return (
      <View style={{...styles.container,backgroundColor:colors.primary}}>
        <ScrollView style={{...styles.container,backgroundColor:colors.primary}}>
  	<StatusBar
						backgroundColor={colors.primary}
						barStyle="light-content"
						translucent={false}
					/>
          {this.state.loading && (<Loader size={40}/>)}
        <View style={{...styles.center,backgroundColor:colors.primary,paddingVertical:50}}>
        <Text style={{color:"#fff",fontSize:fsize.h2+2}}>{this.state.user_type =="kids"?"KIDS":"PARENT"}</Text>
        
        {/* <Image source={images.logo} style={{width:'60%',height:100}} resizeMode="contain"/> */}
        </View>
        <View style={{...styles.center,paddingVertical:50,paddingHorizontal:30,backgroundColor:"#fff",borderRadius:25}}>
        <Text style={{marginBottom:50}}><Text style={{...styles.heading_text,fontSize:fsize.h2+2}}>Login</Text></Text>
        
        {this.state.error != "" && (
          <>
          
           <Spacer/>
           {!Array.isArray(this.state.error) && (
             <Text style={{...styles.formErrorLabel}}>{this.state.error}</Text>
           )}
          </>
        )}
       

        <Formik
      initialValues={{ email: '', password:'' }}
      onSubmit={(values, {resetForm,actions}) => {
      //  console.log(values);
       this.setState({loading:true,error:''});
        // setTimeout(() => {
        //   actions.setSubmitting(false);
        // }, 1000);
        this.SigninApiCall(values);
        resetForm();
      }}
      validationSchema={validationSchema}
    >
      {(formikProps) => (
        <>
       
        <Spacer/>
        <Label labelText="Email" color="#183E6E" textTransform="capitalize"/>
        <TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Email"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="email" onInputBlur={formikProps.handleBlur('email')} onInputChange={formikProps.handleChange('email')} value={formikProps.values.email}/>
        {formikProps.errors.email && formikProps.touched.email &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.email}</Text></View>}
         {formikProps.errors.email == null && <Spacer/> }

         <Label labelText="Password" color="#183E6E" textTransform="capitalize"/>
        <TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Password"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="password" onInputBlur={formikProps.handleBlur('password')} onInputChange={formikProps.handleChange('password')} secureTextEntry showHideButton secureTextEntry value={formikProps.values.password}/>
        {formikProps.errors.password && formikProps.touched.password &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.password}</Text></View>}
         {formikProps.errors.password == null && <Spacer/> }




   
        <Spacer/>
        <TouchableOpacity style={{width:"100%"}} onPress={() =>{}}><Text style={{...styles.instructionText,color:colors.fontBlue,textAlign:'right'}}>Forgot Your Password?</Text></TouchableOpacity>
        <Spacer/>
        <Spacer/>
        <Spacer/>
        <Button borderRadius={30}  text="Sign in" onPress={()=>formikProps.handleSubmit()} backgroundColor={colors.buttonBlue} textColor={colors.buttonTextColor}/>
        <Spacer/>
        <Spacer/>
        <Button borderRadius={30}  text="Sign up" onPress={()=>{this.setState({error:''},()=>{this.props.navigation.navigate("Register",{user_type:this.state.user_type})})}} backgroundColor={colors.buttonBlue} textColor={colors.buttonTextColor}/>

       
        </>
        )}
       </Formik>
    
        </View>
        </ScrollView>
      </View>
    );
  }
}
const mapStateToProps = state => {
	return {
		authUser: state.authUser,
		
	};
	
};
const mapDispatchToProps = dispatch => {
	return {
    saveLoggedUser: data => dispatch(saveLoggedUser(data)),
   
		
	
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);