import React, { Component } from 'react';
import { View, Text,TouchableOpacity,StatusBar,Image,Dimensions } from 'react-native';
import {styles, colors, fsize} from '../../styles/style'
import Button from '../../widgets/button'
import {images } from '../../styles/base';
const  deviceHeight = Dimensions.get('window').height;
export default class LaunchingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <View style={{...styles.container,backgroundColor:colors.primary,}}>
            	<StatusBar
						backgroundColor={colors.primary}
						barStyle="light-content"
						translucent={false}
					/>
                    <View style={{flex:1,}}>

                   
                        <View style={{...styles.center,backgroundColor:colors.primary,paddingVertical:deviceHeight/5}}>
        <Text style={{color:"#fff",fontSize:fsize.h2+2}}>I EARNED IT</Text>
        
        {/* <Image source={images.logo} style={{width:'60%',height:100}} resizeMode="contain"/> */}
        </View>

<View style={{flex:1,paddingVertical:10}}>
    <View style={{flexDirection:"row",justifyContent:"space-around"}}>
    <TouchableOpacity style={{height:35,
                            width:120,
                            justifyContent:"center",
                            borderRadius:20,alignItems:"center",
                            flexDirection:"row",backgroundColor:"#ffff",}}
                            onPress={()=>this.props.navigation.navigate("Login",{user_type:'kids'})}
                            >
        <Text>KIDS</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{height:35,
                            width:120,
                            justifyContent:"center",
                            borderRadius:20,alignItems:"center",
                            flexDirection:"row",backgroundColor:"#ffff",}}
                            onPress={()=>this.props.navigation.navigate("Login",{user_type:'parent'})}
                            >
                <Text>PARENT</Text>
                </TouchableOpacity>
    </View>
</View>

</View>

        {/* <View style={{paddingVertical:50,paddingHorizontal:30,backgroundColor:"#fff",borderRadius:25}}>

           
        <Text style={{marginBottom:50}}><Text style={{...styles.heading_text,fontSize:fsize.h2+2}}>Login</Text></Text>
        </View> */}
            </View>
    );
  }
}
