import React, { Component } from 'react';
import { Text, View,ActivityIndicator,StyleSheet } from 'react-native';
import {connect} from "react-redux";
import {appStateReset} from "../../../action";
import { colors, fsize} from '../../styles/style'
import AsyncStorage from "@react-native-async-storage/async-storage";
class Logout extends Component {
  constructor(props) {
    super(props);
    this.state = {
        loading:true, 
      
    };
   
  }
 
 

  componentDidMount() {
  
    this.props.appStateReset().then((res)=>{
          this.setState({loading:false});
          
         
        }).catch((err)=>{
            this.setState({loading:false});
        });
     
 
  }



  render() {
    return (
      <View style={styles.container}>
        {this.state.loading && (  <ActivityIndicator size="large" color="#FFF" />)}
     
    
      
      </View>
    );
  }
}
const mapStateToProps = state => {
    
  return {
   loggedUser:state.authUser
  };
};
const mapDispatchToProps = dispatch => {
  return {
  
    
      appStateReset: ()=> dispatch(appStateReset()),
     
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Logout);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor:colors.primary,
  },
 

});