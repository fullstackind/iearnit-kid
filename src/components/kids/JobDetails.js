import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, StyleSheet,ImageBackground,Modal,Dimensions,ScrollView,Image,TextInput } from 'react-native';
import {Picker} from '@react-native-picker/picker';
import { Formik } from 'formik';
import * as yup from 'yup';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';
import ImagePicker from 'react-native-image-crop-picker';
import {images, deviceHeight, statusBarConfig, fonts } from '../../styles/base';
import {styles, colors, fsize} from '../../styles/style'
import Button from '../../widgets/button'
import {TextField,Label} from '../../widgets/TextField'
import {Spacer} from '../../widgets/Spacer'
import {Loader} from '../../widgets/Loader'
import { Colors } from 'react-native/Libraries/NewAppScreen';
import {connect} from "react-redux";
import {BASE_URL,IMG_URL} from '../../config';
import axios from 'axios';
import {AlertMessage} from '../../widgets/AlertMessage';
import { showMessage, hideMessage } from "react-native-flash-message";
const validationSchema = yup.object().shape({
  description: yup.string().required().label('Description'),
 
  
});
class JobDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      job_id: this.props.route.params
      ? this.props.route.params.job_id
      : "",
     
      job_detail:{},
      task_images:[],
      loading:false,
      session_expire:false
   
    };
  }
  getJobDetail(){
    this.setState({loading:true});
    axios({
        method: 'get',
        url: BASE_URL+`api/assign-task-detail/${this.state.job_id}`,
        headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
       
      })
      .then( (response)=> {
        // alert(JSON.stringify(response));
        if(response.data.success){
      
          this.setState({job_detail:response.data.data,loading:false});
  
          
          
        }else{
          if(response.data.session_expired){
            this.setState({session_expire:true,loading:false});
          }else{
          this.setState({loading:false,error:response.data.error});
          }
        }
     
        
      })
      .catch((error)=> {
        this.setState({loading:false,error:error});
      
    
      });
}
componentDidMount(){
    this.getJobDetail();
}

taskImageUpload(){
  ImagePicker.openPicker({
    multiple: true
  }).then(images => {
    this.setState({task_images:images});
    // console.log(images);
  });
}
submitTask(values){
  this.setState({loading:true});
  const formData = new FormData();
  formData.append("assign_id",this.state.job_detail.id);
  formData.append("remarks",values.description);
  if(this.state.task_images.length > 0){
    this.state.task_images.forEach(file=>{
      let pathParts =file.path.split('/');
			formData.append("task_images[]", {
				uri: file.path.toString(),
				name: pathParts[pathParts.length - 1],
				type: file.mime
			});


     
    });
  }


  axios({
    method: "POST",
    url: BASE_URL + "api/children/submit-task",
    data: formData,
    headers: {
      "Content-Type": "multipart/form-data",
      "Token":this.props.authUser.token,
    }
  }).then( (response)=> {
    if(response.data.success){
      this.setState({task_images:[],loading:false},()=>{
        showMessage({
          message: "Job Submitted Successfully",
          type: "success",
          position:"top",
          duration:2000,
          // hideStatusBar:true,
          icon:"success",
        });
        this.getJobDetail();
      });
    }else{
      if(response.data.session_expired){
        this.setState({session_expire:true,loading:false});
      }else{
        this.setState({loading:false,error:response.data.error});
      }
    }
    // alert(JSON.stringify(response));
  }).catch((err)=>{
    this.setState({loading:false,error:error});
    // alert(JSON.stringify(err));
  });
  
  // alert(JSON.stringify(values))
}
  render() {
    return (
      <View style={{...styles.container}}>
        {this.state.session_expire && (<AlertMessage></AlertMessage>)}
        {this.state.loading && (<Loader size={30} backgroundColor={colors.secondary} color={colors.primary}></Loader>)}
         {Object.keys(this.state.job_detail).length !=0 && (
                <>
     <ScrollView>  

<View style={{...styles.container,paddingHorizontal:15 }}>
<Spacer/>
  <Text style={{...styles.headTextSmall,fontFamily:fonts.primaryMedium,color:colors.primary}}>{this.state.job_detail.task.title}</Text>

<View style={{flexDirection:"row",justifyContent:"space-between"}}>
  <View>
    <Text style={{...styles.headTextSmall,fontFamily:fonts.primaryMedium,color:colors.primary}}>Date :{this.state.job_detail.date}</Text>
  </View>
  <View>
    <Text style={{...styles.headTextSmall,fontFamily:fonts.primaryMedium,color:colors.primary}}>Points :{this.state.job_detail.task.point?this.state.job_detail.task.point:1000}</Text>
  </View>
</View>
<Spacer/>
<View>
  <Text style={{...styles.headTextSmall,fontFamily:fonts.primaryMedium,color:colors.primary}}>Instructions</Text>
  <Spacer/>
  <View>
    
  <Text style={{...styles.labelText,fontFamily:fonts.primaryLight,textAlign:'justify'}}>{this.state.job_detail.task.description}</Text>
  </View>
  <Spacer/>
</View>
{this.state.job_detail.task.images && this.state.job_detail.task.images.length > 0 && (
  <>
  <View>
<FlatList
            data={ this.state.job_detail.task.images }
            renderItem={ ({item}) =>
              <View style={{
                justifyContent: 'center',
              
                height: 100,
                marginRight: 5,
                // backgroundColor: '#7B1FA2'
              }}
                >
                <Image style={{width:80,height:80,borderRadius:4,overflow:"hidden"}} source={{uri:IMG_URL+'uploads/task/'+item.image}}/>
              </View> 
              
            
            }
            numColumns={3}
            keyExtractor={(item, index) => index.toString()}
         />
</View>
  </>
)}

{this.state.job_detail.status == 0 && (
  <>
 <Spacer/> 
<Spacer/>
<View>
  <Text style={{...styles.headTextSmall,fontFamily:fonts.primaryBold,color:colors.primary}}>Submit Job</Text>
</View>
<Spacer height={5}/>
<Formik
      initialValues={{ description: '' }}
      onSubmit={(values, {resetForm,actions}) => {
      //  console.log(values);
       this.setState({loading:true});
        // setTimeout(() => {
        //   actions.setSubmitting(false);
        // }, 1000);
        this.submitTask(values);
        resetForm();
      }}
      validationSchema={validationSchema}
    >
      {(formikProps) => (
        <>
<View style={{ }}>
						<Text style={{...styles.labelText,fontFamily:fonts.primaryLight,color:colors.fontDark}}>
            Comments(If Any)
						</Text>
         
						<TextInput
							style={{
								height: 80,
								borderColor: "gray",
								borderWidth: 1,
								fontSize: 12,
								color: "gray",
							}}
							
							clearTextOnFocus={true}
							
                            placeholder="Comments"
                            
                            value={formikProps.values.description}
                            placeholderTextColor="#a8a8a8"
                            onChangeText={formikProps.handleChange('description')}
                      onBlur={formikProps.handleBlur('description')}
						/>
            
					</View>
          {formikProps.errors.description && formikProps.touched.description &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.description}</Text></View>}
         {formikProps.errors.description == null && <Spacer/> }
      
          {this.state.task_images.length > 0 && (
     <View>
     <FlatList
                 data={ this.state.task_images }
                 renderItem={ ({item}) =>
                   <View style={{
                     justifyContent: 'center',
                   
                     height: 100,
                     marginRight: 5,
                     // backgroundColor: '#7B1FA2'
                   }}
                     >
                     <Image style={{width:80,height:80,borderRadius:4,overflow:"hidden"}} source={{uri:item.path}}/>
                   </View> 
                   
                 
                 }
                 numColumns={3}
                 keyExtractor={(item, index) => index.toString()}
              />
     </View>
          )}
          <Spacer/>
                <TouchableOpacity style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center",height:60,backgroundColor:colors.buttonGrey,borderStyle:'dotted',borderWidth:2,borderColor:"#c2c2c2",borderRadius:1 }}  onPress={()=>{this.taskImageUpload()}}>
                <Feather name="image" size={40} color={colors.fontLight} />
              <Text style={{...styles.lightText,fontSize:fsize.lg,paddingLeft:10}}>Add Images</Text>
            </TouchableOpacity> 
            <Spacer/>
            <Spacer/>
            <Spacer/>
<View style={{...styles.row_center}}>

       
  <TouchableOpacity style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center",height:50,backgroundColor:colors.buttonPrimary}} onPress={()=>{formikProps.handleSubmit()}}>
    <Text style={{...styles.buttonTextStyle,color:colors.secondary}}>Submit</Text>
  </TouchableOpacity>
  </View>
        </>
      )}
      </Formik>
      </>
)}
{this.state.job_detail.status != 0 && (
  <>
  <View>
  <Text>Job Submition Details</Text>
</View>
<View>
  <View>
  <Text>Description:{this.state.job_detail.remarks}</Text>
  </View>
  <View>
   <View>
     <Text> Uploaded Images</Text>
   </View>
   {this.state.job_detail.assigned_task_images.length > 0 && (
     <View>
     <FlatList
                 data={ this.state.job_detail.assigned_task_images}
                 renderItem={ ({item}) =>
                   <View style={{
                     justifyContent: 'center',
                     
                     height: 100,
                     margin: 5,
                     // backgroundColor: '#7B1FA2'
                   }}
                     >
                     <Image style={{width:100,height:100,borderRadius:4,overflow:"hidden"}} source={{uri:IMG_URL+'uploads/task/assign/'+item.name}}/>
                   </View> 
                   
                 
                 }
                 numColumns={3}
                 keyExtractor={(item, index) => index.toString()}
              />
     </View>
          )}
  </View>
 
</View>
  </>
)}

</View>
        
            </ScrollView>
            </>
         )}
      </View>
    );
  }
}
const mapStateToProps = state => {
	return {
		authUser: state.authUser,
		
	};
	
};


export default connect(mapStateToProps, null)(JobDetails);