import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
  Button,
} from 'react-native';
import {connect} from "react-redux";
import {BASE_URL,IMG_URL} from '../config';
import axios from 'axios';
import { showMessage, hideMessage } from "react-native-flash-message";
class UserDetail extends Component {

  constructor(props) {
    super(props);
    this.state={
        user_id: this.props.route.params
        ? this.props.route.params.user_id
        : "",
        find_user: this.props.route.params
        ? this.props.route.params.find_user
        : false,
        status: this.props.route.params.status
        ? this.props.route.params.status
        : "",
        request_type: this.props.route.params.request_type
        ? this.props.route.params.request_type
        : "",
        request_id: this.props.route.params.request_id
        ? this.props.route.params.request_id
        : "",
        user_detail:{},
    }
  }
getUserDetail(){
    axios({
        method: 'post',
        url: BASE_URL+'api/children/user-detail',
        headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
        data:{ 
          "user_id":this.state.user_id, 
        }
      })
      .then( (response)=> {
        // alert(JSON.stringify(response));
        if(response.data.success){
      
          this.setState({user_detail:response.data.data,loading:false});
  
          
          
        }else{
          this.setState({loading:false,error:response.data.error});
        }
     
        
      })
      .catch((error)=> {
        this.setState({loading:false,error:error});
      
    
      });
}
componentDidMount(){
    this.getUserDetail();
}
sendRequets(){
  axios({
    method: 'post',
    url: BASE_URL+'api/children/send-connection-request',
    headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
    data:{ 
      "requested_userid":this.props.authUser.userData.id, 
      "connected_userid":this.state.user_id
    }
  })
  .then( (response)=> {
    // alert(JSON.stringify(response));
    if(response.data.success){
  
      this.setState({loading:false},()=>{
        showMessage({
          message: "Request Sended Successfully",
          type: "success",
          position:"top",
          duration:2000,
          // hideStatusBar:true,
          icon:"success",
        });
        this.props.navigation.goBack();
      });

      
      
    }else{
      this.setState({loading:false,error:response.data.error});
    }
 
    
  })
  .catch((error)=> {
    this.setState({loading:false,error:error});
  

  });
}
cancelRequest(){
  axios({
    method: 'post',
    url: BASE_URL+'api/children/cancel-connection-request',
    headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
    data:{ 
      "request_id":this.state.request_id, 
      
    }
  })
  .then( (response)=> {
    // alert(JSON.stringify(response));
    if(response.data.success){
  
      this.setState({loading:false},()=>{
        showMessage({
          message: "Request Cancelled Successfully",
          type: "success",
          position:"top",
          duration:2000,
          // hideStatusBar:true,
          icon:"success",
        });
        this.props.navigation.goBack();
        // this.props.navigation.navigate("Users");
      });

      
      
    }else{
      this.setState({loading:false,error:response.data.error});
    }
 
    
  })
  .catch((error)=> {
    this.setState({loading:false,error:error});
  

  });
}
acceptRequest(){
  axios({
    method: 'post',
    url: BASE_URL+'api/children/update-connection-request',
    headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
    data:{ 
      "request_id":this.state.request_id, 
      "status":1
      
    }
  })
  .then( (response)=> {
    // alert(JSON.stringify(response));
    if(response.data.success){
  
      this.setState({loading:false},()=>{
        showMessage({
          message: "Request Accepted Successfully",
          type: "success",
          position:"top",
          duration:2000,
          // hideStatusBar:true,
          icon:"success",
        });
        this.props.navigation.goBack();
        // this.props.navigation.navigate("Users");
      });

      
      
    }else{
      this.setState({loading:false,error:response.data.error});
    }
 
    
  })
  .catch((error)=> {
    this.setState({loading:false,error:error});
  

  });
}
rejectRequest(){
  axios({
    method: 'post',
    url: BASE_URL+'api/children/update-connection-request',
    headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
    data:{ 
      "request_id":this.state.request_id, 
      "status":2
      
    }
  })
  .then( (response)=> {
    // alert(JSON.stringify(response));
    if(response.data.success){
  
      this.setState({loading:false},()=>{
        showMessage({
          message: "Request Rejected Successfully",
          type: "success",
          position:"top",
          duration:2000,
          // hideStatusBar:true,
          icon:"success",
        });
        this.props.navigation.goBack();
        // this.props.navigation.navigate("Users");
      });

      
      
    }else{
      this.setState({loading:false,error:response.data.error});
    }
 
    
  })
  .catch((error)=> {
    this.setState({loading:false,error:error});
  

  });
}
  clickEventListener() {
    Alert.alert("Success", "Product has beed added to cart")
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
            {Object.keys(this.state.user_detail).length !=0 && (
                <>
                
          <View style={{alignItems:'center', marginHorizontal:30}}>
            <Image style={styles.productImg} source={{uri:IMG_URL+'uploads/profile/'+this.state.user_detail.profile_pic}}/>
            <Text style={styles.name}>{this.state.user_detail.name}</Text>
            {/* <Text style={styles.price}>{this.state.user_detail.email}</Text>
            <Text style={styles.price}>{this.state.user_detail.contact_number}</Text> */}
            <Text style={styles.description}>
              {this.state.user_detail.email}
            </Text>
            <Text style={styles.description}>
              {this.state.user_detail.contact_number}
            </Text>
          </View>
         
        
        
          <View style={styles.separator}></View>
          <View style={styles.addToCarContainer}>
            {this.state.find_user && (
              <>
               <TouchableOpacity style={[styles.shareButton,{backgroundColor:"#AF7AC5"}]} onPress={()=> this.sendRequets()}>
              <Text style={styles.shareButtonText}>Send Connection Request</Text>  
            </TouchableOpacity>
              </>
            )}
              {!this.state.find_user && this.state.request_type !=="" && (
              <>
              {this.state.request_type == "sended" && this.state.status == 0 && (
                <>
                 <TouchableOpacity style={[styles.shareButton,{backgroundColor:"#ABB2B9"}]} onPress={()=> {}} disabled={true}>
              <Text style={styles.shareButtonText}>Requested</Text>  
            </TouchableOpacity>
            <TouchableOpacity style={[styles.shareButton,{backgroundColor:"#EC7063"}]} onPress={()=> this.cancelRequest()}>
              <Text style={styles.shareButtonText}>Cancel</Text>  
            </TouchableOpacity>
                </>
              )}
                {this.state.request_type == "received" && this.state.status == 0 && (
                <>
                 <TouchableOpacity style={[styles.shareButton,{backgroundColor:"#58D68D"}]} onPress={()=> this.acceptRequest()}>
              <Text style={styles.shareButtonText}>Accept</Text>  
            </TouchableOpacity>
            <TouchableOpacity style={[styles.shareButton,{backgroundColor:"#EC7063"}]} onPress={()=> this.rejectRequest()}>
              <Text style={styles.shareButtonText}>Reject</Text>  
            </TouchableOpacity>
                </>
              )}
              </>
            )}
            
          </View> 
          </>
            )}
        </ScrollView>
      </View>
    );
  }
}
const mapStateToProps = state => {
	return {
		authUser: state.authUser,
		
	};
	
};


export default connect(mapStateToProps, null)(UserDetail);
const styles = StyleSheet.create({
  container:{
    flex:1,
    marginTop:20,
  },
  productImg:{
    width:200,
    height:200,
  },
  name:{
    fontSize:28,
    color:"#696969",
    fontWeight:'bold'
  },
  price:{
    marginTop:10,
    fontSize:18,
    color:"green",
    fontWeight:'bold'
  },
  description:{
    textAlign:'center',
    marginTop:10,
    color:"#696969",
  },
  star:{
    width:40,
    height:40,
  },
  btnColor: {
    height:30,
    width:30,
    borderRadius:30,
    marginHorizontal:3
  },
  btnSize: {
    height:40,
    width:40,
    borderRadius:40,
    borderColor:'#778899',
    borderWidth:1,
    marginHorizontal:3,
    backgroundColor:'white',

    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  starContainer:{
    justifyContent:'center', 
    marginHorizontal:30, 
    flexDirection:'row', 
    marginTop:20
  },
  contentColors:{ 
    justifyContent:'center', 
    marginHorizontal:30, 
    flexDirection:'row', 
    marginTop:20
  },
  contentSize:{ 
    justifyContent:'center', 
    marginHorizontal:30, 
    flexDirection:'row', 
    marginTop:20
  },
  separator:{
    height:2,
    backgroundColor:"#eeeeee",
    marginTop:20,
    marginHorizontal:30
  },
  shareButton: {
    marginTop:10,
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius:30,
    // backgroundColor: "#00BFFF",
  },
  shareButtonText:{
    color: "#FFFFFF",
    fontSize:20,
  },
  addToCarContainer:{
    marginHorizontal:30
  }
});    

