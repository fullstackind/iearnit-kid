import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
  Button,
} from 'react-native';
import {connect} from "react-redux";
import {BASE_URL,IMG_URL} from '../../config';
import axios from 'axios';
import { showMessage, hideMessage } from "react-native-flash-message";
import{getPoints} from '../../../action';
import {AlertMessage} from '../../widgets/AlertMessage';
import {Loader} from '../../widgets/Loader'
import { colors, fsize} from '../../styles/style'
class RedeemCoupon extends Component {

  constructor(props) {
    super(props);
    this.state={
        coupon_data: this.props.route.params
        ? this.props.route.params.data
        : "",
        session_expire:false, 
        loading:false,
    }
  }

componentDidMount(){
    // this.getUserDetail();
}



purchaseCoupon= ()=>{

  if(!this.state.coupon_data.purchased){
    if(this.state.coupon_data.points <= this.props.authUser.points.earned_points)  {
  this.setState({loading:true});
  axios({
    method: 'post',
    url: BASE_URL+'api/children/purchase-coupon',
    headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
    data:{ 
      "user_id":this.props.authUser.userData.id, 
      "coupon_id":this.state.coupon_data.id,
      "point":this.state.coupon_data.points
      
    }
  })
  .then( (response)=> {
    // alert(JSON.stringify(response));
    if(response.data.success){
  
      this.setState({loading:false},()=>{
        showMessage({
          message: "Coupon Purchased Successfully",
          type: "success",
          position:"top",
          duration:2000,
          // hideStatusBar:true,
          icon:"success",
        });
        this.props.getPoints(this.props.authUser.token,this.props.authUser.userData.id);
        // this.props.navigation.goBack();
        // this.props.navigation.navigate("Users");
      });

      
      
    }else{
      if(response.data.session_expired){
        this.setState({session_expire:true,loading:false});
      }else{
      this.setState({loading:false,error:response.data.error});
      }
    }
 
    
  })
  .catch((error)=> {
    this.setState({loading:false,error:error});
  

  });
}else{
  showMessage({
    message: "You don't have enough points to redeem this coupon.",
    type: "warning",
    position:"top",
    duration:2000,
    // hideStatusBar:true,
    icon:"warning",
  });
}
}
}
  clickEventListener() {
    Alert.alert("Success", "Product has beed added to cart")
  }

  render() {
    return (
      <View style={styles.container}>
 {this.state.session_expire && (<AlertMessage></AlertMessage>)}
        {this.state.loading && (<Loader size={30} backgroundColor={colors.secondary} color={colors.primary}></Loader>)}
        <ScrollView>
       
            {Object.keys(this.state.coupon_data).length !=0 && (
                <>
                
          <View style={{alignItems:'center', marginHorizontal:30}}>
            <Image style={styles.productImg} source={{uri:IMG_URL+'uploads/coupon/'+this.state.coupon_data.image}}/>
            <Text style={styles.name}>{this.state.coupon_data.title}</Text>
            <Text style={styles.description}>{this.state.coupon_data.description}</Text>
            
            <Text style={styles.description}>
            {this.state.coupon_data.points}  Points Needed to Redeem This Coupon
            </Text>
            <Text style={styles.description}>
              You Have  {this.props.authUser.points.earned_points} Points
            </Text>
            
          </View>
         
        
        
          <View style={styles.separator}></View>
          <View style={styles.addToCarContainer}>

            {this.state.coupon_data.purchased && (
              <>
                <TouchableOpacity style={[styles.shareButton,{backgroundColor:"green"}]} onPress={()=> {}}disabled={true}>
              <Text style={styles.shareButtonText}>Redeemed</Text>  
            </TouchableOpacity>
              </>
            )}
{!this.state.coupon_data.purchased && (
              <>
          <TouchableOpacity style={[styles.shareButton,{backgroundColor:((this.state.coupon_data.points <= this.props.authUser.points.earned_points) && !this.state.coupon_data.purchased )?"#AF7AC5":"#ABB2B9"}]} onPress={()=> this.purchaseCoupon()}>
              <Text style={styles.shareButtonText}>Redeeem</Text>  
            </TouchableOpacity>
            </>
            )}

              </View>
        
          </>
            )}
        </ScrollView>
      </View>
    );
  }
}
const mapStateToProps = state => {
	return {
		authUser: state.authUser,
		
	};
	
};
const mapDispatchToProps = dispatch => {
	return {
        getPoints: (token,user_id) => dispatch(getPoints(token,user_id)),
   
		
	
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(RedeemCoupon);
const styles = StyleSheet.create({
  container:{
    flex:1,
    marginTop:20,
  },
  productImg:{
    width:200,
    height:200,
  },
  name:{
    fontSize:28,
    color:"#696969",
    fontWeight:'bold'
  },
  price:{
    marginTop:10,
    fontSize:18,
    color:"green",
    fontWeight:'bold'
  },
  description:{
    textAlign:'center',
    marginTop:10,
    color:"#696969",
  },
  star:{
    width:40,
    height:40,
  },
  btnColor: {
    height:30,
    width:30,
    borderRadius:30,
    marginHorizontal:3
  },
  btnSize: {
    height:40,
    width:40,
    borderRadius:40,
    borderColor:'#778899',
    borderWidth:1,
    marginHorizontal:3,
    backgroundColor:'white',

    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  starContainer:{
    justifyContent:'center', 
    marginHorizontal:30, 
    flexDirection:'row', 
    marginTop:20
  },
  contentColors:{ 
    justifyContent:'center', 
    marginHorizontal:30, 
    flexDirection:'row', 
    marginTop:20
  },
  contentSize:{ 
    justifyContent:'center', 
    marginHorizontal:30, 
    flexDirection:'row', 
    marginTop:20
  },
  separator:{
    height:2,
    backgroundColor:"#eeeeee",
    marginTop:20,
    marginHorizontal:30
  },
  shareButton: {
    marginTop:10,
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius:30,
    // backgroundColor: "#00BFFF",
  },
  shareButtonText:{
    color: "#FFFFFF",
    fontSize:20,
  },
  addToCarContainer:{
    marginHorizontal:30
  }
});    

