import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
  // Button,
} from 'react-native';
import {connect} from "react-redux";
import {BASE_URL,IMG_URL} from '../../config';
import {styles, colors, fsize} from '../../styles/style'
import Button from '../../widgets/button';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';
import axios from 'axios';
import { showMessage, hideMessage } from "react-native-flash-message";
import {AlertMessage} from '../../widgets/AlertMessage';
import {Loader} from '../../widgets/Loader'
class UserDetail extends Component {

  constructor(props) {
    super(props);
    this.state={
        user_id: this.props.route.params
        ? this.props.route.params.user_id
        : "",
        find_user: this.props.route.params
        ? this.props.route.params.find_user
        : false,
        status: this.props.route.params.status
        ? this.props.route.params.status
        : "",
        request_type: this.props.route.params.request_type
        ? this.props.route.params.request_type
        : "",
        request_id: this.props.route.params.request_id
        ? this.props.route.params.request_id
        : "",
        user_detail:{},
        session_expire:false,
        loading:false,
    }
  }
getUserDetail(){
  this.setState({loading:true});
    axios({
        method: 'post',
        url: BASE_URL+'api/children/user-detail',
        headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
        data:{ 
          "user_id":this.state.user_id, 
        }
      })
      .then( (response)=> {
        // alert(JSON.stringify(response));
        if(response.data.success){
      
          this.setState({user_detail:response.data.data,loading:false});
  
          
          
        }else{
          if(response.data.session_expired){
            this.setState({session_expire:true,loading:false});
          }else{
            this.setState({loading:false,error:response.data.error});
          }
          
        }
     
        
      })
      .catch((error)=> {
        this.setState({loading:false,error:error});
      
    
      });
}
componentDidMount(){
    this.getUserDetail();
}
sendRequets(){
  this.setState({loading:true});
  axios({
    method: 'post',
    url: BASE_URL+'api/children/send-connection-request',
    headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
    data:{ 
      "requested_userid":this.props.authUser.userData.id, 
      "connected_userid":this.state.user_id
    }
  })
  .then( (response)=> {
    // alert(JSON.stringify(response));
    if(response.data.success){
  
      this.setState({loading:false},()=>{
        showMessage({
          message: "Request Sent",
          type: "success",
          position:"top",
          duration:2000,
          // hideStatusBar:true,
          icon:"success",
        });
        this.props.navigation.goBack();
      });

      
      
    }else{
      if(response.data.session_expired){
        this.setState({session_expire:true,loading:false});
      }else{

      this.setState({loading:false,error:response.data.error});
      }
    }
 
    
  })
  .catch((error)=> {
    this.setState({loading:false,error:error});
  

  });
}
cancelRequest(){
  this.setState({loading:true});
  axios({
    method: 'post',
    url: BASE_URL+'api/children/cancel-connection-request',
    headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
    data:{ 
      "request_id":this.state.request_id, 
      
    }
  })
  .then( (response)=> {
    // alert(JSON.stringify(response));
    if(response.data.success){
  
      this.setState({loading:false},()=>{
        showMessage({
          message: "Request Cancelled ",
          type: "success",
          position:"top",
          duration:2000,
          // hideStatusBar:true,
          icon:"success",
        });
        this.props.navigation.goBack();
        // this.props.navigation.navigate("Users");
      });

      
      
    }else{
      if(response.data.session_expired){
        this.setState({session_expire:true,loading:false});
      }else{
      this.setState({loading:false,error:response.data.error});
      }
    }
 
    
  })
  .catch((error)=> {
    this.setState({loading:false,error:error});
  

  });
}
acceptRequest(){
  this.setState({loading:true});
  axios({
    method: 'post',
    url: BASE_URL+'api/children/update-connection-request',
    headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
    data:{ 
      "request_id":this.state.request_id, 
      "status":1
      
    }
  })
  .then( (response)=> {
    // alert(JSON.stringify(response));
    if(response.data.success){
  
      this.setState({loading:false},()=>{
        showMessage({
          message: "Request Accepted ",
          type: "success",
          position:"top",
          duration:2000,
          // hideStatusBar:true,
          icon:"success",
        });
        this.props.navigation.goBack();
        // this.props.navigation.navigate("Users");
      });

      
      
    }else{
      if(response.data.session_expired){
        this.setState({session_expire:true,loading:false});
      }else{
      this.setState({loading:false,error:response.data.error});
      }
    }
 
    
  })
  .catch((error)=> {
    this.setState({loading:false,error:error});
  

  });
}
rejectRequest(){
  this.setState({loading:true});
  axios({
    method: 'post',
    url: BASE_URL+'api/children/update-connection-request',
    headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
    data:{ 
      "request_id":this.state.request_id, 
      "status":2
      
    }
  })
  .then( (response)=> {
    // alert(JSON.stringify(response));
    if(response.data.success){
  
      this.setState({loading:false},()=>{
        showMessage({
          message: "Request Rejected ",
          type: "success",
          position:"top",
          duration:2000,
          // hideStatusBar:true,
          icon:"success",
        });
        this.props.navigation.goBack();
        // this.props.navigation.navigate("Users");
      });

      
      
    }else{
      if(response.data.session_expired){
        this.setState({session_expire:true,loading:false});
      }else{
      this.setState({loading:false,error:response.data.error});
      }
    }
 
    
  })
  .catch((error)=> {
    this.setState({loading:false,error:error});
  

  });
}
  clickEventListener() {
    Alert.alert("Success", "Product has beed added to cart")
  }

  render() {
    return (
      <View style={style.container}>
        {this.state.session_expire && (<AlertMessage></AlertMessage>)}
        {this.state.loading && (<Loader size={30} backgroundColor={colors.secondary} color={colors.primary}></Loader>)}
        <ScrollView>
        
       
            {Object.keys(this.state.user_detail).length !=0 && (
                <>
                
          <View style={{alignItems:'center', marginHorizontal:30}}>
            <Image style={{...styles.profileImage}} source={{uri:IMG_URL+'uploads/profile/'+this.state.user_detail.profile_pic}}/>
            <View style={{flex:1}}>
            <Text style={{...styles.headText,color:colors.primary,textAlign:"center"}}>{this.state.user_detail.name}</Text>
            </View>
            {/* <Text style={style.price}>{this.state.user_detail.email}</Text>
            <Text style={style.price}>{this.state.user_detail.contact_number}</Text> */}


      
          </View>
         
          <View style={{...styles.curvedContainer,marginTop:10,elevation:.5,paddingHorizontal:10}}>
          <View style={{...styles.row_center,justifyContent:'flex-start',paddingVertical:15,borderBottomColor:"#f2f2f2",borderBottomWidth:1}}>
            <Feather name="user" size={20} color={colors.thirdary}/>
            <Text style={{...styles.lightText,textTransform:'capitalize',marginLeft:10}}>
            {this.state.user_detail.name}
            </Text>
            </View>
            <View style={{...styles.row_center,justifyContent:'flex-start',paddingVertical:15,borderBottomColor:"#f2f2f2",borderBottomWidth:1}}>
            <FontAwesome5 name="birthday-cake" size={20} color={colors.thirdary}/>
            <Text style={{...styles.lightText,marginLeft:10}}>
            {this.state.user_detail.dob != null ?this.state.user_detail.dob:'NA'}
            </Text>
            </View>
            <View style={{...styles.row_center,justifyContent:'flex-start',paddingVertical:15,borderBottomColor:"#f2f2f2",borderBottomWidth:1}}>
            <Feather name="mail" size={20} color={colors.thirdary}/>
            <Text style={{...styles.lightText,textTransform:'lowercase',marginLeft:10}}>
              {this.state.user_detail.email}
            </Text>
            </View>
            <View style={{...styles.row_center,justifyContent:'flex-start',paddingVertical:15,borderBottomColor:"#f2f2f2",borderBottomWidth:1}}>
            <Feather name="phone" size={20} color={colors.thirdary}/>
            <Text style={{...styles.lightText,textTransform:'lowercase',marginLeft:10}}>
            {this.state.user_detail.country_code != null && (
                this.state.user_detail.country_code
              )}
            {" "+this.state.user_detail.contact_number}
            </Text>
            </View>
            <View style={{...styles.row_center,justifyContent:'flex-start',paddingVertical:15,borderBottomColor:"#f2f2f2",borderBottomWidth:1}}>
            <Feather name="map" size={20} color={colors.thirdary}/>
            <Text style={{...styles.lightText,marginLeft:10}}>
            {this.state.user_detail.address != null ?this.state.user_detail.address:'NA'}
            </Text>
            </View>
            </View>  
        
          <View style={style.separator}></View>
          <View style={style.addToCarContainer}>
            {this.state.find_user && (
              <>
               <Button text="Connect" width={'100%'} backgroundColor={colors.buttonPrimary} textColor={colors.secondary} onPress={()=> this.sendRequets()}/>
               {/* <TouchableOpacity style={[style.shareButton,{backgroundColor:"#AF7AC5"}]} onPress={()=> this.sendRequets()}>
              <Text style={style.shareButtonText}>Send Connection Request</Text>  
            </TouchableOpacity> */}
              </>
            )}
              {!this.state.find_user && this.state.request_type !=="" && (
              <>
              {this.state.request_type == "sended" && this.state.status == 0 && (
                 <View style={{...styles.row_center}}>
                 <Button text="Requested" width={'49%'} backgroundColor={colors.buttonPrimary} textColor={colors.secondary} onPress={()=> {}}/>
   
             <Button text="Cancel" width={'49%'} backgroundColor="#EC7063" textColor={colors.secondary} onPress={()=> this.cancelRequest()}/>
         
               </View>
          
              )}
                {this.state.request_type == "received" && this.state.status == 0 && (
                <View style={{...styles.row_center}}>
                  <Button text="Accept" width={'49%'} backgroundColor={colors.buttonPrimary} textColor={colors.secondary} onPress={()=> this.acceptRequest()}/>
    
              <Button text="Reject" width={'49%'} backgroundColor="#EC7063" textColor={colors.secondary} onPress={()=> this.rejectRequest()}/>
          
                </View>
              )}
              </>
            )}
            
          </View> 
          </>
            )}
        </ScrollView>
      </View>
    );
  }
}
const mapStateToProps = state => {
	return {
		authUser: state.authUser,
		
	};
	
};


export default connect(mapStateToProps, null)(UserDetail);
const style = StyleSheet.create({
  container:{
    flex:1,
    marginTop:20,
  },
  productImg:{
    width:200,
    height:200,
  },
  name:{
    fontSize:28,
    color:"#696969",
    fontWeight:'bold'
  },
  price:{
    marginTop:10,
    fontSize:18,
    color:"green",
    fontWeight:'bold'
  },
  description:{
    textAlign:'center',
    marginTop:10,
    color:"#696969",
  },
  star:{
    width:40,
    height:40,
  },
  btnColor: {
    height:30,
    width:30,
    borderRadius:30,
    marginHorizontal:3
  },
  btnSize: {
    height:40,
    width:40,
    borderRadius:40,
    borderColor:'#778899',
    borderWidth:1,
    marginHorizontal:3,
    backgroundColor:'white',

    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  starContainer:{
    justifyContent:'center', 
    marginHorizontal:30, 
    flexDirection:'row', 
    marginTop:20
  },
  contentColors:{ 
    justifyContent:'center', 
    marginHorizontal:30, 
    flexDirection:'row', 
    marginTop:20
  },
  contentSize:{ 
    justifyContent:'center', 
    marginHorizontal:30, 
    flexDirection:'row', 
    marginTop:20
  },
  separator:{
    height:2,
    backgroundColor:"#eeeeee",
    marginTop:20,
    marginHorizontal:30
  },
  shareButton: {
    marginTop:10,
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius:8,
    // backgroundColor: "#00BFFF",
  },
  shareButtonText:{
    color: "#FFFFFF",
    fontSize:20,
  },
  addToCarContainer:{
    marginHorizontal:10
  }
});    

