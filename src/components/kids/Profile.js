import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
  // Button,
} from 'react-native';
import {connect} from "react-redux";
import {BASE_URL,IMG_URL} from '../../config';
import {styles, colors, fsize} from '../../styles/style'
import Button from '../../widgets/button';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';
import axios from 'axios';
import { showMessage, hideMessage } from "react-native-flash-message";
import {AlertMessage} from '../../widgets/AlertMessage';
import {Loader} from '../../widgets/Loader'
class Profile extends Component {

  constructor(props) {
    super(props);
    this.state={
        user_detail:{},
        session_expire:false,
        loading:false,
    }
  }

componentDidMount(){
   
}


 
  render() {
    return (
      <View style={style.container}>
        {this.state.session_expire && (<AlertMessage></AlertMessage>)}
        {this.state.loading && (<Loader size={30} backgroundColor={colors.secondary} color={colors.primary}></Loader>)}
        <ScrollView>
        <View style={{alignItems:'center', marginHorizontal:30}}>
          {this.props.authUser.userData.avatar ==null && this.props.authUser.userData.profile_pic != null && (
            <>
            <Image style={{...styles.profileImage}} source={{uri:IMG_URL+'uploads/profile/'+this.props.authUser.userData.profile_pic}}/>
            </>
          )}
             { this.props.authUser.userData.profile_pic ==null && this.props.authUser.userData.avatar != null && (
            <>
            <Image style={{...styles.profileImage}} source={{uri:IMG_URL+'uploads/profile/avatar/'+this.props.authUser.userData.avatar}}/>
            </>
          )}
             <View style={{flex:1}}>
            <Text style={{...styles.headText,color:colors.primary,textAlign:"center"}}>{this.props.authUser.userData.name}</Text>
            </View>
            {/* <Text style={style.price}>{this.state.user_detail.email}</Text>
            <Text style={style.price}>{this.state.user_detail.contact_number}</Text> */}


      
          </View>
          <View style={{...styles.curvedContainer,marginTop:10,elevation:.5,paddingHorizontal:10}}>
          <View style={{...styles.row_center,justifyContent:'flex-start',paddingVertical:15,borderBottomColor:"#f2f2f2",borderBottomWidth:1}}>
            <Feather name="user" size={20} color={colors.primary}/>
            <Text style={{...styles.lightText,textTransform:'capitalize',marginLeft:10}}>
            {this.props.authUser.userData.name}
            </Text>
            </View>
            <View style={{...styles.row_center,justifyContent:'flex-start',paddingVertical:15,borderBottomColor:"#f2f2f2",borderBottomWidth:1}}>
            <FontAwesome5 name="birthday-cake" size={20} color={colors.primary}/>
            <Text style={{...styles.lightText,textTransform:'capitalize',marginLeft:10}}>
            {this.props.authUser.userData.dob != null ?this.props.authUser.userData.dob:'NA'}
            </Text>
            </View>
            <View style={{...styles.row_center,justifyContent:'flex-start',paddingVertical:15,borderBottomColor:"#f2f2f2",borderBottomWidth:1}}>
            <Feather name="mail" size={20} color={colors.primary}/>
            <Text style={{...styles.lightText,textTransform:'lowercase',marginLeft:10}}>
              {this.props.authUser.userData.email}
            </Text>
            </View>
            <View style={{...styles.row_center,justifyContent:'flex-start',paddingVertical:15,borderBottomColor:"#f2f2f2",borderBottomWidth:1}}>
            <Feather name="phone" size={20} color={colors.primary}/>
            <Text style={{...styles.lightText,textTransform:'lowercase',marginLeft:10}}>
            {this.props.authUser.userData.country_code != null && (
                this.props.authUser.userData.country_code
              )}
            {" "+this.props.authUser.userData.contact_number}
            </Text>
            </View>
            <View style={{...styles.row_center,justifyContent:'flex-start',paddingVertical:15,borderBottomColor:"#f2f2f2",borderBottomWidth:1}}>
            <Feather name="map" size={20} color={colors.primary}/>
            <Text style={{...styles.lightText,marginLeft:10}}>
            {this.props.authUser.userData.address != null ?this.props.authUser.userData.address:'NA'}
            </Text>
            </View>
            </View>
            <View style={style.separator}></View>
            <View style={style.addToCarContainer}>
            <View style={{flexDirection:"row",justifyContent:"center"}}>
               <Button text="Edit" width={'90%'} backgroundColor={colors.buttonBlue} textColor={colors.secondary} onPress={()=> {this.props.navigation.navigate("EditProfile")}}/>
              </View>
             
          </View> 
         
        </ScrollView>
      </View>
    );
  }
}
const mapStateToProps = state => {
	return {
		authUser: state.authUser,
		
	};
	
};


export default connect(mapStateToProps, null)(Profile);
const style = StyleSheet.create({
  container:{
    flex:1,
    marginTop:20,
  },
  productImg:{
    width:200,
    height:200,
  },
  name:{
    fontSize:28,
    color:"#696969",
    fontWeight:'bold'
  },
  price:{
    marginTop:10,
    fontSize:18,
    color:"green",
    fontWeight:'bold'
  },
  description:{
    textAlign:'center',
    marginTop:10,
    color:"#696969",
  },
  star:{
    width:40,
    height:40,
  },
  btnColor: {
    height:30,
    width:30,
    borderRadius:30,
    marginHorizontal:3
  },
  btnSize: {
    height:40,
    width:40,
    borderRadius:40,
    borderColor:'#778899',
    borderWidth:1,
    marginHorizontal:3,
    backgroundColor:'white',

    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  starContainer:{
    justifyContent:'center', 
    marginHorizontal:30, 
    flexDirection:'row', 
    marginTop:20
  },
  contentColors:{ 
    justifyContent:'center', 
    marginHorizontal:30, 
    flexDirection:'row', 
    marginTop:20
  },
  contentSize:{ 
    justifyContent:'center', 
    marginHorizontal:30, 
    flexDirection:'row', 
    marginTop:20
  },
  separator:{
    height:2,
    backgroundColor:"#eeeeee",
    marginTop:20,
    marginHorizontal:30
  },
  shareButton: {
    marginTop:10,
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius:8,
    // backgroundColor: "#00BFFF",
  },
  shareButtonText:{
    color: "#FFFFFF",
    fontSize:20,
  },
  addToCarContainer:{
    marginHorizontal:10
  }
});    

