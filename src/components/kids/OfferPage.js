import React, { Component } from 'react'
import { Text, View,ActivityIndicator } from 'react-native'
import { WebView } from 'react-native-webview';
import {styles, colors, fsize} from '../../styles/style'
import {Loader} from '../../widgets/Loader'
export default class OfferPage extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
            <WebView
              startInLoadingState={true}
              renderLoading={() => <Loader size={30} backgroundColor={colors.secondary} color={colors.primary}></Loader>}

              source={{ uri: this.props.route.params
                ? this.props.route.params.link
                : "" }}
              style={{ marginTop: 0 }}
            />
            </View>
        )
    }
}
