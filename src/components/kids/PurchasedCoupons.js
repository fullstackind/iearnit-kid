import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, StyleSheet,ImageBackground,Modal,Dimensions,ScrollView,Linking } from 'react-native';
import {Picker} from '@react-native-picker/picker';
import { Formik } from 'formik';
import * as yup from 'yup';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';
import {images, deviceHeight, statusBarConfig, fonts } from '../../styles/base';
import {styles, colors, fsize} from '../../styles/style'
import Button from '../../widgets/button'
import {TextField,Label} from '../../widgets/TextField'
import {Spacer} from '../../widgets/Spacer'
import {Loader} from '../../widgets/Loader'
import { Colors } from 'react-native/Libraries/NewAppScreen';
import {connect} from "react-redux";
import {BASE_URL,IMG_URL} from '../../config';
import axios from 'axios';
import {AlertMessage} from '../../widgets/AlertMessage';
import { showMessage, hideMessage } from "react-native-flash-message";
class PurchasedCoupons extends Component {
  constructor(props) {
    super(props);
    this.state = {
      coupons:[],
    };
  }
  getActiveJobs(){
    this.setState({loading:true});
    axios({
      method: 'post',
      url: BASE_URL+'api/children/purchased-coupons',
      headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
      data:{ 
        "user_id":this.props.authUser.userData.id, 
      }
    })
    .then( (response)=> {
      // alert(JSON.stringify(response));
      if(response.data.success){
    
        this.setState({coupons:response.data.data,loading:false});

        
        
      }else{
        if(response.data.session_expired){
          this.setState({session_expire:true});
        }else{
        this.setState({loading:false,error:response.data.error});
        }
      }
   
      
    })
    .catch((error)=> {
      this.setState({loading:false,error:error});
    
  
    });
  
}
componentDidMount = () => {
  this._unsubscribe = this.props.navigation.addListener('focus', () => {
    this.getActiveJobs();
    });
  
};
componentWillUnmount() {
  this._unsubscribe();
  }
async openLink(url){
  const supported = await Linking.canOpenURL(url);
  if (supported) {
   
    await Linking.openURL(url);
  } else {
    showMessage({
      message: "Cannot open url. Please check this url is correct. ",
      type: "warning",
      position:"top",
      duration:2000,
      // hideStatusBar:true,
      icon:"warning",
    });
   
  }
}

  render() {
    return (
      <View style={{...styles.container}}>
         {this.state.session_expire && (<AlertMessage></AlertMessage>)}
           {this.state.loading && (<Loader size={30} backgroundColor={colors.secondary} color={colors.primary}></Loader>)}
        {this.state.coupons.length > 0 && (
          <>
         
        <FlatList
              style={{
               
               backgroundColor:"#f2f2f2",
               paddingVertical:10,
              
               borderRadius:10,
              

              }}
              data={this.state.coupons}
              scrollEnabled={true}
              renderItem={({item, index}) => {
       
                return (
                
                  <TouchableOpacity
                 
                      style={{
                        flex: 1,
                       
                        
                        paddingVertical: 5,
                       marginBottom:10,
                        backgroundColor: '#fff',
                        
                        marginHorizontal:5
                      }}
                      onPress={()=>{item.coupon.link !="" ?this.openLink(item.coupon.link):''}}
                    >
                     <View style={{padding:5,margin:5,flexDirection:"row"}}>
                     <ImageBackground style={{width:100,height:100,borderRadius:4,overflow:"hidden"}} source={{uri:IMG_URL+'uploads/coupon/'+item.coupon.image}}> 
                     </ImageBackground>
                      
                      
                      <View style={{flex:5,marginLeft:20,paddingVertical:10}}>
                        {/* <View style={{flex:1,alignItems:"center",justifyContent:"space-between",flexDirection:"row"}}>
                    <Text  style={{...styles.mediumText,color:colors.fontDark}}>{item.name}</Text>
                     <Text style={styles.time}>7.10pm</Text>
                     </View> */}
                     <Text  style={{...styles.headTextSmall}}>{item.coupon.title}</Text>
                     <Spacer height={5}/>
                     <Text style={{...styles.lightText}}>Points: {item.coupon.points}</Text>
                     <Spacer height={5}/>
                     <Text style={{...styles.lightText}}>Code: {item.coupon.coupon_code}</Text>
                     {/* <Spacer height={5}/>
                     <Text style={{...styles.lightText}}>Points: {item.task.points}</Text> */}
                     {/* <Text style={{color:colors.grey_text,fontSize:fonts.sm,fontFamily:font.primary,}}>{item.date} {item.slote}</Text>
                     <Text style={{color:colors.grey_text,fontSize:fonts.sm,fontFamily:font.primary,marginTop:5}}>Status : <Text style={{color:'green',fontSize:fonts.sm,fontFamily:font.primary,marginTop:5}}>{getStatus(item.status)}</Text> </Text> */}
                     {/* <View style={{flex:1,alignItems:"center",justifyContent:'flex-start',flexDirection:"row",marginRight:20,marginTop:15}}>
                       <TouchableOpacity style={{paddingVertical: 5,backgroundColor:"#ffa500",paddingHorizontal:20,borderRadius:4,marginRight:10}} onPress={()=>{}}><Text style={{color:"#fff"}}>Start</Text></TouchableOpacity>
                       <TouchableOpacity style={{paddingVertical: 5,backgroundColor:"#ff7f50",paddingHorizontal:10,borderRadius:4}} onPress={()=>{}}><Text style={{color:"#fff"}}>Reschedule</Text></TouchableOpacity>
                     </View> */}
                     
                     </View>
                     <View style={{flex:1,paddingVertical:10,flexDirection:'column',justifyContent:'space-between'}}>
                     {/* <MaterialIcons name="favorite" size={25} color="red" /> */}
                     {/* <MaterialIcons name="share" size={25} color="grey" onPress={()=>{this.setState({isShare:true,student_id:item.id})}}/> */}
                     {/* <Ionicons name="call-outline" size={25} color={colors.brightRed}  onPress={()=>{}} /> */}
                     {/* <Spacer/> */}
                     {/* <Ionicons name="md-chatbox-ellipses-outline" size={25} color={colors.brightRed}  onPress={()=>{this.registerUsers({id:item.chatuser_id,email:item.email})}} /> */}
                     {/* <MaterialIcons name="delete" size={30} color="grey" /> */}
                     </View>

                     
                     </View>
                     
                    </TouchableOpacity>
                 
                );
              }}
              keyExtractor={(item, index) => index.toString()}
            />
             </>
        )}
         {!this.state.loading && this.state.coupons.length == 0 && (
            <>
            <View style={{flex:1,justifyContent:"center",alignItems:"center"}}>
                <Text> Coupons not founds... </Text>
            </View>
            </>
          )}
      </View>
    );
  }
}
const mapStateToProps = state => {
	return {
		authUser: state.authUser,
		
	};
	
};


export default connect(mapStateToProps, null)(PurchasedCoupons);