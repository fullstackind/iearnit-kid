import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
  // Button,
  ImageBackground,
  Picker
} from 'react-native';
import {connect} from "react-redux";
import {BASE_URL,IMG_URL} from '../../config';
import {styles, colors, fsize} from '../../styles/style'
import ModalSelector from 'react-native-modal-selector'
import Button from '../../widgets/button';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';
import Icon from "react-native-vector-icons/FontAwesome";
import axios from 'axios';
import { showMessage, hideMessage } from "react-native-flash-message";
import ImagePicker from 'react-native-image-crop-picker';
import {AlertMessage} from '../../widgets/AlertMessage';
import {Loader} from '../../widgets/Loader'
import {TextField,Label} from '../../widgets/TextField'
import {Spacer} from '../../widgets/Spacer'
import {saveLoggedUser} from '../../../action';
import DateTimePicker from "@react-native-community/datetimepicker";
import { Formik } from 'formik';
import * as yup from 'yup';
const validationSchema = yup.object().shape({
  name: yup.string().required().matches(/^[aA-zZ\s]+$/, "Only alphabets are allowed for this field ").label(' Name'),
  contact_number: yup.string()
  .required()
  .matches(/^[0-9]+$/, "Must be only digits")
  .min(10, 'Must be exactly 10 digits')
  .max(10, 'Must be exactly 10 digits').label('Contact Number'),
  address: yup.string().matches(/^[aA-zZ\s]+$/, "Only alphabets are allowed for this field ").label('Address'),
  // dob: yup.date().max(new Date(Date.now() - 567648000000), "You must be at least 18 years").required().label('Date of birth'),
  });
class UpdateProfile extends Component {

  constructor(props) {
    super(props);
    this.state={
        user_detail:{},
        session_expire:false,
        loading:false,
        avatar_img:'',
        profile_pic:'',
        profile_image:'',
        show: false,
        date:'',
    }
  }

componentDidMount(){
   
}

SigninApiCall=(values)=>{
  // alert(JSON.stringify(values));

  let formdata = new FormData();
  formdata.append("name", values.name);
  formdata.append("contact_number", values.contact_number);
  formdata.append("user_id", this.props.authUser.userData.id);
  // formdata.append("gender", values.gender);
  formdata.append("avatar", this.state.avatar_img);
  formdata.append("country_code",values.country_code);
  formdata.append("dob",values.dob);
  formdata.append("address",values.address);
  if(this.state.profile_image!=""){
    let pathParts =this.state.profile_image.path.split('/');
    formdata.append("image", {
      uri: this.state.profile_image.path.toString(),
      name: pathParts[pathParts.length - 1],
      type: this.state.profile_image.mime
    });
  }
  axios({
      method: 'post',
      url: BASE_URL+'api/children/update-profile',
      headers: { 'Content-Type': 'multipart/form-data' },
      data:formdata,
    })
    .then( (response)=> {

      // alert(JSON.stringify(response));
      
      if(response.data.success){
        this.props.saveLoggedUser(response.data.data).then((res)=>{
          this.setState({loading:false},()=>{
            showMessage({
              message: "Profile Updated",
              type: "success",
              position:"top",
              duration:2000,
              // hideStatusBar:true,
              icon:"success",
            });
            this.props.navigation.navigate("Profile");
          });
        }).catch((err)=>{
          this.setState({loading:false},()=>{
            showMessage({
              message: "Something went wrong",
              type: "danger",
              position:"top",
              duration:2000,
              // hideStatusBar:true,
              icon:"danger",
            });
            // this.props.navigation.navigate("Profile");
          });
         });

       


        // this.props.saveLoggedUser(response.data.data).then((res)=>{
        //   this.setState({loading:false});
        // }).catch((err)=>{
        //   this.setState({loading:false});
        // });
        
      }else{
        this.setState({loading:false,error:response.data.error});
      }
    //   this.setState({loading:false});
    //   // alert(JSON.stringify(response));
    // console.log("dat",response.data)
    //   // this.setState({bookmarks:response.data.bookmark_items})
    //   // this.setState({completed:true});
  
      
    })
    .catch((error)=> {
      this.setState({loading:false,error:['something went wrong']});
      console.log('er',error);
     
      // this.setState({error:true});
  
    });

}
formatDate(date) {
  var d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [year, month, day].join("-");
  }
profileUpload=()=>{


  ImagePicker.openPicker({
    width: 100,
    height: 100,
    cropping: true,
    mediaType:"photo",
    cropperCircleOverlay:true,
  }).then(image => {
    this.setState({profile_pic:image.path,profile_image:image});
  });
}
 
  render() {
    let genderindex = 0;
    const genderdata = [
        { key: genderindex++, label: 'Male',value:'male' },
        { key: genderindex++, label: 'Female',value:'female' },
        // { key: genderindex++, label: 'others',value:'others' },
      ];
      let countryCodeindex = 0;
    const countryCodedata = [
        { key: countryCodeindex++, label: '+91',value:'+91' },
        { key: countryCodeindex++, label: '+971',value:'+971' },
        { key: countryCodeindex++, label: '+1',value:'+1' },
        { key: countryCodeindex++, label: '+44',value:'+44' },
        { key: countryCodeindex++, label: '+966',value:'+966' },
        { key: countryCodeindex++, label: '+65',value:'+65' },
      ];
    return (
      <View style={style.container}>
        {this.state.session_expire && (<AlertMessage></AlertMessage>)}
        {this.state.loading && (<Loader size={30} backgroundColor={colors.secondary} color={colors.primary}></Loader>)}
        <ScrollView>
        <View style={{alignItems:'center', marginHorizontal:30}}>
        {this.state.profile_pic == "" && this.props.authUser.userData.profile_pic != null && (
            <>
            <Image style={{...styles.profileImage}} source={{uri:IMG_URL+'uploads/profile/'+this.props.authUser.userData.profile_pic}}/>
            </>
          )}
             { this.state.profile_pic == "" &&  this.props.authUser.userData.avatar != null && (
            <>
            <Image style={{...styles.profileImage}} source={{uri:IMG_URL+'uploads/profile/avatar/'+this.props.authUser.userData.avatar}}/>
            </>
          )}

          {this.state.profile_pic != "" && (
             <>
             <Image style={{...styles.profileImage}} source={{uri:this.state.profile_pic}}/>
             </>
          )}
            <Text>Profile Pic</Text>
            


      
          </View>
          
  
             
          
             
              
                 
                  <View style={{...styles.curvedContainer,paddingHorizontal:10}}>
                  <Spacer/>
              <Text>Image Upload</Text>
              
                  <View >
                  <Spacer/>
               
                  <ImageBackground
                style={{width: 80,
                  height: 80,
                  borderRadius: 40,
                  borderWidth: 0,
                  borderColor: "white",
                  marginBottom: 10,
                  overflow:"hidden",
                  position:"relative"}}
                resizeMode="cover"
                source={require('../../assets/images/photo.jpg')}>
                  <Icon
                type="FontAwesome"
                name="camera"
              
                onPress={this.profileUpload}
                style={{
                  position: "absolute",
                  fontSize: 24,
                  color: "gray",
                 bottom:10,
                 left:40
                }}
              />
                </ImageBackground>


                 
                  </View>
                  <Spacer/>
                  <Text>Or Choose an Avatar</Text>
                  <Spacer/>
                  
                    <View style={{flexDirection: 'row',
    flexWrap: 'wrap',
    height: 'auto',}}>

        <TouchableOpacity onPress={()=>this.setState({avatar_img:'avatar1.png',profile_pic:IMG_URL+'uploads/profile/avatar/avatar1.png'})}>
        <Image style={{...styles.profileImage}} source={{uri:IMG_URL+'uploads/profile/avatar/avatar1.png'}}/>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>this.setState({avatar_img:'avatar2.png',profile_pic:IMG_URL+'uploads/profile/avatar/avatar2.png'})}>
        <Image style={{...styles.profileImage}} source={{uri:IMG_URL+'uploads/profile/avatar/avatar2.png'}}/>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>this.setState({avatar_img:'avatar3.jpg',profile_pic:IMG_URL+'uploads/profile/avatar/avatar3.jpg'})}>
        <Image style={{...styles.profileImage}} source={{uri:IMG_URL+'uploads/profile/avatar/avatar3.jpg'}}/>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>this.setState({avatar_img:'avatar5.jpg',profile_pic:IMG_URL+'uploads/profile/avatar/avatar5.jpg'})}>
        <Image style={{...styles.profileImage}} source={{uri:IMG_URL+'uploads/profile/avatar/avatar5.jpg'}}/>
        </TouchableOpacity>
        
       
    </View>
   
    <Spacer/>

    <View style={{width:"100%"}}>
        <Formik
          innerRef={(p) => (this.formik = p)}
      initialValues={{ name: this.props.authUser.userData.name?this.props.authUser.userData.name:'',contact_number:this.props.authUser.userData.contact_number?this.props.authUser.userData.contact_number:'',gender:this.props.authUser.userData.gender?this.props.authUser.userData.gender:'',country_code:this.props.authUser.userData.country_code?this.props.authUser.userData.country_code:'+91',dob:this.props.authUser.userData.dob?this.props.authUser.userData.dob:'',address:this.props.authUser.userData.address?this.props.authUser.userData.address:''}}
      onSubmit={(values, {resetForm,actions}) => {
    //    console.log(values);
       this.setState({loading:true});
        // setTimeout(() => {
        //   actions.setSubmitting(false);
        // }, 1000);
        this.SigninApiCall(values);
        resetForm();
      }}
      validationSchema={validationSchema}
    >
      {(formikProps) => (
        <>
        <Spacer/>
        <Label labelText="Name" color="#183E6E" textTransform="capitalize"/>
        <TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Name"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="name" onInputBlur={formikProps.handleBlur('name')} onInputChange={formikProps.handleChange('name')} value={formikProps.values.name}/>
        {formikProps.errors.name && formikProps.touched.name &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.name}</Text></View>}
         {formikProps.errors.name == null && <Spacer/> }

         <Label labelText="Date of birth" color="#183E6E" textTransform="capitalize"/>
         <TouchableOpacity onPress={() => this.setState({ show: true })}>
                <View
                  style={{
                    borderWidth: 1,
                    borderColor: colors.inputBorder,
                    marginVertical:1,
                    paddingHorizontal: 10,
                    paddingVertical: 5,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                    fontSize:fsize.sm,
                    borderRadius:8,
                    height:50,
                  }}
                >
                  <Text style={{ color: "gray", fontSize: fsize.sm }}>
                    {formikProps.values.dob}
                  </Text>
                  <Ionicons
                    name="ios-calendar-sharp"
                    size={28}
                    color={colors.inputBorder}
                  />
                </View>
              </TouchableOpacity>
              {formikProps.errors.dob && formikProps.touched.dob &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.dob}</Text></View>}
         {formikProps.errors.dob == null && <Spacer/> }
         <Label labelText="Contact Number" color="#183E6E" textTransform="capitalize"/>
         <View style={{...styles.row_center}}>
           
           <View style={{flex:3,marginRight:5}}>
              <View style={{width:"100%",borderColor:colors.inputBorder,borderWidth:1,backgroundColor:colors.secondary, borderRadius:8,alignSelf:'center'}}>
             
              {/* <Picker
               // ref={this.pickerRef}
               style={{color:"#000",height:50,}}
               selectedValue={formikProps.values.country_code}
               onValueChange={itemValue => formikProps.setFieldValue('country_code', itemValue)}
               >
               <Picker.Item label="+91" value="+91" />
               <Picker.Item label="+971" value="+971" />
               <Picker.Item label="+1" value="+1" />
               <Picker.Item label="+44" value="+44" />
               <Picker.Item label="+966" value="+966" />
               <Picker.Item label="+65" value="+65" />

             </Picker> */}
             <ModalSelector
                    data={countryCodedata}
                    initValue={formikProps.values.country_code}
                    onChange={(option)=>{ formikProps.setFieldValue('country_code', option.value)}} >
<TextField placeholderTextColor={colors.inputPlaceholder} marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2}
                        
                        editable={false}
                        placeholder="+91"
                        value={formikProps.values.country_code} />
                        </ModalSelector>
         </View>
       </View>

       <View style={{...styles.row_center,flex:4,backgroundColor:colors.secondary,paddingLeft:0, borderRadius:8,alignSelf:'center',marginLeft:5}}>
       <TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Contact Number"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="contact_number" onInputBlur={formikProps.handleBlur('contact_number')} onInputChange={formikProps.handleChange('contact_number')} value={formikProps.values.contact_number} keyboardType="phone-pad"/>
       </View>
 
       </View>



        
        {formikProps.errors.contact_number && formikProps.touched.contact_number &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.contact_number}</Text></View>}
         {formikProps.errors.contact_number == null && <Spacer/> }
         {/* <Label labelText="Gender" color="#183E6E" textTransform="capitalize"/> */}
        
         {/* <View style={{width:"100%"}}> */}
              
         {/* <Picker style={{width: "100%", height: 80}} itemStyle={{height: 80}}
               selectedValue={formikProps.values.gender}
               onValueChange={itemValue => formikProps.setFieldValue('gender', itemValue)}
               >
                 <Picker.Item label="Select" value="" />
               <Picker.Item label="Male" value="male" />
               <Picker.Item label="Female" value="female" />
               <Picker.Item label="Others" value="others" />
              
             </Picker> */}
               {/* <ModalSelector
                    data={genderdata}
                    initValue={formikProps.values.gender}
                    onChange={(option)=>{ formikProps.setFieldValue('gender', option.value)}} >
<TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2}
                        
                        editable={false}
                        placeholder="Gender"
                        value={formikProps.values.gender} />
                        </ModalSelector> */}
         {/* </View> */}
         {/* {formikProps.errors.gender && formikProps.touched.gender &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.gender}</Text></View>}
        {formikProps.errors.gender == null && <Spacer/> } */}
        <Label labelText="Address" color="#183E6E" textTransform="capitalize"/>
        <TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Address"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="address" onInputBlur={formikProps.handleBlur('address')} onInputChange={formikProps.handleChange('address')} value={formikProps.values.address}/>
        {formikProps.errors.address && formikProps.touched.address &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.address}</Text></View>}
         {formikProps.errors.address == null && <Spacer/> }
            <View style={style.addToCarContainer}>
            <View style={{flexDirection:"row",justifyContent:"center"}}>
               <Button text="Update" width={'100%'} backgroundColor={colors.buttonBlue} textColor={colors.secondary} onPress={()=> formikProps.handleSubmit()}/>
              </View>
             
          </View> 
        </>
      )}
      </Formik>
            </View>



                  
                  </View>
             
          
          
           
         
        </ScrollView>
        {this.state.show && (
          <DateTimePicker
            testID="dateTimePicker"
            value={new Date()}
            mode={"date"}
            is24Hour={true}
            display="default"
            format="YYYY-MM-DD"
            defaultDate={new Date()}
            maximumDate={new Date()}
            onChange={(event, date)=>{
              this.setState({date:date?this.formatDate(date):'',show:false},()=>{
                this.formik.setFieldValue('dob', date?this.formatDate(date):'');
              })
            }}
          />
        )}
      </View>
    );
  }
}
const mapStateToProps = state => {
	return {
		authUser: state.authUser,
		
	};
	
};

const mapDispatchToProps = dispatch => {
	return {
    saveLoggedUser: data => dispatch(saveLoggedUser(data)),
   
		
	
	};
};
export default connect(mapStateToProps, mapDispatchToProps)(UpdateProfile);
const style = StyleSheet.create({
  container:{
    flex:1,
    marginTop:20,
  },
  productImg:{
    width:200,
    height:200,
  },
  name:{
    fontSize:28,
    color:"#696969",
    fontWeight:'bold'
  },
  price:{
    marginTop:10,
    fontSize:18,
    color:"green",
    fontWeight:'bold'
  },
  description:{
    textAlign:'center',
    marginTop:10,
    color:"#696969",
  },
  star:{
    width:40,
    height:40,
  },
  btnColor: {
    height:30,
    width:30,
    borderRadius:30,
    marginHorizontal:3
  },
  btnSize: {
    height:40,
    width:40,
    borderRadius:40,
    borderColor:'#778899',
    borderWidth:1,
    marginHorizontal:3,
    backgroundColor:'white',

    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  starContainer:{
    justifyContent:'center', 
    marginHorizontal:30, 
    flexDirection:'row', 
    marginTop:20
  },
  contentColors:{ 
    justifyContent:'center', 
    marginHorizontal:30, 
    flexDirection:'row', 
    marginTop:20
  },
  contentSize:{ 
    justifyContent:'center', 
    marginHorizontal:30, 
    flexDirection:'row', 
    marginTop:20
  },
  separator:{
    height:2,
    backgroundColor:"#eeeeee",
    marginTop:20,
    marginHorizontal:30
  },
  shareButton: {
    marginTop:10,
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius:8,
    // backgroundColor: "#00BFFF",
  },
  shareButtonText:{
    color: "#FFFFFF",
    fontSize:20,
  },
  addToCarContainer:{
    marginHorizontal:10
  }
});    

