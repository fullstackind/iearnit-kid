import React, { Component } from 'react'
import { Text, View,StyleSheet,FlatList,TouchableOpacity,Image } from 'react-native'
import {images, deviceHeight, statusBarConfig, fonts } from '../../styles/base';
import {styles, colors, fsize} from '../../styles/style'
import{getPoints} from '../../../action';
import {connect} from "react-redux";
import {BASE_URL,IMG_URL} from '../../config';
import axios from 'axios';
import {AlertMessage} from '../../widgets/AlertMessage';
import { showMessage, hideMessage } from "react-native-flash-message";
import {Loader} from '../../widgets/Loader'
class ListCoupons extends Component {
    constructor(props) {
        super(props);
        this.state = {
          
          coupons:[],
          session_expire:false,
         
        };
      }
      getCoupons(){
        axios({
            method: 'post',
            url: BASE_URL+'api/children/list-coupons',
            headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
            data:{ 
              "user_id":this.props.authUser.userData.id, 
            }
          })
          .then( (response)=> {
            // alert(JSON.stringify(response));
            if(response.data.success){
          
              this.setState({coupons:response.data.data,loading:false});
      
              
              
            }else{
              if(response.data.session_expired){
                this.setState({session_expire:true,loading:false});
              }else{
              this.setState({loading:false,error:response.data.error});
              }
            }
         
            
          })
          .catch((error)=> {
            this.setState({loading:false,error:error});
          
        
          });
      }
      componentDidMount(){
          this.getCoupons();
      }
      purchaseCoupon= (item)=>{
          if(!item.purchased){
            if(item.points <= this.props.authUser.points.earned_points)  {
              this.setState({loading:true});
              axios({
                method: 'post',
                url: BASE_URL+'api/children/purchase-coupon',
                headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
                data:{ 
                  "user_id":this.props.authUser.userData.id, 
                  "coupon_id":item.id,
                  "point":item.points
                  
                }
              })
              .then( (response)=> {
                // alert(JSON.stringify(response));
                if(response.data.success){
              
                  this.setState({loading:false},()=>{
                    showMessage({
                      message: "Coupon Purchased ",
                      type: "success",
                      position:"top",
                      duration:2000,
                      // hideStatusBar:true,
                      icon:"success",
                    });
                    this.props.getPoints(this.props.authUser.token,this.props.authUser.userData.id);
                    this.getCoupons();
                    // this.props.navigation.goBack();
                    // this.props.navigation.navigate("Users");
                  });
            
                  
                  
                }else{
                  if(response.data.session_expired){
                    this.setState({session_expire:true,loading:false});
                  }else{
                  this.setState({loading:false,error:response.data.error});
                  }
                }
             
                
              })
              .catch((error)=> {
                this.setState({loading:false,error:error});
              
            
              });
            }else{
              showMessage({
                message: "You don't have enough points to redeem this coupon.",
                type: "warning",
                position:"top",
                duration:2000,
                // hideStatusBar:true,
                icon:"warning",
              });

            }
          }


        }

       
       
  
    render() {
        return (
            <View style={styless.container}>
                {this.state.session_expire && (<AlertMessage></AlertMessage>)}
        {this.state.loading && (<Loader size={30} backgroundColor={colors.secondary} color={colors.primary}></Loader>)}
        {this.state.coupons.length > 0 && (
            <>
           
            <FlatList style={styless.list}
              contentContainerStyle={styless.listContainer}
              data={this.state.coupons}
              horizontal={false}
              numColumns={2}
              keyExtractor= {(item) => {
                return item.id;
              }}
              renderItem={({item}) => {
                return (
                  <View style={styless.card} >
                    <View style={styless.cardFooter}></View>
                    <Image style={styless.cardImage} source={{uri:IMG_URL+'uploads/coupon/'+item.image}}/>
                    <View style={styless.cardHeader}>
                      <View style={{alignItems:"center", justifyContent:"center"}}>

                        <Text style={styless.title}>{item.title}</Text>

                        {item.purchased && (
                            <>
                             <TouchableOpacity style={{height:30,width:100,paddingVertical:6,paddingHorizontal:6,marginTop:8,backgroundColor:"green"}} onPress={()=> {}} disabled={true}>
                             <Text style={styless.buttonText}>Redeemed</Text>
                             </TouchableOpacity>
                            </>
                        )}
 {!item.purchased && (
                            <>

                        <TouchableOpacity style={{height:30,width:100,paddingVertical:6,paddingHorizontal:6,marginTop:8,backgroundColor:((item.points <= this.props.authUser.points.earned_points)  )?"#AF7AC5":"#ABB2B9"}} onPress={()=> this.purchaseCoupon(item)}>
  
                       <Text style={styless.buttonText}>{((item.points <= this.props.authUser.points.earned_points)  )?'Redeem':'Redeem'}</Text>
                       </TouchableOpacity>
                       </>
                        )}
                      </View>
                       
                    </View>
                    
                  </View>
                )
              }}/>
               </>
        )}
          </View>
        )
    }
}
const mapStateToProps = state => {
    // console.log(state);
      return {
          authUser: state.authUser,
          
      };
      
  };
  const mapDispatchToProps = dispatch => {
	return {
        getPoints: (token,user_id) => dispatch(getPoints(token,user_id)),
   
		
	
	};
};
  
  export default connect(mapStateToProps, mapDispatchToProps)(ListCoupons);
  const styless = StyleSheet.create({
    container:{
      flex:1,
    //   marginTop:20,
    },
    list: {
      paddingHorizontal: 5,
      backgroundColor:colors.secondary,
    },
    listContainer:{
      alignItems:'center'
    },
    /******** card **************/
    card:{
      shadowColor: '#00000021',
  
      shadowOffset: {
        width: 0,
        height: 6,
      },
      shadowOpacity: 0.37,
      shadowRadius: 7.49,
  
      elevation: 12,
      marginVertical: 10,
      backgroundColor:"white",
      flexBasis: '42%',
      marginHorizontal: 10,
    },
    cardHeader: {
      paddingVertical: 17,
      paddingHorizontal: 16,
      borderTopLeftRadius: 1,
      borderTopRightRadius: 1,
      flexDirection: 'row',
      alignItems:"center", 
      justifyContent:"center"
    },
    cardContent: {
      paddingVertical: 12.5,
      paddingHorizontal: 16,
    },
    cardFooter:{
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingTop: 12.5,
      paddingBottom: 25,
      paddingHorizontal: 16,
      borderBottomLeftRadius: 1,
      borderBottomRightRadius: 1,
    },
    cardImage:{
      height: 100,
      width: 100,
      alignSelf:'center'
    },
    title:{
      fontSize:12,
      flex:1,
      alignSelf:'center',
      color:colors.fontDark
    },
    buttonText:{
        fontSize:12,
        flex:1,
        alignSelf:'center',
        color:colors.fontDark
      },
  });   