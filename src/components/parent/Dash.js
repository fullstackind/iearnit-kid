import React, { Component } from 'react';
import { View, Text, StatusBar,  TouchableOpacity, FlatList, SafeAreaView, Image, ScrollView, Platform, } from 'react-native';
import {Picker} from '@react-native-picker/picker';
import { Formik } from 'formik';
import * as yup from 'yup';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';
import {images, deviceHeight, statusBarConfig, fonts } from '../../styles/base';
import {styles, colors, fsize} from '../../styles/style'
import Button from '../../widgets/button'
import {TextField,Label} from '../widgets/TextField'
import {Spacer} from '../../widgets/Spacer'
import {Loader} from '../../widgets/Loader'
import {AlertMessage} from '../../widgets/AlertMessage';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import {connect} from "react-redux";
import {BASE_URL,IMG_URL,trimString} from '../../config';
import axios from 'axios';
import { showMessage, hideMessage } from "react-native-flash-message";
class Dash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user_connections:[],
      session_expire:false,
      counts:{
        "task":0,
        "connections":0
      },
      loading:false,
    };
  }
 
getUserConnections(){
  this.setState({loading:true});
  // return new Promise((resolve,reject)=>{
    axios({
      method: 'post',
      url: BASE_URL+'api/parent/list-connected-users',
      headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
      data:{ 
        "user_id":this.props.authUser.userData.id, 
      }
    })
    .then( (response)=> {
     
      // alert(JSON.stringify(response));
      if(response.data.success){
    
        this.setState({user_connections:response.data.data,loading:false});

        
        
      }else{
        if(response.data.session_expired){
          this.setState({session_expire:true,loading:false});
          // alert("session expire");
        }else{
          this.setState({loading:false,error:response.data.error});
        }
        
      }
   
      
    })
    .catch((error)=> {
      this.setState({loading:false,error:error});
    
  
    });
  // });
}
getCounts(){
  
  // return new Promise((resolve,reject)=>{
    axios({
      method: 'post',
      url: BASE_URL+'api/parent/dashboard-count',
      headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
      data:{ 
        "user_id":this.props.authUser.userData.id, 
      }
    })
    .then( (response)=> {
     
      // alert(JSON.stringify(response));
      if(response.data.success){
    
        this.setState({counts:response.data.data,loading:false});

        
        
      }else{
        if(response.data.session_expired){
          this.setState({session_expire:true,loading:false});
          // alert("session expire");
        }else{
          this.setState({loading:false,error:response.data.error});
        }
        
      }
   
      
    })
    .catch((error)=> {
      this.setState({loading:false,error:error});
    
  
    });
  // });
}
componentDidMount = () => {
  this._unsubscribe = this.props.navigation.addListener('focus', () => {
    this.getUserConnections();
    this.getCounts();
    });
    // this.getUserConnections();
   
};
componentWillUnmount() {
  this._unsubscribe();
  }
  render() {
    return (
      <View style={{...styles.container,backgroundColor:'#F9F6F6'}}>
         {this.state.loading && (<Loader size={30} backgroundColor={colors.secondary} color={colors.primary}></Loader>)}
        {this.state.session_expire && (<AlertMessage></AlertMessage>)}
        <StatusBar backgroundColor={statusBarConfig.color} barStyle={statusBarConfig.barStyle} translucent={statusBarConfig.translucent}/>
        <ScrollView style={{...styles.container}}>
        <View style={{...styles.container,backgroundColor:'#F9F6F6'}}>
        <View style={{backgroundColor:colors.primary,minHeight:300,  position:'relative',paddingTop:Platform.OS === 'ios' ? 60 : 10}}>
                  <View style={{...styles.row_center,paddingBottom:15,marginHorizontal:20}}>
        <TouchableOpacity style={{borderColor:colors.primary,borderWidth:1,justifyContent:"center",alignItems:"center"}} onPress={()=>{this.props.navigation.toggleDrawer();}}>
        <Feather name="menu" size={25} color={colors.secondary} style={{}} />
        </TouchableOpacity>
        {/* <View style={{borderColor:colors.primary,borderWidth:1,justifyContent:"center",alignItems:"center"}}>
        
        <Text style={{...styles.labelText,fontFamily:fonts.primaryLight,textAlign:"center",color:colors.secondary}}>MY JOBS</Text>
        </View> */}
        <View style={{justifyContent:"flex-end",alignItems:"center",flexDirection:'row',width:'30%'}}>
          
        {/* <Feather name="users" size={20} color={colors.secondary} style={{}} onPress={()=>this.props.navigation.navigate('Users')} />
        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Jobs')} style={{paddingHorizontal:10}}><Text style={{...styles.labelText,fontFamily:fonts.primaryLight,textAlign:"center",color:colors.secondary}}>MY JOBS</Text></TouchableOpacity> */}
        <Feather name="bell" size={20} color={colors.secondary} style={{}} onPress={()=>this.props.navigation.navigate("Notifications")} />
        </View>
        </View>
        <View style={{flexDirection:"row",justifyContent:'space-between',marginHorizontal:20,alignItems:"center"}}>   
       
        <View style={{flexDirection:"row"}}>
        <FontAwesome5 name="hand-sparkles" size={25} color={colors.thirdary} style={{marginRight:10, minWidth:30}} /> 
        <View>
        <Text style={{...styles.headText,fontFamily:fonts.primaryMedium,fontSize:16}}>Hey User, Lets start Earning</Text>
        {/* <Text style={{...styles.headText,fontFamily:fonts.primaryMedium,fontSize:18}}>Lets start Earning </Text> */}
        {/* <Text style={{...styles.headText,fontFamily:fonts.primaryMedium,fontSize:16}}>{this.props.authUser.userData.name?this.props.authUser.userData.name:'Mr. Sam'} </Text> */}
        </View>
        </View>

        <TouchableOpacity onPress={()=>this.props.navigation.navigate("Profile")}>
          
        <Image
        style={{...styles.profileImage}}
        resizeMode="cover"
        source={{uri:IMG_URL+'uploads/profile/'+this.props.authUser.userData.profile_pic}}
        // source={images.ProfilePic}

      />
        </TouchableOpacity>
        </View> 
        </View>
        <View style={{...styles.curvedContainer,minHeight:deviceHeight/4,marginTop:-100, shadowColor: "#000",shadowOffset: {width: 0,height: 4,},shadowOpacity: 0.30,shadowRadius: 4.65,elevation: 8,}}>
        {/* <View style={{...styles.row_center,paddingVertical:5,marginHorizontal:8}}>
        <View style={{borderColor:colors.primary,borderWidth:1,height:40,width:40,borderRadius:20,justifyContent:"center",alignItems:"center"}}>
        <Feather name="menu" size={20} color={colors.primary} style={{}} />
        </View>
        <View style={{borderColor:colors.primary,borderWidth:1,height:40,width:40,borderRadius:20,justifyContent:"center",alignItems:"center"}}>
        <Feather name="user" size={20} color={colors.primary} style={{}} />
        </View>
        <View style={{borderColor:colors.primary,borderWidth:1,height:40,width:40,borderRadius:20,justifyContent:"center",alignItems:"center"}}>
        <Feather name="bell" size={20} color={colors.primary} style={{}} />
        </View>
        </View> */}
        <Spacer/>
        <View style={{...styles.row_center,paddingHorizontal:10}}>

        <View style={{...styles.center}}>
        <Text style={{...styles.headTextSmall,fontFamily:fonts.primaryMedium,color:colors.primary}}>Total Jobs</Text>
        <Text style={{...styles.headTextLarge,fontFamily:fonts.primaryLight,color:colors.thirdary}}>{this.state.counts.task}</Text>
        </View>
        <View style={{...styles.center}}>
        <Text style={{...styles.headTextSmall,fontFamily:fonts.primaryMedium,color:colors.primary}}>Total Connections</Text>
        <Text style={{...styles.headTextLarge,fontFamily:fonts.primaryLight,color:colors.thirdary}}>{this.state.counts.connections}</Text>
        </View>

        </View>
        <Spacer height={10}/>
        <View style={{justifyContent:'flex-end',flex:1}}>
        <Text style={{...styles.headTextSmall,fontFamily:fonts.primaryMedium,color:colors.primary,paddingLeft:10}}>Connections</Text>
        <Spacer/>
        <View style={{...styles.row_center,justifyContent:"flex-start",overflow:'hidden',paddingLeft:10,backgroundColor:"#f1f1f1",paddingVertical:5,alignItems:"center"}}>
        {this.state.user_connections.length === 0 &&
<TouchableOpacity onPress={()=>this.props.navigation.navigate('Users')} style={{width:50,height:50,backgroundColor:colors.secondary,borderRadius:25,alignItems:"center",justifyContent:'center',marginVertical:20}}>
  <Feather name="plus" size={20} color={colors.thirdary} style={{}} />
  <Text style={{...styles.labelText,fontFamily:fonts.primaryLight,textAlign:"center",fontSize:fsize.xs,color:colors.primary,paddingTop:2}}>ADD</Text>
  </TouchableOpacity>

        }
        {this.state.user_connections.length > 0 && this.state.user_connections.map((item, index) => 
        {
          if(index < 4){
            return ( <TouchableOpacity style={{justifyContent:'center',alignItems:'center',paddingRight:2}} onPress={()=>{this.props.navigation.navigate("UserDetail",{user_id:item.id,find_user:false,status:item.status,request_type:item.request_type,request_id:item.request_id})}}>
           
           {item.avatar == null &&  item.profile_pic != null && (
            <>
           
            <Image style={{...styles.profileImage}} resizeMode="cover" source={{uri:IMG_URL+'uploads/profile/'+item.profile_pic}}/>
</>
           )}
     
     {item.profile_pic == null  &&  item.avatar != null && (
            <>
           
            <Image style={{...styles.profileImage}} resizeMode="cover" source={{uri:IMG_URL+'uploads/profile/avatar/'+item.avatar}}/>
</>
           )}
            <Text style={{...styles.labelText,fontFamily:fonts.primaryLight,textAlign:"center"}}>{trimString(item.name,10)}</Text>
             </TouchableOpacity>)
          }else{
            return null;
          }
        }
      
    
    
    )}


       {/* { this.state.user_connections.length > 0 && ['Max','Fred','Michel','John'].map(val =>(
        <TouchableOpacity style={{justifyContent:'center',alignItems:'center',paddingRight:2}} onPress={()=>{this.props.navigation.navigate("UserDetail")}}>
       <Image style={{...styles.profileImage}} resizeMode="cover" source={images.ProfilePic}/>
       <Text style={{...styles.labelText,fontFamily:fonts.primaryLight,textAlign:"center"}}>{val}</Text>
        </TouchableOpacity>
       ))
       } */}
        {this.state.user_connections.length > 0 &&
       <TouchableOpacity style={{...styles.center}} onPress={()=>{this.props.navigation.navigate("UserConnections")}}>
       <Text style={{...styles.lightText,paddingLeft:5,fontSize:fsize.sm,color:colors.primary}}>View All</Text>
       {/* <Text style={{...styles.lightText,paddingLeft:5,fontSize:fsize.sm,color:colors.primary}}>All</Text> */}
       </TouchableOpacity>

        }
        </View>
        </View>
        </View>

        {/* <View style={{...styles.curvedContainer,minHeight:deviceHeight/5,marginTop:10,elevation:.5,paddingHorizontal:10}}>
        <Spacer/>
        <Text> Redeem </Text>
        <Spacer/>
        <View style={{...styles.row_center}}>
          <View style={{width:'22%',}}>
          <Image style={{...styles.couponImage}} resizeMode="cover"
             
             source={{
          uri: 'https://images.unsplash.com/photo-1598214887283-1b96ceceea96?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=401&q=80',
        }}/>
        <Spacer height={5}/>
        <Text style={{...styles.labelText,fontFamily:fonts.primaryMedium,textAlign:"center",fontSize:8}}>$10 OFF(100PTS)</Text>
          </View>
          <View style={{width:'22%',}}>
          <Image style={{...styles.couponImage}} resizeMode="cover"
             
             source={{
          uri: 'https://images.unsplash.com/photo-1562488780-42dd3b5fc171?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80',
        }}/>
             <Spacer height={5}/>
        <Text style={{...styles.labelText,fontFamily:fonts.primaryMedium,textAlign:"center",fontSize:8}}>$5 OFF(100PTS)</Text>
          </View>

          <View style={{width:'22%',}}>
          <Image style={{...styles.couponImage}} resizeMode="cover"
             
             source={{
          uri: 'https://images.unsplash.com/photo-1569411032431-07598b0012c2?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=375&q=80',
        }}/>
             <Spacer height={5}/>
        <Text style={{...styles.labelText,fontFamily:fonts.primaryMedium,textAlign:"center",fontSize:8}}>$15 OFF(100PTS)</Text>
          </View>
          <View style={{width:'22%',}}>
          <Image style={{...styles.couponImage}} resizeMode="cover"
             
             source={{
          uri: 'https://images.unsplash.com/photo-1618923850107-d1a234d7a73a?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80',
        }}/>
             <Spacer height={5}/>
        <Text style={{...styles.labelText,fontFamily:fonts.primaryMedium,textAlign:"center",fontSize:8}}>$8 OFF(100PTS)</Text>
          </View>
        </View>  
           
        </View> */}

        <View style={{...styles.curvedContainer,marginTop:10,elevation:.5,paddingHorizontal:10}}>
        <Spacer/>
        <Text> Feeds </Text>
        <Spacer/>
            <View style={{flex:1}}>
            <Image style={{...styles.feedImage}} resizeMode="cover"
             
             source={{
          uri: 'https://images.unsplash.com/photo-1540959733332-eab4deabeeaf?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80',
        }}/>
            <View style={{...styles.container}}>
                <Text style={{...styles.labelText,fontFamily:fonts.primaryLight,padding: 5,textAlign:'justify'}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</Text>
            
            </View>
            </View>
            <Spacer/>
        </View>

      
        </View>
        </ScrollView>
      </View>
    );
  }
}
const mapStateToProps = state => {
	return {
		authUser: state.authUser,
		
	};
	
};


export default connect(mapStateToProps, null)(Dash);