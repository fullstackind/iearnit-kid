import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, StyleSheet,ImageBackground,Modal,Dimensions,ScrollView,Image,TextInput } from 'react-native';
import {Picker} from '@react-native-picker/picker';
import { Formik } from 'formik';
import * as yup from 'yup';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';
import ImagePicker from 'react-native-image-crop-picker';
import DateTimePicker from "@react-native-community/datetimepicker";
import {images, deviceHeight, statusBarConfig, fonts } from '../styles/base';
import {styles, colors, fsize} from '../styles/style'
import Button from '../widgets/button'
import {TextField,Label} from '../widgets/TextField'
import {Spacer} from '../widgets/Spacer'
import {Loader} from '../widgets/Loader'
import { Colors } from 'react-native/Libraries/NewAppScreen';
import {connect} from "react-redux";
import {BASE_URL,IMG_URL} from '../config';
import { showMessage, hideMessage } from "react-native-flash-message";
import axios from 'axios';
const validationSchema = yup.object().shape({
  title: yup.string().required().label('Title'),
  description: yup.string().required().label('Description'),
  points: yup.string().required().label('Points'),
  date: yup.string().required().label('Date'),
  
});
class AssignTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      job_id: this.props.route.params
      ? this.props.route.params.job_id
      : "",
     
      job_detail:{},
      task_images:[],
      childrenModalVisible:false,
      user_connections:[],
      assign_data:{},
      children_id:'',
      show: false,
      date:'',
   
    };
  }
  getUserConnections(){
    return new Promise((resolve,reject)=>{
      axios({
        method: 'post',
        url: BASE_URL+'api/parent/list-connected-users',
        headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
        data:{ 
          "user_id":this.props.authUser.userData.id, 
        }
      })
      .then( (response)=> {
        // alert(JSON.stringify(response));
        if(response.data.success){
      
          this.setState({user_connections:response.data.data,loading:false});
  
          
          
        }else{
          this.setState({loading:false,error:response.data.error});
        }
     
        
      })
      .catch((error)=> {
        this.setState({loading:false,error:error});
      
    
      });
    });
  }
  formatDate(date) {
		var d = new Date(date),
		  month = "" + (d.getMonth() + 1),
		  day = "" + d.getDate(),
		  year = d.getFullYear();
	
		if (month.length < 2) month = "0" + month;
		if (day.length < 2) day = "0" + day;
	
		return [year, month, day].join("-");
	  }
componentDidMount(){
    this.getUserConnections();
}

taskImageUpload(){
  ImagePicker.openPicker({
    multiple: true
  }).then(images => {
    this.setState({task_images:images});
    console.log(images);
  });
}
assignTask(values){
  // alert(JSON.stringify(values));
  
this.setState({childrenModalVisible:true,assign_data:values});


  
  // alert(JSON.stringify(values))
}
taskAssign(){
  const formData = new FormData();
  formData.append("user_id",this.props.authUser.userData.id);
  formData.append("title",this.state.assign_data.title);
  formData.append("description",this.state.assign_data.description);
  formData.append("points",this.state.assign_data.points);
  formData.append("date",this.state.assign_data.date);
  formData.append("children_id",this.state.children_id);
  if(this.state.task_images.length > 0){
    this.state.task_images.forEach(file=>{
      let pathParts =file.path.split('/');
			formData.append("task_images[]", {
				uri: file.path.toString(),
				name: pathParts[pathParts.length - 1],
				type: file.mime
			});


     
    });
  }


  axios({
    method: "POST",
    url: BASE_URL + "api/parent/create-task",
    data: formData,
    headers: {
      "Content-Type": "multipart/form-data",
      "Token":this.props.authUser.token,
    }
  }).then( (response)=> {
    if(response.data.success){
      this.setState({task_images:[],assign_data:'',childrenModalVisible:false,children_id:'',date:''},()=>{
        showMessage({
          message: "Task Assigned Successfully",
          type: "success",
          position:"top",
          duration:2000,
          // hideStatusBar:true,
          icon:"success",
        });
        this.formik.resetForm();
      });
    }
    
  }).catch((err)=>{
    // alert(JSON.stringify(err));
  });
}
  render() {
    return (
      <View style={{...styles.container}}>

          <ScrollView>

<Formik
innerRef={(p) => (this.formik = p)}
      initialValues={{ title:'',description:'',points: '',date:this.state.date }}
      onSubmit={(values, {resetForm,actions}) => {
      //  console.log(values);
       this.setState({loading:true});
        // setTimeout(() => {
        //   actions.setSubmitting(false);
        // }, 1000);
        this.assignTask(values);
        // resetForm();
      }}
      validationSchema={validationSchema}
      // enableReinitialize
    >
      {(formikProps) => (
        <>
<View style={{ justifyContent: "space-around", paddingBottom: 5,paddingHorizontal:15 }}>
						<Text style={{color: "gray", paddingVertical: 5}}>
							Title
						</Text>

						<TextInput
							style={{
								height: 45,
								borderColor: "gray",
								borderWidth: 1,
								fontSize: 12,
								color: "gray",
							}}
							
							clearTextOnFocus={true}
							
                            placeholder="Title"
                            
                            value={formikProps.values.title}
                            placeholderTextColor="#a8a8a8"
                            onChangeText={formikProps.handleChange('title')}
                      onBlur={formikProps.handleBlur('title')}
						/>
            
					</View>
          {formikProps.errors.title && formikProps.touched.title &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.title}</Text></View>}
         {formikProps.errors.title == null && <Spacer/> }
        
         <View style={{ justifyContent: "space-around", paddingBottom: 5,paddingHorizontal:15 }}>
						<Text style={{color: "gray", paddingVertical: 5}}>
							Description
						</Text>

						<TextInput
							style={{
								height: 45,
								borderColor: "gray",
								borderWidth: 1,
								fontSize: 12,
								color: "gray",
							}}
							
							clearTextOnFocus={true}
							
                            placeholder="Description"
                            
                            value={formikProps.values.description}
                            placeholderTextColor="#a8a8a8"
                            onChangeText={formikProps.handleChange('description')}
                      onBlur={formikProps.handleBlur('description')}
						/>
            
					</View>
          {formikProps.errors.description && formikProps.touched.description &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.description}</Text></View>}
         {formikProps.errors.description == null && <Spacer/> }
         <View style={{ justifyContent: "space-around", paddingBottom: 5,paddingHorizontal:15 }}>
						<Text style={{color: "gray", paddingVertical: 5}}>
							Points
						</Text>

						<TextInput
							style={{
								height: 45,
								borderColor: "gray",
								borderWidth: 1,
								fontSize: 12,
								color: "gray",
							}}
							
							clearTextOnFocus={true}
							
                            placeholder="Points"
                            
                            value={formikProps.values.points}
                            placeholderTextColor="#a8a8a8"
                            onChangeText={formikProps.handleChange('points')}
                      onBlur={formikProps.handleBlur('points')}
						/>
            
					</View>
          {formikProps.errors.points && formikProps.touched.points &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.points}</Text></View>}
         {formikProps.errors.points == null && <Spacer/> }
         <View style={{ justifyContent: "space-around", paddingBottom: 5,paddingHorizontal:15 }}>
						<Text style={{color: "gray", paddingVertical: 5}}>
							Date
						</Text>

            <TouchableOpacity onPress={() => this.setState({ show: true })}>
                <View
                  style={{
                    borderWidth: 1,
                    borderColor: "gray",
                    paddingHorizontal: 10,
                    paddingVertical: 5,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <Text style={{ color: "gray", fontSize: 12 }}>
                    {this.state.date}
                  </Text>
                  <Ionicons
                    name="ios-calendar-sharp"
                    size={28}
                    color={"gray"}
                  />
                </View>
              </TouchableOpacity>

						{/* <TextInput
							style={{
								height: 45,
								borderColor: "gray",
								borderWidth: 1,
								fontSize: 12,
								color: "gray",
							}}
							
							clearTextOnFocus={true}
							
                            placeholder="Date"
                            
                            value={formikProps.values.date}
                            placeholderTextColor="#a8a8a8"
                            onChangeText={formikProps.handleChange('date')}
                      onBlur={formikProps.handleBlur('date')}
						/> */}
            
					</View>
          {formikProps.errors.date && formikProps.touched.date &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.date}</Text></View>}
         {formikProps.errors.date == null && <Spacer/> }
         <View style={{ justifyContent: "space-around", paddingBottom: 5,paddingHorizontal:15 }}>
						<Text style={{color: "gray", paddingVertical: 5}}>
							Task Images
						</Text>
                        <TouchableOpacity style={{
								height: 45,
								borderColor: "gray",
								borderWidth: 1,
								fontSize: 12,
								color: "gray",
                                justifyContent:"center",
                                
							}} onPress={()=>{this.taskImageUpload()}}>
                            <Text>Select Images</Text>
                        </TouchableOpacity>
                        </View>
           {this.state.task_images.length > 0 && (
     <View>
     <FlatList
                 data={ this.state.task_images }
                 renderItem={ ({item}) =>
                   <View style={{flex:1,
                     justifyContent: 'center',
                     alignItems: 'center',
                     height: 100,
                     margin: 5,
                     // backgroundColor: '#7B1FA2'
                   }}
                     >
                     <Image style={{width:100,height:100,borderRadius:4,overflow:"hidden"}} source={{uri:item.path}}/>
                   </View> 
                   
                 
                 }
                 numColumns={3}
                 keyExtractor={(item, index) => index.toString()}
              />
     </View>
          )}
   <View style={{ flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center", paddingBottom: 5,paddingHorizontal:15,backgroundColor:colors.buttonPrimary,margin:8}}>
   <TouchableOpacity onPress={()=>{formikProps.handleSubmit()}} style={{height:25}}>
    <Text>Next</Text>
  </TouchableOpacity>
       </View>

        </>
      )}
      </Formik>



        
         
         <Modal
  animationType="slide"
  transparent={true}
  visible={this.state.childrenModalVisible}
  onRequestClose={() => {
    this.setState({reviewModalVisible:!this.state.childrenModalVisible})
  }}>
    <View
    style={{
      height: '100%',
      marginTop: 'auto',
      // backgroundColor: "rgba(60, 60, 60, 0.8)"
    }}>
       <View
                    style={{
                      width: "100%",
                      height: Dimensions.get("window").height,
                      backgroundColor: "#fff",
                      paddingVertical: 5,
                      paddingHorizontal:20,
                      borderTopLeftRadius: 20,
                      borderTopRightRadius: 20,
                      alignItems: "center"
                    }}>
<ScrollView style={{width:"100%"}}>
<View>
<TouchableOpacity
                     	style={{
                         height:30,
												flexDirection: "row",
												
											}}
                      onPress={() => {
                        this.setState({childrenModalVisible:!this.state.childrenModalVisible,children_id:''});
                        
                      }}>
                          <Text style={{fontSize:14,color:"red"}}>X</Text>
                          </TouchableOpacity>
  </View>


                      <Text
									style={{
									
						fontSize: 16,
										color: "gray",
										paddingVertical: 8,
									}}
								>
								Select Children
								</Text>

                                { this.state.user_connections.length > 0 && (
  <>
  <View>
<FlatList
            data={ this.state.user_connections }
            style={{
               
              backgroundColor:"#f2f2f2",
              paddingVertical:10,
             
              borderRadius:10,
             

             }}
             scrollEnabled={true}
            renderItem={ ({item}) =>
              <TouchableOpacity
              style={{
                flex: 1,
               
                
                paddingVertical: 5,
               marginBottom:10,
                backgroundColor: this.state.children_id== item.id?'#DBA2F2':'#F5EBF9',
                
                marginHorizontal:5
              }}
              onPress={()=>{this.setState({children_id:item.id})}}
                >
                   <View style={{padding:5,margin:5,flexDirection:"row"}} >
                   <ImageBackground style={{width:100,height:100,borderRadius:4,overflow:"hidden"}} source={{uri:IMG_URL+'uploads/profile/'+item.profile_pic}}> 
                     </ImageBackground>
                     <View style={{flex:5,marginLeft:20,paddingVertical:10}}>
                     <Text  style={{...styles.headTextSmall}}>{item.name}</Text>
                     <Spacer height={5}/>
                     <Text style={{...styles.lightText}}>{item.email}</Text>
                     <Spacer height={5}/>
                     <Text style={{...styles.lightText}}>{item.contact_number}</Text>
                       </View>
                   </View>
                    {/* <View style={{flex:1,flexDirection:"row"}}>
                    <FontAwesome5 name="check-square" size={20} color="gray" style={{marginRight:6}} />
                     <ImageBackground style={{width:60,height:60,borderRadius:4,overflow:"hidden"}} source={{uri:IMG_URL+'uploads/profile/'+item.profile_pic}}> 
                     </ImageBackground>
                        </View>
                    
               <Text>{item.name}</Text> */}
              </TouchableOpacity> 
              
            
            }
            
            keyExtractor={(item, index) => index.toString()}
         />
</View>
  </>
)}
 <View style={{ flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center", paddingBottom: 5,paddingHorizontal:15,backgroundColor:colors.buttonPrimary,margin:8}}>
   <TouchableOpacity onPress={()=>{this.taskAssign()}} style={{height:25}}>
    <Text>Submit</Text>
  </TouchableOpacity>
       </View>
     
  </ScrollView>

                      </View>
    
    </View>

  </Modal>
  </ScrollView>
  {this.state.show && (
          <DateTimePicker
            testID="dateTimePicker"
            value={new Date()}
            mode={"date"}
            is24Hour={true}
            display="default"
            format="YYYY-MM-DD"
            defaultDate={new Date()}
            onChange={(event, date)=>{
              this.setState({date:date?this.formatDate(date):'',show:false},()=>{
                this.formik.setFieldValue('date', date?this.formatDate(date):'');
              })
            }}
          />
        )}
      </View>
    );
  }
}
const mapStateToProps = state => {
	return {
		authUser: state.authUser,
		
	};
	
};


export default connect(mapStateToProps, null)(AssignTask);