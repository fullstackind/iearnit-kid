import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, StyleSheet,ImageBackground,Modal,Dimensions,ScrollView,Image,TextInput } from 'react-native';
import {Picker} from '@react-native-picker/picker';
import { Formik } from 'formik';
import * as yup from 'yup';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';
import ImagePicker from 'react-native-image-crop-picker';
import {images, deviceHeight, statusBarConfig, fonts } from '../styles/base';
import {styles, colors, fsize} from '../styles/style'
import Button from '../widgets/button'
import {TextField,Label} from '../widgets/TextField'
import {Spacer} from '../widgets/Spacer'
import {Loader} from '../widgets/Loader'
import { Colors } from 'react-native/Libraries/NewAppScreen';
import {connect} from "react-redux";
import {BASE_URL,IMG_URL} from '../config';
import { showMessage, hideMessage } from "react-native-flash-message";
import axios from 'axios';
const validationSchema = yup.object().shape({
  points: yup.string().required().label('Points'),
 
  
});
class JobDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      job_id: this.props.route.params
      ? this.props.route.params.job_id
      : "",
     
      job_detail:{},
      task_images:[],
      reviewModalVisible:false,
   
    };
  }
  getJobDetail(){
    axios({
        method: 'get',
        url: BASE_URL+`api/assign-task-detail/${this.state.job_id}`,
        headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
       
      })
      .then( (response)=> {
        // alert(JSON.stringify(response));
        if(response.data.success){
      
          this.setState({job_detail:response.data.data,loading:false});
  
          
          
        }else{
          this.setState({loading:false,error:response.data.error});
        }
     
        
      })
      .catch((error)=> {
        this.setState({loading:false,error:error});
      
    
      });
}
componentDidMount(){
    this.getJobDetail();
}

taskImageUpload(){
  ImagePicker.openPicker({
    multiple: true
  }).then(images => {
    this.setState({task_images:images});
    console.log(images);
  });
}
reviewTask(values){
  


  axios({
    method: "POST",
    url: BASE_URL + "api/parent/review-task",
    data:{
      "assign_id":this.state.job_detail.id,
      "status":2,
      "point":values.points
    },
    headers: {
      "Content-Type": 'application/json',
      "Token":this.props.authUser.token,
    }
  }).then( (response)=> {
    if(response.data.success){
      this.setState({reviewModalVisible:!this.state.reviewModalVisible},()=>{
        showMessage({
          message: "Task Reviewed Successfully",
          type: "success",
          position:"top",
          duration:2000,
          // hideStatusBar:true,
          icon:"success",
        });
      });
    }
    // alert(JSON.stringify(response));
  }).catch((err)=>{
    // alert(JSON.stringify(err));
  });
  
  // alert(JSON.stringify(values))
}
  render() {
    return (
      <View style={{...styles.container}}>
         {Object.keys(this.state.job_detail).length !=0 && (
                <>
     <ScrollView>  

<View>
  <Text>{this.state.job_detail.task.title}</Text>
</View>
<View style={{flexDirection:"row",justifyContent:"space-between"}}>
  <View>
    <Text>Date :{this.state.job_detail.date}</Text>
  </View>
  <View>
    <Text>Points :{this.state.job_detail.task.point}</Text>
  </View>
</View>
<View>
  <Text>Instructions</Text>
  <View>
  <Text>{this.state.job_detail.task.description}</Text>
  </View>
</View>
{this.state.job_detail.task.images && this.state.job_detail.task.images.length > 0 && (
  <>
  <View>
<FlatList
            data={ this.state.job_detail.task.images }
            renderItem={ ({item}) =>
              <View style={{flex:1,
                justifyContent: 'center',
                alignItems: 'center',
                height: 100,
                margin: 5,
                // backgroundColor: '#7B1FA2'
              }}
                >
                <Image style={{width:100,height:100,borderRadius:4,overflow:"hidden"}} source={{uri:IMG_URL+'uploads/task/'+item.image}}/>
              </View> 
              
            
            }
            numColumns={3}
            keyExtractor={(item, index) => index.toString()}
         />
</View>
  </>
)}


{this.state.job_detail.status != 0   && (
  <>
  <View>
  <Text>Job Submition Details</Text>
</View>
<View>
  <View>
  <Text>Description:{this.state.job_detail.remarks}</Text>
  </View>
  <View>
   <View>
     <Text> Uploaded Images</Text>
   </View>
   {this.state.job_detail.assigned_task_images.length > 0 && (
     <View>
     <FlatList
                 data={ this.state.job_detail.assigned_task_images}
                 renderItem={ ({item}) =>
                   <View style={{flex:1,
                     justifyContent: 'center',
                     alignItems: 'center',
                     height: 100,
                     margin: 5,
                     // backgroundColor: '#7B1FA2'
                   }}
                     >
                     <Image style={{width:100,height:100,borderRadius:4,overflow:"hidden"}} source={{uri:IMG_URL+'uploads/task/assign/'+item.name}}/>
                   </View> 
                   
                 
                 }
                 numColumns={3}
                 keyExtractor={(item, index) => index.toString()}
              />
     </View>
          )}
{this.state.job_detail.status == 1 && (
       <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center",height:50,backgroundColor:colors.buttonPrimary,margin:8}}>
       <TouchableOpacity onPress={()=>{this.setState({reviewModalVisible:true})}}>
         <Text>Review</Text>
       </TouchableOpacity>
     </View>
)}
   

  </View>
 
</View>
  </>
)}


        
            </ScrollView>
            </>
         )}
         <Modal
  animationType="slide"
  transparent={true}
  visible={this.state.reviewModalVisible}
  onRequestClose={() => {
    this.setState({reviewModalVisible:!this.state.reviewModalVisible})
  }}>
    <View
    style={{
      height: '50%',
      marginTop: 'auto',
      // backgroundColor: "rgba(60, 60, 60, 0.8)"
    }}>
       <View
                    style={{
                      width: "100%",
                      height: Dimensions.get("window").height / 2,
                      backgroundColor: "#fff",
                      paddingVertical: 5,
                      paddingHorizontal:20,
                      borderTopLeftRadius: 20,
                      borderTopRightRadius: 20,
                      alignItems: "center"
                    }}>
<ScrollView style={{width:"100%"}}>
<View style={{width:"100%",paddingHorizontal:10,paddingTop:5}}>
  
                      <TouchableOpacity
                     	style={{
                         height:30,
												flexDirection: "row",
												justifyContent: "flex-end",
											}}
                      onPress={() => {
                        this.setState({reviewModalVisible:!this.state.reviewModalVisible});
                        
                      }}>
                          <Text style={{fontSize:14,color:"red"}}>X</Text>
                          </TouchableOpacity>
                    
                      </View>
                      <Text
									style={{
									
						fontSize: 16,
										color: "gray",
										paddingVertical: 8,
									}}
								>
								Review Task
								</Text>
                <Formik
      initialValues={{ points: '' }}
      onSubmit={(values, {resetForm,actions}) => {
      //  console.log(values);
       this.setState({loading:true});
        // setTimeout(() => {
        //   actions.setSubmitting(false);
        // }, 1000);
        this.reviewTask(values);
        resetForm();
      }}
      validationSchema={validationSchema}
    >
      {(formikProps) => (
        <>
<View style={{ justifyContent: "space-around", paddingBottom: 5,paddingHorizontal:15 }}>
						<Text style={{color: "gray", paddingVertical: 5}}>
							Points
						</Text>

						<TextInput
							style={{
								height: 45,
								borderColor: "gray",
								borderWidth: 1,
								fontSize: 12,
								color: "gray",
							}}
							
							clearTextOnFocus={true}
							
                            placeholder="Points"
                            
                            value={formikProps.values.points}
                            placeholderTextColor="#a8a8a8"
                            onChangeText={formikProps.handleChange('points')}
                      onBlur={formikProps.handleBlur('points')}
						/>
            
					</View>
          {formikProps.errors.points && formikProps.touched.points &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.points}</Text></View>}
         {formikProps.errors.points == null && <Spacer/> }
        
          
    
     
<View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center",height:50,backgroundColor:colors.buttonPrimary,margin:8}}>
  <TouchableOpacity onPress={()=>{formikProps.handleSubmit()}}>
    <Text>Submit</Text>
  </TouchableOpacity>
</View>
        </>
      )}
      </Formik>
  </ScrollView>

                      </View>
    
    </View>

  </Modal>
      </View>
    );
  }
}
const mapStateToProps = state => {
	return {
		authUser: state.authUser,
		
	};
	
};


export default connect(mapStateToProps, null)(JobDetails);