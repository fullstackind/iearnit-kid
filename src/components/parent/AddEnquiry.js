import React, { Component } from 'react';
import { View, Text, Image, StatusBar, ScrollView, TouchableOpacity, StyleSheet } from 'react-native';
import {Picker} from '@react-native-picker/picker';
// import MultiSelect from 'react-native-multiple-select';
import { TagSelect } from 'react-native-tag-select';
import { Formik } from 'formik';
import * as yup from 'yup';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import {images } from '../styles/base';
import {styles, colors, fsize} from '../styles/style'
import Button from '../widgets/button'
import {TextField,Label} from '../widgets/TextField'
import {Spacer} from '../widgets/Spacer'
import {Loader} from '../widgets/Loader'
import { Colors } from 'react-native/Libraries/NewAppScreen';
const validationSchema = yup.object().shape({
  // userName: yup.string().required().label('User Name'),
 
  // password: yup.string().required().label('Password')
});
const items = [{
  id: '92iijs7yta',
  name: 'Ondo'
}, {
  id: 'a0s0a8ssbsd',
  name: 'Ogun'
}, {
  id: '16hbajsabsd',
  name: 'Calabar'
}, {
  id: 'nahs75a5sg',
  name: 'Lagos'
}, {
  id: '667atsas',
  name: 'Maiduguri'
}, {
  id: 'hsyasajs',
  name: 'Anambra'
}, {
  id: 'djsjudksjd',
  name: 'Benue'
}, {
  id: 'sdhyaysdj',
  name: 'Kaduna'
}, {
  id: 'suudydjsjd',
  name: 'Abuja'
  }
];
export default class AddEnquiry extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedLanguage:"Ms",
      selectedItems : []
    };
    // this._multiSelect = React.createRef(); 
  }
  SigninApiCall=(userEmail,Password)=>{
this.props.navigation.navigate('Enquiries');

  }
  onSelectedItemsChange = selectedItems => {
    this.setState({ selectedItems });
  };
  render() {
    const { selectedItems } = this.state;
//     Brookfield	Brookfield4	Casa Atlanta	Casablanca
// Casablanca Ii	Elegant	Espana	Home
// Indira	Irish County	Landscape	Langford Garden
// Langford Garden II	Midford Garden	Midford Garden II	Mufeed
// Mufeed1	Mufeed30	Mufeed313	Mufeed3133
// mufeed35	mufeed6	Mylayout	New Era
// New Era 1	New Era By 25	New Projectq1	New Projectq2
// new Q	new Q1	Orange County	Orchid
// Palm Meadows I RNL	Palm Meadows II A 52B	Palm Meadows II B 84B	Palm Meadows II C 81B
// Palm Meadows III	Palm Meadows Villas	Prestieege1	PROJECT qura
// Project Test1	Project Testt	Project1	Purvankara
// Purvankara1	Raghava Apartment	sadf	Silicon Valley
// Silver Oak	Silver Park	Spanish County	Test123 Fghfsd
// ttest	Waseem Khan	Wellington Park	White Field
    const data = [
      { id: 1, label: 'Brookfield' },
      { id: 2, label: 'Brookfield4' },
      { id: 3, label: 'Casa Atlanta' },
      { id: 4, label: 'Casablanca' },
      { id: 5, label: 'Casablanca Ii' },
      { id: 6, label: 'Elegant' },
      { id: 7, label: 'Espana' },
      { id: 8, label: 'Home' },
      { id: 9, label: 'Indira' },
      { id: 10, label: 'Irish County' },
      { id: 11, label: 'Brookfield' },
      { id: 12, label: 'Brookfield4' },
      { id: 13, label: 'Casa Atlanta' },
      { id: 14, label: 'Casablanca' },
      { id: 15, label: 'Casablanca Ii' },
      { id: 16, label: 'Elegant' },
      { id: 17, label: 'Espana' },
      { id: 18, label: 'Home' },
      { id: 19, label: 'Indira' },
      { id: 20, label: 'Irish County' },
      { id: 21, label: 'Brookfield' },
      { id: 22, label: 'Brookfield4' },
      { id: 23, label: 'Casa Atlanta' },
      { id: 24, label: 'Casablanca' },
      { id: 25, label: 'Casablanca Ii' },
      { id: 26, label: 'Elegant' },
      { id: 27, label: 'Espana' },
      { id: 28, label: 'Home' },
      { id: 29, label: 'Indira' },
      { id: 30, label: 'Irish County' },
      { id: 31, label: 'Brookfield' },
      { id: 32, label: 'Brookfield4' },
      { id: 33, label: 'Casa Atlanta' },
      { id: 34, label: 'Casablanca' },
      { id: 35, label: 'Casablanca Ii' },
      { id: 36, label: 'Elegant' },
      { id: 37, label: 'Espana' },
      { id: 38, label: 'Home' },
      { id: 39, label: 'Indira' },
      { id: 40, label: 'Irish County' },
    ];
    return (
      <View style={{...styles.container,backgroundColor:colors.secondary}}>
        <ScrollView style={{...styles.container,backgroundColor:colors.secondary}}>
  	<StatusBar
						backgroundColor={colors.primary}
						barStyle="light-content"
						translucent={false}
					/>
    
        <View style={{...styles.center,paddingVertical:50,paddingHorizontal:30}}>
       
        <Formik
      initialValues={{ userName: '', password:'' }}
      onSubmit={(values, actions) => {
       console.log(values);
       this.setState({loader:true});
        // setTimeout(() => {
        //   actions.setSubmitting(false);
        // }, 1000);
        this.SigninApiCall(values.userName,values.password);
    
      }}
      validationSchema={validationSchema}
    >
      {(formikProps) => (
        <>
       
        <Spacer/>

     
        <Label labelText="First Name" color="#183E6E" textTransform="capitalize"/>
        <TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Username" marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="userName" onInputBlur={formikProps.handleBlur('userName')} onInputChange={formikProps.handleChange('userName')}/>
        {formikProps.errors.userName && formikProps.touched.userName &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.userName}</Text></View>}
        {formikProps.errors.userName == null && <Spacer/> }
    
        <Label labelText="First Name" color="#183E6E" textTransform="capitalize"/>
         <View style={{...styles.row_center}}>
           
            <View style={{flex:3,marginRight:5}}>
               <View style={{width:"100%",borderColor:colors.inputBorder,borderWidth:1,backgroundColor:colors.secondary, borderRadius:8,alignSelf:'center'}}>
              
               <Picker
                // ref={this.pickerRef}
                style={{color:"#000",height:50,}}
                selectedValue={this.state.selectedLanguage}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({selectedLanguage:itemValue})
                }>
                <Picker.Item label="Mr" value="A" />
                <Picker.Item label="Ms" value="B" />

              </Picker>
          </View>
        </View>
        <View style={{...styles.row_center,flex:4,backgroundColor:colors.secondary,paddingLeft:0, borderRadius:8,alignSelf:'center',marginLeft:5}}>
           <TextField borderColor={colors.inputBorder} borderWidth={1} height={50} placeholderTextColor={colors.inputPlaceholder} placeholder="Enter unit no" marginVertical={1} width='100%' fontSize={fsize.xs} letterSpacing={2} fieldName="Name" onInputBlur={()=>{}} onInputChange={()=>{}}/>
        </View>
  
        </View>
    
        <Label labelText="First Name" color="#183E6E" textTransform="capitalize"/>
        <TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Username"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="userName" onInputBlur={formikProps.handleBlur('userName')} onInputChange={formikProps.handleChange('userName')}/>
        {formikProps.errors.password && formikProps.touched.password &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.password}</Text></View>}
         {formikProps.errors.password == null && <Spacer/> }

         <Label labelText="First Name" color="#183E6E" textTransform="capitalize"/>
         <View style={{...styles.row_center}}>
           
            <View style={{flex:3,marginRight:5}}>
               <View style={{width:"100%",borderColor:colors.inputBorder,borderWidth:1,backgroundColor:colors.secondary, borderRadius:8,alignSelf:'center'}}>
              
               <Picker
                // ref={this.pickerRef}
                style={{color:"#000",height:50,}}
                selectedValue={this.state.selectedLanguage}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({selectedLanguage:itemValue})
                }>
                <Picker.Item label="Mr" value="A" />
                <Picker.Item label="Ms" value="B" />

              </Picker>
          </View>
        </View>
        <View style={{...styles.row_center,flex:4,backgroundColor:colors.secondary,paddingLeft:0, borderRadius:8,alignSelf:'center',marginLeft:5}}>
           <TextField borderColor={colors.inputBorder} borderWidth={1} height={50} placeholderTextColor={colors.inputPlaceholder} placeholder="Enter unit no" marginVertical={1} width='100%' fontSize={fsize.xs} letterSpacing={2} fieldName="Name" onInputBlur={()=>{}} onInputChange={()=>{}}/>
        </View>
  
        </View>

        <Label labelText="First Name" color="#183E6E" textTransform="capitalize"/>
        <TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Username"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="userName" onInputBlur={formikProps.handleBlur('userName')} onInputChange={formikProps.handleChange('userName')}/>
        {formikProps.errors.password && formikProps.touched.password &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.password}</Text></View>}
         {formikProps.errors.password == null && <Spacer/> }

         <Label labelText="First Name" color="#183E6E" textTransform="capitalize"/>
        <TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Username"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="userName" onInputBlur={formikProps.handleBlur('userName')} onInputChange={formikProps.handleChange('userName')}/>
        {formikProps.errors.password && formikProps.touched.password &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.password}</Text></View>}
         {formikProps.errors.password == null && <Spacer/> }

         <Label labelText="First Name" color="#183E6E" textTransform="capitalize"/>
        <TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Username"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="userName" onInputBlur={formikProps.handleBlur('userName')} onInputChange={formikProps.handleChange('userName')}/>
        {formikProps.errors.password && formikProps.touched.password &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.password}</Text></View>}
         {formikProps.errors.password == null && <Spacer/> }

         <Label labelText="First Name" color="#183E6E" textTransform="capitalize"/>
        <TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Username"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="userName" onInputBlur={formikProps.handleBlur('userName')} onInputChange={formikProps.handleChange('userName')}/>
        {formikProps.errors.password && formikProps.touched.password &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.password}</Text></View>}
         {formikProps.errors.password == null && <Spacer/> }

         <Label labelText="First Name" color="#183E6E" textTransform="capitalize"/>
         <View style={{width:"100%",borderColor:colors.inputBorder,borderWidth:1,backgroundColor:colors.secondary, borderRadius:8,alignSelf:'center'}}>
              
              <Picker
               // ref={this.pickerRef}
               style={{color:"#000",height:50,}}
               selectedValue={this.state.selectedLanguage}
               onValueChange={(itemValue, itemIndex) =>
                 this.setState({selectedLanguage:itemValue})
               }>
               <Picker.Item label="Mr" value="A" />
               <Picker.Item label="Ms" value="B" />

             </Picker>
         </View>

 <Label labelText="First Name" color="#183E6E" textTransform="capitalize"/>
         <View style={{width:"100%",borderColor:colors.inputBorder,borderWidth:1,backgroundColor:colors.secondary, borderRadius:8,alignSelf:'center'}}>
              
              <Picker
               // ref={this.pickerRef}
               style={{color:"#000",height:50,}}
               selectedValue={this.state.selectedLanguage}
               onValueChange={(itemValue, itemIndex) =>
                 this.setState({selectedLanguage:itemValue})
               }>
               <Picker.Item label="Mr" value="A" />
               <Picker.Item label="Ms" value="B" />

             </Picker>
         </View>

         <Label labelText="First Name" color="#183E6E" textTransform="capitalize"/>
        <TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Username"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="userName" onInputBlur={formikProps.handleBlur('userName')} onInputChange={formikProps.handleChange('userName')}/>
        {formikProps.errors.password && formikProps.touched.password &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.password}</Text></View>}
         {formikProps.errors.password == null && <Spacer/> }

         <Label labelText="First Name" color="#183E6E" textTransform="capitalize"/>
         <View style={{width:"100%",borderColor:colors.inputBorder,borderWidth:1,backgroundColor:colors.secondary, borderRadius:8,alignSelf:'center'}}>
              
              <Picker
               // ref={this.pickerRef}
               style={{color:"#000",height:50,}}
               selectedValue={this.state.selectedLanguage}
               onValueChange={(itemValue, itemIndex) =>
                 this.setState({selectedLanguage:itemValue})
               }>
               <Picker.Item label="Mr" value="A" />
               <Picker.Item label="Ms" value="B" />

             </Picker>
         </View>

         <Label labelText="First Name" color="#183E6E" textTransform="capitalize"/>
        <TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Username"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="userName" onInputBlur={formikProps.handleBlur('userName')} onInputChange={formikProps.handleChange('userName')}/>
        {formikProps.errors.password && formikProps.touched.password &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.password}</Text></View>}
         {formikProps.errors.password == null && <Spacer/> }

         <Label labelText="First Name" color="#183E6E" textTransform="capitalize"/>
        <TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Username"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="userName" onInputBlur={formikProps.handleBlur('userName')} onInputChange={formikProps.handleChange('userName')}/>
        {formikProps.errors.password && formikProps.touched.password &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.password}</Text></View>}
         {formikProps.errors.password == null && <Spacer/> }

         <Label labelText="First Name" color="#183E6E" textTransform="capitalize"/>
        <TextField height={80} multiline borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Username"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="userName" onInputBlur={formikProps.handleBlur('userName')} onInputChange={formikProps.handleChange('userName')}/>
        {formikProps.errors.password && formikProps.touched.password &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.password}</Text></View>}
         {formikProps.errors.password == null && <Spacer/> }

         <Label labelText="First Name" color="#183E6E" textTransform="capitalize"/>
        <TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Username"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="userName" onInputBlur={formikProps.handleBlur('userName')} onInputChange={formikProps.handleChange('userName')}/>
        {formikProps.errors.password && formikProps.touched.password &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.password}</Text></View>}
         {formikProps.errors.password == null && <Spacer/> }
         <Label labelText="Project Interested In" color="#183E6E" textTransform="capitalize"/>
         <Spacer/>
         <TagSelect
        //  value={[ { id: 3, label: 'Casa Atlanta' },
        //  { id: 4, label: 'Casablanca' }]}
          data={data}
          itemStyle={styless.item}
          itemLabelStyle={styless.label}
          itemStyleSelected={styless.itemSelected}
          itemLabelStyleSelected={styless.labelSelected}
        />
        <Spacer/>
        <Spacer/>
        <Button borderRadius={30}  text="Submit" onPress={()=>formikProps.handleSubmit()} backgroundColor={colors.buttonPrimary} textColor={colors.buttonTextColor}/>
       
        </>
        )}
       </Formik>
    
        </View>
        {/* <View style={{flex:1}}>
         <MultiSelect
          hideTags
          items={items}
          uniqueKey="id"
          ref={c => this._multiSelect = c}
          onSelectedItemsChange={this.onSelectedItemsChange}
          selectedItems={selectedItems}
          selectText="Pick Items"
          searchInputPlaceholderText="Search Items..."
          onChangeInput={ (text)=> console.log(text)}
          altFontFamily="ProximaNova-Light"
          tagRemoveIconColor="#CCC"
          tagBorderColor="#CCC"
          tagTextColor="#CCC"
          selectedItemTextColor="#CCC"
          selectedItemIconColor="#CCC"
          itemTextColor="#000"
          displayKey="name"
          searchInputStyle={{ color: '#CCC' }}
          submitButtonColor="#CCC"
          submitButtonText="Submit"
        />
        <View style={{flex:1}}>
          {this._multiSelect && this._multiSelect.getSelectedItemsExt(selectedItems)}
        </View>
        </View> */}
        </ScrollView>
      </View>
    );
  }
}


const styless = StyleSheet.create({

  item: {
    borderWidth: 1,
    borderColor: '#333',    
    backgroundColor: '#FFF',
    padding: 5,

  },
  label: {
    color: '#333',
    fontSize:10
  },
  itemSelected: {
    backgroundColor: colors.primary,
  },
  labelSelected: {
    color: '#FFF',
  },
});


