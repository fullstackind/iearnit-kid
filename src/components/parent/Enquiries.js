import React, { Component } from 'react';
import { View, Text, StatusBar,  TouchableOpacity, FlatList, SafeAreaView, Image } from 'react-native';
import { Formik } from 'formik';
import * as yup from 'yup';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import Fontisto from 'react-native-vector-icons/Fontisto';
import {images, deviceHeight } from '../styles/base';
import {styles, colors, fsize} from '../styles/style'
import Button from '../widgets/button'
import {TextField,Label} from '../widgets/TextField'
import {Spacer} from '../widgets/Spacer'
import {Loader} from '../widgets/Loader'
import data from './data.json';
const validationSchema = yup.object().shape({
  userName: yup.string().required().label('User Name'),
 
  password: yup.string().required().label('Password')
});
export default class Enquiries extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }; 
  }
  SigninApiCall=(userEmail,Password)=>{


  }
  renderItem = ( {item,index} ) => {
    console.log(item)
        return (
        <View style={{height:50,...styles.row_center,marginBottom:20}}  key={index} onPress={()=>{}}>

            <View>
            <View style={{...styles.row_center}}>
            <Text style={{...styles.headText,marginRight:10}}>{item.name}</Text>
            <Image source={images.edit} style={{width:15,height:15}} resizeMode="contain"/>
            <Text style={{...styles.highlightText}}>Edit</Text>
            </View>
            <Spacer height={1}/>
            <Text style={{...styles.lightText}}>{item.phone}</Text>
            </View>
           

            <View style={{...styles.center}}  key={index} onPress={()=>{}}>
            <Button borderRadius={8} paddingVertical={8} fontSize={10}  text="Followup" onPress={()=>{this.props.navigation.navigate('Availability')}} backgroundColor={colors.primary} textColor={colors.buttonTextColor}/>
         
            </View>
        </View>
        )
  }
  render() {
    return (
      <View style={{...styles.container,backgroundColor:colors.primary}}>
        {/* <ScrollView style={{...styles.container,backgroundColor:colors.primary}}> */}
  	<StatusBar
						backgroundColor={colors.primary}
						barStyle="light-content"
						translucent={false}
					/>
        <View style={{backgroundColor:'#FBFDFB',height:deviceHeight,marginTop:50}}>
        <View>

        <Spacer/>
        <View style={{...styles.row_center,width:"90%",backgroundColor:colors.secondary,elevation:1,paddingLeft:20, borderRadius:30,alignSelf:'center'}}>
       
        <View style={{flex:1}}>
        <TextField placeholder="Name" marginVertical={1} width='80%' fontSize={fsize.md} letterSpacing={2} fieldName="Name" onInputBlur={()=>{}} onInputChange={()=>{}}/>
        </View>
        <Feather name="search" size={25} color="grey" style={{marginRight:10, minWidth:30}} />
        </View>
     
        </View>
        <SafeAreaView style={styles.container}>
 
 <FlatList
 style={{paddingTop:20,flex:1,paddingHorizontal:30}}
   data={data}
   numColumns={1}                  // set number of columns 
  
   keyExtractor={(item,index) => index.toString()}
   renderItem={( item, index )=>this.renderItem(item,index)}
 
   scrollEnabled
 />
</SafeAreaView>



        </View>

        {/* </ScrollView> */}
      </View>
    );
  }
}
