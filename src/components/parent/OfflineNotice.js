import React, { Component } from 'react'
import { Text, View,Dimensions,Platform } from 'react-native'
import NetInfo from "@react-native-community/netinfo";

const {width}=Dimensions.get('window');

export default class OfflineNotice extends Component {
    state={
        connectionInfo:true
    }
    componentDidMount(){
        NetInfo.fetch().then(isConnected =>{
            this.handleConnectivityChange(isConnected);
            NetInfo.addEventListener(state => {this.handleConnectivityChange(state)});
            // NetInfo.addEventListener('connectionChange', this.handleConnectivityChange);
          
           
        });
    }
    handleConnectivityChange=(conectioninfo)=>{
        this.setState({connectionInfo:conectioninfo.isConnected})
    }
  render() {
      if(!this.state.connectionInfo){
        return (
            <View style={{ alignItems:'center',justifyContent:'center',backgroundColor:'red',paddingTop:18 }}>
              <Text style={{ color:'#fff',fontSize:18}}> No Internet Connection </Text>
            </View>
          )
      }else{
          return null;
      }
  
  }
}