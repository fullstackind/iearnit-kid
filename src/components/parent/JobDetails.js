import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, StyleSheet,ImageBackground,Modal,Dimensions,ScrollView,Image,TextInput } from 'react-native';
import {Picker} from '@react-native-picker/picker';
import { Formik } from 'formik';
import * as yup from 'yup';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';
import ImagePicker from 'react-native-image-crop-picker';
import {images, deviceHeight, statusBarConfig, fonts } from '../../styles/base';
import {styles, colors, fsize} from '../../styles/style'
import Button from '../../widgets/button'
import {TextField,Label} from '../../widgets/TextField'
import {Spacer} from '../../widgets/Spacer'
import {Loader} from '../../widgets/Loader'
import { Colors } from 'react-native/Libraries/NewAppScreen';
import {connect} from "react-redux";
import {BASE_URL,IMG_URL} from '../../config';
import { showMessage, hideMessage } from "react-native-flash-message";
import axios from 'axios';
import {AlertMessage} from '../../widgets/AlertMessage';
const validationSchema = yup.object().shape({
  points: yup.string().required().matches(/^[0-9]+$/, "Must be only digits").label('Points'),
 
  
});
class JobDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      job_id: this.props.route.params
      ? this.props.route.params.job_id
      : "",
     
      job_detail:{},
      task_images:[],
      reviewModalVisible:false,
      loading:false,
      session_expire:false
   
    };
  }
  getJobDetail(){
    this.setState({loading:true}); 
    axios({
        method: 'get',
        url: BASE_URL+`api/assign-task-detail/${this.state.job_id}`,
        headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
       
      })
      .then( (response)=> {
        // alert(JSON.stringify(response));
        if(response.data.success){
      
          this.setState({job_detail:response.data.data,loading:false});
  
          
          
        }else{
          if(response.data.session_expired){
            this.setState({session_expire:true,loading:false});
          }else{
          this.setState({loading:false,error:response.data.error});
          }
        }
     
        
      })
      .catch((error)=> {
        this.setState({loading:false,error:error});
      
    
      });
}
componentDidMount(){
    this.getJobDetail();
}

taskImageUpload(){
  ImagePicker.openPicker({
    multiple: true
  }).then(images => {
    this.setState({task_images:images});
    // console.log(images);
  });
}
reviewTask(values){
  
  this.setState({loading:true,reviewModalVisible:false}); 

  axios({
    method: "POST",
    url: BASE_URL + "api/parent/review-task",
    data:{
      "assign_id":this.state.job_detail.id,
      "status":2,
      "point":values.points
    },
    headers: {
      "Content-Type": 'application/json',
      "Token":this.props.authUser.token,
    }
  }).then( (response)=> {
    if(response.data.success){
      this.setState({reviewModalVisible:!this.state.reviewModalVisible,loading:false},()=>{
        showMessage({
          message: "Job Reviewed Successfully",
          type: "success",
          position:"top",
          duration:2000,
          // hideStatusBar:true,
          icon:"success",
        });
        this.props.navigation.goBack();
      });
    }else{
      if(response.data.session_expired){
        this.setState({session_expire:true,loading:false});
      }else{
        this.setState({loading:false,error:response.data.error});
      }
    }
    // console.log(response.data)
    // alert(JSON.stringify(response));
  }).catch((err)=>{
    this.setState({loading:false,error:error,reviewModalVisible:false});
    // alert(JSON.stringify(err));
  });
  
  // alert(JSON.stringify(values))
}
  render() {
    return (
      <View style={{...styles.container}}>
         {this.state.session_expire && (<AlertMessage></AlertMessage>)}
        {this.state.loading && (<Loader size={30} backgroundColor={colors.secondary} color={colors.primary}></Loader>)}
         {Object.keys(this.state.job_detail).length !=0 && (
                <>
     <ScrollView>  

<View style={{...styles.container,paddingHorizontal:15 }}>
<Spacer/>
  <Text style={{...styles.headTextSmall,fontFamily:fonts.primaryMedium,color:colors.primary}}>{this.state.job_detail.task.title}</Text>

<View style={{flexDirection:"row",justifyContent:"space-between"}}>
  <View>
    <Text style={{...styles.headTextSmall,fontFamily:fonts.primaryMedium,color:colors.primary}}>Date :{this.state.job_detail.date}</Text>
  </View>
  <View>
    <Text style={{...styles.headTextSmall,fontFamily:fonts.primaryMedium,color:colors.primary}}>Points :{this.state.job_detail.task.points?this.state.job_detail.task.points:1000}</Text>
  </View>
</View>
<Spacer/>
<View>
  <Text style={{...styles.headTextSmall,fontFamily:fonts.primaryMedium,color:colors.primary}}>Instructions</Text>
  <Spacer/>
  <View>
    
  <Text style={{...styles.labelText,fontFamily:fonts.primaryLight,textAlign:'justify'}}>{this.state.job_detail.task.description}</Text>
  </View>
  <Spacer/>
</View>
{this.state.job_detail.task.images && this.state.job_detail.task.images.length > 0 && (
  <>
  <View>
<FlatList
            data={ this.state.job_detail.task.images }
            renderItem={ ({item}) =>
              <View style={{
                justifyContent: 'center',
              
                height: 100,
                marginRight: 5,
                // backgroundColor: '#7B1FA2'
              }}
                >
                <Image style={{width:80,height:80,borderRadius:4,overflow:"hidden"}} source={{uri:IMG_URL+'uploads/task/'+item.image}}/>
              </View> 
              
            
            }
            numColumns={3}
            keyExtractor={(item, index) => index.toString()}
         />
</View>
  </>
)}


{this.state.job_detail.status != 0 && (
  <>
  <View>
  <Text>Job Submition Details</Text>
</View>
<View>
  <View>
  <Text>Description:{this.state.job_detail.remarks}</Text>
  </View>
  <View>
   <View>
     <Text> Uploaded Images</Text>
   </View>
   {this.state.job_detail.assigned_task_images.length > 0 && (
     <View>
     <FlatList
                 data={ this.state.job_detail.assigned_task_images}
                 renderItem={ ({item}) =>
                   <View style={{
                     justifyContent: 'center',
                     
                     height: 100,
                     margin: 5,
                     // backgroundColor: '#7B1FA2'
                   }}
                     >
                     <Image style={{width:100,height:100,borderRadius:4,overflow:"hidden"}} source={{uri:IMG_URL+'uploads/task/assign/'+item.name}}/>
                   </View> 
                   
                 
                 }
                 numColumns={3}
                 keyExtractor={(item, index) => index.toString()}
              />
     </View>
          )}
          {this.state.job_detail.status == 1 && (
       <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center",height:50,backgroundColor:colors.buttonPrimary,margin:8}}>
       <TouchableOpacity onPress={()=>{this.setState({reviewModalVisible:true})}}>
         <Text>Review</Text>
       </TouchableOpacity>
     </View>
)}
   
  </View>
 
</View>
  </>
)}

</View>
        
            </ScrollView>
            </>
         )}
         <Modal
  animationType="slide"
  transparent={true}
  visible={this.state.reviewModalVisible}
  onRequestClose={() => {
    this.setState({reviewModalVisible:!this.state.reviewModalVisible})
  }}>
    <View
    style={{
      height: '50%',
      marginTop: 'auto',
      // backgroundColor: "rgba(60, 60, 60, 0.8)"
    }}>
       <View
                    style={{
                      width: "100%",
                      height: Dimensions.get("window").height / 2,
                      backgroundColor: "#fff",
                      paddingVertical: 5,
                      paddingHorizontal:20,
                      borderTopLeftRadius: 20,
                      borderTopRightRadius: 20,
                      alignItems: "center"
                    }}>
<ScrollView style={{width:"100%"}}>
<View style={{width:"100%",paddingHorizontal:10,paddingTop:5}}>
  
                      <TouchableOpacity
                     	style={{
                         height:30,
												flexDirection: "row",
												justifyContent: "flex-end",
											}}
                      onPress={() => {
                        this.setState({reviewModalVisible:!this.state.reviewModalVisible});
                        
                      }}>
                          <Text style={{fontSize:14,color:"red"}}>X</Text>
                          </TouchableOpacity>
                    
                      </View>
                      <Text
									style={{
									
						fontSize: 16,
										color: "gray",
										paddingVertical: 8,
									}}
								>
								Review Job
								</Text>
                <Formik
      initialValues={{ points: '' }}
      onSubmit={(values, {resetForm,actions}) => {
      //  console.log(values);
       this.setState({loading:true});
        // setTimeout(() => {
        //   actions.setSubmitting(false);
        // }, 1000);
        this.reviewTask(values);
        resetForm();
      }}
      validationSchema={validationSchema}
    >
      {(formikProps) => (
        <>

<Label labelText="Points" color="#183E6E" textTransform="capitalize"/>
<Spacer/>
<TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Points"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="points" onInputBlur={formikProps.handleBlur('points')} onInputChange={formikProps.handleChange('points')} value={formikProps.values.points} keyboardType="number-pad"/>
        {formikProps.errors.points && formikProps.touched.points &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.points}</Text></View>}
         {formikProps.errors.points == null && <Spacer/> }
{/* <View style={{ justifyContent: "space-around", paddingBottom: 5,paddingHorizontal:15 }}>
						<Text style={{color: "gray", paddingVertical: 5}}>
							Points
						</Text>

						<TextInput
							style={{
								height: 45,
								borderColor: "gray",
								borderWidth: 1,
								fontSize: 12,
								color: "gray",
							}}
							
							clearTextOnFocus={true}
							
                            placeholder="Points"
                            
                            value={formikProps.values.points}
                            placeholderTextColor="#a8a8a8"
                            onChangeText={formikProps.handleChange('points')}
                      onBlur={formikProps.handleBlur('points')}
						/>
            
					</View> */}
          {/* {formikProps.errors.points && formikProps.touched.points &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.points}</Text></View>}
         {formikProps.errors.points == null && <Spacer/> } */}
        
        <Button text="Submit" width={'100%'} backgroundColor={colors.buttonPrimary} textColor={colors.secondary} onPress={()=> formikProps.handleSubmit()}/>  
    
     
{/* <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center",height:50,backgroundColor:colors.buttonPrimary,margin:8}}>
  <TouchableOpacity onPress={()=>{formikProps.handleSubmit()}}>
    <Text>Submit</Text>
  </TouchableOpacity>
</View> */}
        </>
      )}
      </Formik>
  </ScrollView>

                      </View>
    
    </View>

  </Modal>
      </View>
    );
  }
}
const mapStateToProps = state => {
	return {
		authUser: state.authUser,
		
	};
	
};


export default connect(mapStateToProps, null)(JobDetails);