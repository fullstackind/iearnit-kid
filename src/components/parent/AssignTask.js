import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, StyleSheet,ImageBackground,Modal,Dimensions,ScrollView,Image,TextInput } from 'react-native';
import {Picker} from '@react-native-picker/picker';
import { Formik } from 'formik';
import * as yup from 'yup';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';
import ImagePicker from 'react-native-image-crop-picker';
import DateTimePicker from "@react-native-community/datetimepicker";
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import {images, deviceHeight, statusBarConfig, fonts } from '../styles/base';
import {styles, colors, fsize} from '../../styles/style'
import Button from '../../widgets/button'
import {TextField,Label} from '../../widgets/TextField'
import {Spacer} from '../../widgets/Spacer'
import {Loader} from '../../widgets/Loader'
import { Colors } from 'react-native/Libraries/NewAppScreen';
import {connect} from "react-redux";
import {BASE_URL,IMG_URL} from '../../config';
import { showMessage, hideMessage } from "react-native-flash-message";
import axios from 'axios';
import {AlertMessage} from '../../widgets/AlertMessage';
const validationSchema = yup.object().shape({
  title: yup.string().required().matches(/^[aA-zZ\s]+$/, "Only alphabets are allowed for this field ").label('Title'),
  description: yup.string().required().matches(/^[aA-zZ\s]+$/, "Only alphabets are allowed for this field ").label('Description'),
  points: yup.string().required().matches(/^[0-9]+$/, "Must be only digits").label('Points'),
  date: yup.string().required().label('Date'),
  
});
class AssignTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      job_id: this.props.route.params
      ? this.props.route.params.job_id
      : "",
     
      job_detail:{},
      selectedDates:{},
      task_images:[],
      childrenModalVisible:false,
      user_connections:[],
      assign_data:{},
      children_id:'',
      show: false,
      date:'',
      loading:false,
      session_expire:false,
      error:'',
   
    };
  }
  getUserConnections(){
    return new Promise((resolve,reject)=>{
      axios({
        method: 'post',
        url: BASE_URL+'api/parent/list-connected-users',
        headers: { 'Content-Type': 'application/json','Token':this.props.authUser.token },
        data:{ 
          "user_id":this.props.authUser.userData.id, 
        }
      })
      .then( (response)=> {
        // alert(JSON.stringify(response));
        if(response.data.success){
      
          this.setState({user_connections:response.data.data,loading:false});
  
          
          
        }else{
          if(response.data.session_expired){
            this.setState({session_expire:true,loading:false});
          }else{
          this.setState({loading:false,error:response.data.error});
          }
        }
     
        
      })
      .catch((error)=> {
        this.setState({loading:false,error:error});
      
    
      });
    });
  }
  formatDate(date) {
		var d = new Date(date),
		  month = "" + (d.getMonth() + 1),
		  day = "" + d.getDate(),
		  year = d.getFullYear();
	
		if (month.length < 2) month = "0" + month;
		if (day.length < 2) day = "0" + day;
	
		return [year, month, day].join("-");
	  }
componentDidMount(){
    this.getUserConnections();
}

taskImageUpload(){
  ImagePicker.openPicker({
    multiple: true
  }).then(images => {
    this.setState({task_images:images});
    // console.log(images);
  });
}
assignTask(values){
  // alert(JSON.stringify(values));
  
this.setState({childrenModalVisible:true,assign_data:values});


  
  // alert(JSON.stringify(values))
}
taskAssign(){
  if(this.state.children_id ==""){
    this.setState({error:'Please select children'});
    // showMessage({
    //   message: "Please select children",
    //   type: "warning",
    //   position:"top",
    //   duration:2000,
    //   // hideStatusBar:true,
    //   icon:"warning",
    // });
  }else{

  
  this.setState({loading:true});
  const formData = new FormData();
  formData.append("user_id",this.props.authUser.userData.id);
  formData.append("title",this.state.assign_data.title);
  formData.append("description",this.state.assign_data.description);
  formData.append("points",this.state.assign_data.points);
  formData.append("date",this.state.assign_data.date);
  formData.append("children_id",this.state.children_id);
  if(this.state.task_images.length > 0){
    this.state.task_images.forEach(file=>{
      let pathParts =file.path.split('/');
			formData.append("task_images[]", {
				uri: file.path.toString(),
				name: pathParts[pathParts.length - 1],
				type: file.mime
			});


     
    });
  }


  axios({
    method: "POST",
    url: BASE_URL + "api/parent/create-task",
    data: formData,
    headers: {
      "Content-Type": "multipart/form-data",
      "Token":this.props.authUser.token,
    }
  }).then( (response)=> {
    if(response.data.success){
      this.setState({task_images:[],assign_data:'',childrenModalVisible:false,children_id:'',date:'',loading:false,error:''},()=>{
        showMessage({
          message: "Task Assigned ",
          type: "success",
          position:"top",
          duration:2000,
          // hideStatusBar:true,
          icon:"success",
        });
        this.formik.resetForm();
      });
    }else{
      if(response.data.session_expired){
        this.setState({session_expire:true,loading:false});
      }else{
        this.setState({loading:false,error:''});
      }
    }
    
  }).catch((err)=>{
    this.setState({loading:false,error:error});
    // alert(JSON.stringify(err));
  });
}
}
setDates=(date)=>{
let dateObject={[date.dateString]:{color: '#70d7c7', textColor: 'white'}}
let listOfDates=this.state.selectedDates;
if(listOfDates.hasOwnProperty([date.dateString])){

delete listOfDates[date.dateString]
this.setState((prev)=>{
  return{
    selectedDates:{...prev.selectedDates,...listOfDates}
  } 
  })
}else{
  this.setState((prev)=>{
    return{
      selectedDates:{...prev.selectedDates,...dateObject}
    } 
    })
}

// '2021-10-22': {color: '#70d7c7', textColor: 'white'},

}
  render() {
    console.log(this.state)
    return (
      <View style={{...styles.container}}>
 {this.state.session_expire && (<AlertMessage></AlertMessage>)}
        {this.state.loading && (<Loader size={30} backgroundColor={colors.secondary} color={colors.primary}></Loader>)}
          <ScrollView>


          <Spacer/>
<Spacer/>
<Spacer/>
<Spacer/>
          <View style={{...styles.curvedContainer,elevation:.5,paddingHorizontal:15}}>
<Formik
innerRef={(p) => (this.formik = p)}
      initialValues={{ title:'',description:'',points: '',date:this.state.date }}
      onSubmit={(values, {resetForm,actions}) => {
      //  console.log(values);
       this.setState({loading:true});
        // setTimeout(() => {
        //   actions.setSubmitting(false);
        // }, 1000);
        this.assignTask(values);
        // resetForm();
      }}
      validationSchema={validationSchema}
      // enableReinitialize
    >
      {(formikProps) => (
        <>
        <Spacer/>
<Spacer/>
<Label labelText="Title" color="#183E6E" textTransform="capitalize"/>
<TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Title"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="title" onInputBlur={formikProps.handleBlur('title')} onInputChange={formikProps.handleChange('title')} value={formikProps.values.title}/>
        {formikProps.errors.title && formikProps.touched.title &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.title}</Text></View>}
         {formikProps.errors.title == null && <Spacer/> }

         <Label labelText="Description" color="#183E6E" textTransform="capitalize"/>
<TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Description"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="description" onInputBlur={formikProps.handleBlur('description')} onInputChange={formikProps.handleChange('description')} value={formikProps.values.description} 	multiline/>
        {formikProps.errors.description && formikProps.touched.description &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.description}</Text></View>}
         {formikProps.errors.description == null && <Spacer/> }

         <Label labelText="Points" color="#183E6E" textTransform="capitalize"/>
<TextField borderColor={colors.inputBorder} borderWidth={1} placeholderTextColor={colors.inputPlaceholder} placeholder="Points"  marginVertical={1} width='100%' fontSize={fsize.sm} letterSpacing={2} fieldName="points" onInputBlur={formikProps.handleBlur('points')} onInputChange={formikProps.handleChange('points')} value={formikProps.values.points} keyboardType="number-pad"/>
        {formikProps.errors.points && formikProps.touched.points &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.points}</Text></View>}
         {formikProps.errors.points == null && <Spacer/> }
        
         <Label labelText="Date" color="#183E6E" textTransform="capitalize"/>
         {/* <TouchableOpacity onPress={() => this.setState({ show: true })}>
                <View
                  style={{
                    borderWidth: 1,
                    borderColor: colors.inputBorder,
                    marginVertical:1,
                    paddingHorizontal: 10,
                    paddingVertical: 5,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                    fontSize:fsize.sm,
                    borderRadius:8,
                    height:50,
                  }}
                >
                  <Text style={{ color: "gray", fontSize: fsize.sm }}>
                    {this.state.date}
                  </Text>
                  <Ionicons
                    name="ios-calendar-sharp"
                    size={28}
                    color={colors.inputBorder}
                  />
                </View>
              </TouchableOpacity> */}
        
        <Calendar
  markingType={'period'}
  scrollEnabled={true}
  onDayPress={(day) => {this.setDates(day); console.log('day pressed',day);}}
  markedDates={this.state.selectedDates}
/>
          {formikProps.errors.date && formikProps.touched.date &&
        <View style={{width:"100%"}}><Text style={{...styles.formErrorLabel}}>{formikProps.errors.date}</Text></View>}
         {formikProps.errors.date == null && <Spacer/> }

         <Label labelText="Task Images" color="#183E6E" textTransform="capitalize"/>
         <TouchableOpacity style={{
								height: 45,
								
								borderWidth: 1,
								fontSize:fsize.sm,
                    borderRadius:8,
                    height:50,
								color: "gray",
                borderColor: colors.inputBorder,
                                justifyContent:"center",
                                
							}} onPress={()=>{this.taskImageUpload()}}>
                            <Text style={{textAlign:'center',color:"#a8a8a8"}}>Select Images</Text>
                        </TouchableOpacity>


        
           {this.state.task_images.length > 0 && (
     <View>
     <FlatList
                 data={ this.state.task_images }
                 renderItem={ ({item}) =>
                   <View style={{flex:1,
                     justifyContent: 'center',
                     alignItems: 'center',
                     height: 100,
                     margin: 5,
                     // backgroundColor: '#7B1FA2'
                   }}
                     >
                     <Image style={{width:100,height:100,borderRadius:4,overflow:"hidden"}} source={{uri:item.path}}/>
                   </View> 
                   
                 
                 }
                 numColumns={3}
                 keyExtractor={(item, index) => index.toString()}
              />
     </View>
          )}
          <Spacer/>
<Spacer/>
   <View style={{}}>
   <Button text="Next" width={'100%'} backgroundColor={colors.buttonBlue} textColor={colors.secondary} onPress={()=> formikProps.handleSubmit()}/>
 
       </View>

        </>
      )}
      </Formik>

<Spacer/>
<Spacer/>
<Spacer/>
  </View>
  </ScrollView>
  {this.state.show && (
          <DateTimePicker
            testID="dateTimePicker"
            value={new Date()}
            mode={"date"}
            is24Hour={true}
            display="default"
            format="YYYY-MM-DD"
            defaultDate={new Date()}
            minimumDate={new Date()}
            onChange={(event, date)=>{
              this.setState({date:date?this.formatDate(date):'',show:false},()=>{
                this.formik.setFieldValue('date', date?this.formatDate(date):'');
              })
            }}
          />
        )}



        
         
<Modal
  animationType="slide"
  transparent={true}
  visible={this.state.childrenModalVisible}
  onRequestClose={() => {
    this.setState({reviewModalVisible:!this.state.childrenModalVisible})
  }}>
    <View
    style={{
      height: '100%',
      marginTop: 'auto',
      // backgroundColor: "rgba(60, 60, 60, 0.8)"
    }}>
       <View
                    style={{
                      width: "100%",
                      height: Dimensions.get("window").height,
                      backgroundColor: "#fff",
                      paddingVertical: 5,
                      paddingHorizontal:20,
                      borderTopLeftRadius: 20,
                      borderTopRightRadius: 20,
                      alignItems: "center"
                    }}>
<ScrollView style={{width:"100%"}}>
<View>
<TouchableOpacity
                     	style={{
                        
												flexDirection: "row",
                        justifyContent:"flex-end",
												paddingVertical:10 }}
                      onPress={() => {
                        this.setState({childrenModalVisible:!this.state.childrenModalVisible,children_id:'',loading:false,error:''});
                        
                      }}>
                          <Text style={{fontSize:20,color:"#EC7063"}}>X</Text>
                          </TouchableOpacity>
  </View>


                      <Text
									style={{
									
						...styles.headTextSmall
									}}
								>
								Select Children
								</Text>

                                { this.state.user_connections.length > 0 && (
  <>
  <View>
    <Spacer/>
    <Spacer/>
    <Spacer/>
    {this.state.error !="" && (<Text style={{...styles.formErrorLabel}}>{this.state.error}</Text>)}
<FlatList
            data={ this.state.user_connections }
            style={{
               
              
              paddingVertical:10,
             
              borderRadius:10,
             

             }}
             scrollEnabled={true}
            renderItem={ ({item}) =>
              <TouchableOpacity
              style={{
                flex: 1,
               
                
                padding: 8,
          marginBottom:10,
                backgroundColor: '#f2f2f2',
                borderWidth: this.state.children_id== item.id?2:0,
                borderColor: this.state.children_id== item.id?'#EC7063':'#f2f2f2',
              }}
              onPress={()=>{this.setState({children_id:item.id,error:''})}}
                >
                   <View style={{flexDirection:"row"}} >

                   {item.avatar == null &&  item.profile_pic != null && (
            <>
           <ImageBackground style={{width:50,height:50,borderRadius:4,overflow:"hidden"}} source={{uri:IMG_URL+'uploads/profile/'+item.profile_pic}}> 
                     </ImageBackground>
            {/* <Image style={{...styles.profileImage}} resizeMode="cover" source={{uri:IMG_URL+'uploads/profile/'+item.profile_pic}}/> */}
</>
           )}
     
     {item.profile_pic == null  &&  item.avatar != null && (
            <>
           <ImageBackground style={{width:50,height:50,borderRadius:4,overflow:"hidden"}} source={{uri:IMG_URL+'uploads/profile/avatar/'+item.avatar}}> 
                     </ImageBackground>
            {/* <Image style={{...styles.profileImage}} resizeMode="cover" source={{uri:IMG_URL+'uploads/profile/avatar/'+item.avatar}}/> */}
</>
           )}

                   


                     <View style={{flex:5,marginLeft:20,paddingVertical:10}}>
                     <Text  style={{...styles.headTextSmall}}>{item.name}</Text>
                     {/* <Spacer height={5}/>
                     <Text style={{...styles.lightText}}>{item.email}</Text>
                     <Spacer height={5}/>
                     <Text style={{...styles.lightText}}>{item.contact_number}</Text> */}
                       </View>
                   </View>
                    {/* <View style={{flex:1,flexDirection:"row"}}>
                    <FontAwesome5 name="check-square" size={20} color="gray" style={{marginRight:6}} />
                     <ImageBackground style={{width:60,height:60,borderRadius:4,overflow:"hidden"}} source={{uri:IMG_URL+'uploads/profile/'+item.profile_pic}}> 
                     </ImageBackground>
                        </View>
                    
               <Text>{item.name}</Text> */}
              </TouchableOpacity> 
              
            
            }
            
            keyExtractor={(item, index) => index.toString()}
         />
</View>
  </>
)}
 <View style={{ }}>
   {/* <TouchableOpacity onPress={()=>{this.taskAssign()}} style={{height:25}}>
    <Text>Submit</Text>
  </TouchableOpacity> */}
    <Spacer/>
  <Spacer/>
  <Button text="Submit" width={'100%'} backgroundColor={colors.buttonBlue} textColor={colors.secondary} onPress={()=> this.taskAssign()}/>  
  <Spacer/>
  <Spacer/>
       </View>
     
  </ScrollView>

                      </View>
    
    </View>

  </Modal>
      </View>
    );
  }
}
const mapStateToProps = state => {
	return {
		authUser: state.authUser,
		
	};
	
};


export default connect(mapStateToProps, null)(AssignTask);