import React from "react";
import { SafeAreaView, StyleSheet, TextInput, Text, View } from "react-native";
import {styles, colors, fsize} from '../../styles/style'

const LineBreaker= (props) => {

  
    return (
     
        <View style={{backgroundColor:props.color,width:props.width,height:props.height}}></View>
    
    );
  };


export  {LineBreaker};