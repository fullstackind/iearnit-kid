import React from "react";
import { View, Text, ScrollView, StatusBar, ActivityIndicator,TouchableOpacity } from 'react-native';

import {styles, colors, fsize} from '../styles/style'
import {appStateReset} from "../../action";
import { useDispatch } from "react-redux";
const AlertMessage= (props) => {
  const dispatch = useDispatch();
  const loginClick = () => {
   
    dispatch(appStateReset());
}

    return (
     
        <View style={{position:"absolute",width:"100%",height:"100%",justifyContent:'center',alignItems:"center",zIndex:1,backgroundColor:props.backgroundColor?props.backgroundColor:'rgba(0, 0, 0, .3)'}}>
        <View style={{backgroundColor:"#FFF",paddingHorizontal:12,paddingVertical:12}}>
        <Text> Session Expired</Text>
        <Text>Your login session expired.please login to continue.</Text>
        <TouchableOpacity style={{backgroundColor:colors.buttonPrimary,alignItems:"center",margin:12,height:25}} onPress={loginClick}>
          <Text>Login</Text>
          </TouchableOpacity>
        </View>
        
      
        {/* <ActivityIndicator size={props.size?props.size:60} color={props.color?props.color:colors.secondary} /> */}
        </View>
    
    );
  };


export  {AlertMessage};