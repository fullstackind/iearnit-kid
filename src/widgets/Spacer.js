import React from "react";
import { SafeAreaView, StyleSheet, TextInput, Text, View } from "react-native";
import {styles, colors, fsize} from '../styles/style'

const Spacer= (props) => {

  
    return (
     
        <View style={{...styles.spacer,height:props.height?props.height:10}}></View>
    
    );
  };


export  {Spacer};