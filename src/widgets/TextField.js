import React from "react";
import { SafeAreaView, StyleSheet, TextInput, Text, View,TouchableOpacity } from "react-native";
import Ionicons from 'react-native-vector-icons/Ionicons';
import {styles, colors, fsize} from '../styles/style'
const TextField= (props) => {
  // const [text, onChangeText] = React.useState("Useless Text");
  const [value, onChangeValue] = React.useState(null);
  const [secureTextEntry, setSecureTextEntry] = React.useState(props.secureTextEntry?props.secureTextEntry:false);


  return (
    
   <View style={{position:'relative',width:'100%'}}>
     {console.log(setSecureTextEntry)}
   {props.secureTextEntry !== undefined && props.showHideButton &&
   <TouchableOpacity onPress={()=>{setSecureTextEntry(!secureTextEntry)}} style={{position:'absolute',top:16,right:15,zIndex:1}}>
    <Ionicons name={secureTextEntry?"eye":"eye-off"} size={20} color="#565757" />
   </TouchableOpacity>
   }
   
      <TextInput
        style={{...styles.inputField,
          backgroundColor:props.backgroundColor?props.backgroundColor:colors.secondary,
          height: props.height?props.height:50,
          width: props.width?props.width:'100%',
          marginVertical:props.marginVertical?props.marginVertical: 12,
          color:props.textColor?props.textColor:'#000',
          fontSize:props.fontSize?props.fontSize:fsize.md-1,
          letterSpacing:props.letterSpacing?props.letterSpacing:0,
          borderColor:props.borderColor?props.borderColor:"transparent",
          borderWidth:props.borderWidth?props.borderWidth:0
        }}
        onChangeText={(val)=>props.onInputChange?props.onInputChange(val):onChangeValue}
        onBlur={(val)=>props.onInputBlur?props.onInputBlur(val):null}
        value={props.value?props.value:value}
        placeholder={props.placeholder?props.placeholder:""}
        placeholderTextColor={props.placeholderTextColor?props.placeholderTextColor:'#DCDCDC'}
        keyboardType={props.keyboardType?props.keyboardType:"default"}
        secureTextEntry={secureTextEntry}
        multiline={props.multiline?props.multiline:false}
        numberOfLines={props.numberOfLines?props.numberOfLines:1}
      />
  </View>
  );
};
const Label= (props) => {

  
    return (
     
        <Text style={{...styles.labelText,
          color:props.color?props.color:"#fff",
          textTransform:props.textTransform?props.textTransform:'uppercase',
        }}>{props.labelText}</Text>
    
    );
  };
  const Spacer= (props) => {

  
    return (
     
        <View style={{...styles.spacer,height:10}}></View>
    
    );
  };


export  {TextField,Label,Spacer};