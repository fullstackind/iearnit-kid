import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import {styles, colors, fsize} from '../styles/style'
export default class Button extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress} style={{...styles.button,
      backgroundColor:this.props.backgroundColor,
      width:this.props.width?this.props.width:'100%',
      paddingHorizontal: this.props.paddingHorizontal?this.props.paddingHorizontal:10,
      paddingVertical:this.props.paddingVertical?this.props.paddingVertical:15,
      borderRadius: this.props.borderRadius?this.props.borderRadius:8
      }}>
        <Text style={{...styles.buttonTextStyle,
          color:this.props.textColor,
          letterSpacing:this.props.letterSpacing,
          fontSize:this.props.fontSize?this.props.fontSize:fsize.md,

          }}> {this.props.text} </Text>
      </TouchableOpacity>
    );
  }
}
