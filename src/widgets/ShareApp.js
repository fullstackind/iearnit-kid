import React from 'react';
import { Component } from 'react';
import { Share, View, Text, Image } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Spacer } from './Spacer';
import {styles, colors, fsize} from '../../styles/style'
import { images } from '../../styles/base';
import Button from './button';
export default class ShareApp extends Component {

    constructor(props) {
        super(props);
        this.state = {
        
        };
        
      }
  onShare = async () => {
    try {
      const result = await Share.share({
       title: 'Download the GoldPrep it is the best application for NEET PG |INICET |FMGE | NEXT.',
  message: 'Download the GoldPrep it is the best application for NEET PG |INICET |FMGE | NEXT. I am using GoldPrep for NEET PG. it is best application For solving pure clinical question and explanation is amazing , AppLink :https://play.google.com/store/apps/details?id=com.qbank', 
  url: 'https://play.google.com/store/apps/details?id=com.qbank'
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };
  render(){
    return (
     
                  <View style={{...styles.centeredView,marginTop:0,backgroundColor:"#fff"}}>
                     <Image
          style={{width:180,height:100}}
          source={images.logo}
        />
            {/* <Image source={images.users} resizeMode="contain" style={{width:100,height:100}} /> */}
           <Spacer/>
           <Spacer/>
           <Text style={{...styles.headTextSmall,paddingHorizontal:50,textAlign:"center"}}>Share App!!</Text>
     
           <Spacer/>
           <Text style={{...styles.planTiles,paddingHorizontal:50,textAlign:"center"}}>Use below link to share the goldprep app with your family and friends.</Text>
           <Spacer/>
        <Spacer/>
        <Spacer/>
        <Button 
          width='80%'
          fontSize={fsize.md} 
        text="Share App" onPress={()=>this.onShare()} backgroundColor={colors.buttonPrimary} textColor={colors.buttonTextColor}/>

      </View>
         
       
      )
  }

}