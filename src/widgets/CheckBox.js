import React from "react";
import { SafeAreaView, StyleSheet, TextInput, Text, View, TouchableOpacity } from "react-native";
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

import {styles, colors, fsize, fonts} from '../../styles/style'
import { Spacer } from "./Spacer";
const CheckBox= (props) => {
  const [text, onChangeText] = React.useState("Useless Text");
  const [number, onChangeNumber] = React.useState(null);

  return (
   <>
         <TouchableOpacity
         onPress={()=>props.setActiveAnswer(props.id)}
         style={{
             backgroundColor:props.active?colors.fontBlue:'#F6F9FA',
             ...styles.row_center,
             minHeight:42,
             padding: 10,
             borderRadius:8}}>
            {props.active ?
            <View style={{width:25}}>
            <Ionicons name="checkmark" size={20} color={colors.secondary} />
            </View>
            :
            <View style={{width:25}}>
            
            </View>
            } 
             <Text style={{...styles.labelText,color:props.active?colors.secondary:"#000",fontFamily:fonts.primaryMedium}}>{props.label}</Text>
         </TouchableOpacity>
          <Spacer/>
  </>
  );
};

export  {CheckBox};