import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {styles, colors, fsize} from '../../styles/style'
export default class Logo extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <Text style={{marginBottom:0}}><Text style={{...styles.logo_text_B,fontSize:fsize.h2}}></Text><Text style={{...styles.logo_text,fontSize:fsize.h2-2}}>Goldprep</Text></Text>
    );
  }
}
