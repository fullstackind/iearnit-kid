import React from "react";
import { View, Text, ScrollView, StatusBar, ActivityIndicator } from 'react-native';
import {styles, colors, fsize} from '../styles/style'

const Loader= (props) => {

  
    return (
     
        <View style={{position:"absolute",width:"100%",height:"100%",justifyContent:'center',alignItems:"center",zIndex:1,backgroundColor:props.backgroundColor?props.backgroundColor:'rgba(0, 0, 0, .3)'}}>
        <ActivityIndicator size={props.size?props.size:60} color={props.color?props.color:colors.secondary} />
        </View>
    
    );
  };


export  {Loader};