import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { BASE_URL } from '../src/config';



export const loginState = (data) => (dispatch) =>{
    dispatch({ type: 'Login',status:data});
}
export const saveLoggedUser = (data) => (dispatch) =>
  new Promise(function(resolve, reject) {
    AsyncStorage.setItem('child_app_token',data.api_token)
        .then((response) => {
            dispatch({ type: 'Login',status:true});
            dispatch({ type: 'SET_TOKEN',token:data.api_token});
            dispatch({ type: 'SET_USER_DATA',payload:data});
            dispatch({ type: 'SAVE_POINTS',points:{"earned_points":data.points,"redeemed_points":data.redeemed_points}});
            resolve(data);
        })
        .catch((err) => {
            reject(err);
        })
    
  });
  export const appStateReset = () => (dispatch) =>
  new Promise(function(resolve, reject) {

    AsyncStorage.removeItem('child_app_token').then((ress)=>{
      dispatch({ type: 'RESET',payload:{}});
      resolve(ress);
    }).catch((err) => {
      dispatch({ type: 'RESET',payload:{}});
      resolve(ress);
  });
  
  
        


  });
// const _retrieveData = async () => {
    
//   try {
//     let value = await AsyncStorage.getItem('@storage_Key');
  
//     if (value !== null) {
//       return Promise.resolve(value)
//       }else{
//         return Promise.reject()
//     }
//   } catch (error) {
//     console.log(error);
//     return Promise.reject(error)
//   }
// }
const _setKey= async(key)=>{
  try {
    let value =  await AsyncStorage.setItem('child_app_token', key);
    return Promise.resolve(value);
  } catch (e) {
    return Promise.reject(e)
  }
}
export const setToken = async(token) =>{
  try {
    const value = await AsyncStorage.getItem('child_app_token');
    if (value !== null) {
       dispatch({
        type: 'SET_TOKEN',
        token: value
      });
     
    }else{
      dispatch({
        type: 'SET_TOKEN',
        token: null
      });
    }
    
  } catch (e) {
    dispatch({
      type: 'SET_TOKEN',
      token:null
    });
  }

}



export const getToken = (token) =>{
  _setKey()
  .then(()=>{
 
   dispatch({
     type: 'GET_TOKEN',
   });
 
  })
  .catch((e)=>{console.log(e)})
 }


 export const checkToken = async() =>{
  try {
    const value = await AsyncStorage.getItem('child_app_token');
    if (value !== null) {
      return Promise.resolve(value)
     
    }else{
      return Promise.reject()
    }
    
  } catch (e) {
    return Promise.reject(e)
    // Error retrieving data
  }
 }

 export const setUserData = (token) =>{


          return async dispatch => {
            await axios({
              method: 'get',
              url: `${serverLink}/api/Native/Profile`,
             
              headers: {"Authorization" : `Bearer ${token}` }, 
               
            
            
            })
            .then( (response)=> {
              console.log(response);
              dispatch({
                                type: 'SET_USER_DATA',
                                payload: response.data.result
                              });
              Promise.resolve()
              
             })
            .catch((error)=> {
              console.log('er',error.response);
          
              Promise.reject() 
            });
         
          };
    
  }

  export const saveToken = token => dispatch => {
    dispatch({
      type: 'SET_TOKEN',
      token: token
    });
  };
  export const savePoints = points => dispatch => {
    dispatch({
      type: 'SAVE_POINTS',
      points: points
    });
  };
  export const getPoints = (token,user_id) =>{


    return async dispatch => {
      await axios({
        method: 'post',
        url: BASE_URL+'api/children/get-points',
       
        headers: { 'Content-Type': 'application/json',"Token" : token },
        data:{ 
          "user_id":user_id, 
        }
      
      
      })
      .then( (response)=> {
        // console.log(response);
        if(response.data.success){
          dispatch({
            type: 'SAVE_POINTS',
            points: response.data.data
          });
         
        }
       
        Promise.resolve()
        
       })
      .catch((error)=> {
        console.log('er',error.response);
        dispatch({
          type: 'SAVE_POINTS',
          points: {"earned_points":0,"redeemed_points":0}
        });
        Promise.reject() 
      });
   
    };

}

