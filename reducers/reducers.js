export function common(state={loading:false,error:[],reload:false},action){
    switch (action.type) {
        case 'LOADING':
        return { ...state, loading: action.isLoading };
        case 'ERROR':
            return { ...state, error: action.error };
           
        default:
        return state;
    }
}



const initialState={login:false,userData:{},token:null,points:{"earned_points":0,"redeemed_points":0}}


export function authUser(state=initialState,action){
    switch (action.type) {
        case 'Login':
            return { ...state,login:action.status };
            case 'SET_TOKEN':
              return { ...state,token:action.token };
        case 'SET_USER_DATA':
              return { ...state,userData:action.payload }; 
              case 'RESET':
                return initialState;    
                case 'SAVE_POINTS':
                    return { ...state,points:action.points };        
        default:
            return state;
    }
}






  