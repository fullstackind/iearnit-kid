/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TouchableOpacity,
  Image,
  Dimensions
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import {
	createDrawerNavigator,
	DrawerContentScrollView,
	DrawerItemList,
} from "@react-navigation/drawer";
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Foundation from "react-native-vector-icons/Foundation";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Feather from 'react-native-vector-icons/Feather';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { colors,fonts,fsize,images } from './src/styles/base';
import{checkToken,saveLoggedUser} from './action';
import {connect} from "react-redux";
import {useSelector} from "react-redux";
import axios from 'axios';
import { BASE_URL ,IMG_URL} from './src/config';
import { styles } from './src/styles/style';
import store from "./store";
import { Colors } from 'react-native/Libraries/NewAppScreen';

import Login from './src/components/auth/Login';
import Register from './src/components/auth/Register';
import Logout from './src/components/auth/Logout';
import SplashScreen from './src/components/SplashScreen';
import LaunchingScreen from './src/components/auth/LaunchingScreen';

import ParentScreens from './src/navigation/parentNavigations';
import KidsScreens from './src/navigation/kidsNavigations';


















const AuthStack = createNativeStackNavigator();
const AuthStackScreen = () => {

	
	return (
		<AuthStack.Navigator 
		screenOptions={({ navigation, route })=>({
			
	
		  })
		}>
		
    <AuthStack.Screen
				name="Landing"
				component={LaunchingScreen}
				options={{headerShown: false}}
			/>
      	<AuthStack.Screen
				name="Login"
				component={Login}
				options={{headerShown: false}}
			/>
		
		<AuthStack.Screen
				name="Register"
				component={Register}
				options={{headerShown: false}}
			/>	
		</AuthStack.Navigator>
	);
};

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
      token:false,
      loading:true,
		};

    
  }
	loginCheck(){

  }
componentDidMount=()=>{
// alert(JSON.stringify(this.props.loggedUser.userData))
  checkToken().then((value)=>{
    console.log("token:>",value)
    axios({
      method: 'post',
      url: BASE_URL+'api/valid-token',
      headers: { 'Content-Type': 'application/json','Token':value },
      data:{ 
        "token":value, 
        
       
      }
    })
    .then( (response)=> {
      // alert(JSON.stringify(response));
      if(response.data.success){
    


        this.props.saveLoggedUser(response.data.data).then((res)=>{
          this.setState({loading:false});
        }).catch((err)=>{
           this.setState({loading:false});
         });
        
      }else{
        this.setState({loading:false,error:response.data.error});
      }
  
  
      
    })
    .catch((error)=> {
      this.setState({loading:false,error:error});
      // console.log('er',error);
     
      // this.setState({error:true});
  
    });
  }).catch((e)=>{
    // console.log("error:",e)
    setTimeout(() => {this.setState({loading: false})}, 2000)
  })


  
}

render(){
  if(this.state.loading){
    return (
     <SplashScreen></SplashScreen>
    );
  }else if(this.props.loggedUser.userData && Object.keys(this.props.loggedUser.userData).length !=0 && this.props.loggedUser.userData.user_type =='children' ){
    return (
  
      <NavigationContainer>
      
          <KidsScreens/>        
         
        
      </NavigationContainer>
       
  
    );
  }else if(this.props.loggedUser.userData && Object.keys(this.props.loggedUser.userData).length !=0 && this.props.loggedUser.userData.user_type =='parent' ){
    return (
  
      <NavigationContainer>
      
          <ParentScreens/>        
         
        
      </NavigationContainer>
       
  
    );
  }else{
  
    return (
  
      <NavigationContainer>
       
          <AuthStackScreen/>        
         
        
      </NavigationContainer>
       
  
    );
  }

 

}
 

  
}
const mapStateToProps = (state) => {
	console.log(state);
	return {
		loggedUser: state.authUser,
		
	};
};
const mapDispatchToProps = (dispatch) => {
	return {
		saveLoggedUser: (data) => dispatch(saveLoggedUser(data)),
		// appStateReset:()=>dispatch(appStateReset()),
		
	};
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
